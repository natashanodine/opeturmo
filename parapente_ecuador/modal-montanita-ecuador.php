<div class="modal fade" id="DosMangasModal" tabindex="-1" role="dialog" aria-labelledby="DosMangasModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="DosMangasModalLabel">Dos Mangas Manglaralto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselDosMangas" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_tours/montanita/dos-mangas-ecuador-montanita.jpg" alt="Montanita dos Mangas Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/montanita/dos_mangas_montanita_ecuador.jpg" alt="Montanita dos Mangas Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselDosMangas" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_tours/montanita/dos-mangas-ecuador-montanita.jpg" alt="Montanita dos Mangas Ecuador"></li>
							<li data-target="#carouselDosMangas" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_tours/montanita/dos_mangas_montanita_ecuador.jpg" alt="Montanita dos Mangas Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Dos Mangas Manglaralto</h2><header>
											<p>Tour Dos Mangas a 6,5km de la parroquia de Manglaralto cabalgata por la ruta de la Cascada o ruta de las pozas donde se puede observar variedad de flora y fauna con posibilidad de observar aves y monos.</p>
											<p>Ruta de la Cascada: Es considerado un bosque primario de especies maderables, frutales y medicinales, y habitan una gran cantidad de monos aulladores, que pueden ser observados con mucha facilidad en la época de invierno (diciembre a junio)
											</p>
											<p>Ruta de las piscinas naturales: Este sendero tiene cierto grado de dificultad y pendiente que atraviesa varias pozas formada naturalmente en el hecho del rocoso rio que recorre el bosque húmedo con pequeña formación rocosas que contiene estalactitas y estalagmitas.</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="SurfModal" tabindex="-1" role="dialog" aria-labelledby="SurfModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="SurfModalLabel">Clases de Surf</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselSurf" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class=" " src="https://opeturmo.com/images_tours/montanita/surf-classes-montanita.jpg" alt="Surf classes Montanita">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/montanita/Surf-Montanita.jpg" alt="Surf classes Montanita">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselSurf" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_tours/montanita/surf-classes-montanita.jpg" alt="Surf classes Montanita"></li>
							<li data-target="#carouselSurf" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_tours/montanita/Surf-Montanita.jpg" alt="Surf classes Montanita"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Clases de Surf</h2><header>
											<p>Una clase de dos horas, los primeros 45 minutos son utilizados en la revisión de la teoría en la
																	playa, para que luego podemos practicar en el mar durante una hora y 15 minutos y conseguir
																	lo que todo estudiante quiere ansiosamente hacer - de pie sobre la tabla y paseo una ola.</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>