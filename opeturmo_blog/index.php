<?php get_header(); ?>

<div class="row blog_row">
<section class="col-12 col-md-9">
	<?php if (have_posts()) : while (have_posts()) : the_post();     ?>
		<div class="blog_post"<?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header class="header">
				<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
			</header>

			

			<article class="entry row">
				<div class="row">
				<div class="col-md-12 images">
				<p><?php include (TEMPLATEPATH . '/inc/meta.php' ); ?></p>
										 <?php
										preg_match_all('/(<img [^>]*>)/', get_the_content(), $images);
					for($i=0; isset($images[1]) && $i < count($images[1]); $i++) {
						echo $images[1][$i];
					}
										?>
										<p><?php $content = preg_replace('/(<img [^>]*>)/', '', get_the_content());
					$content = apply_filters('the_content', $content);
					echo $content;
				?></p>
				<div class="postmetadata">
				<p><?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
			</div>
				</div>				
				</div>	

				
			</article>

			

		</div>

	<?php endwhile; ?>

	<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

	<?php else : ?>

		<h2>Not Found</h2>

	<?php endif; ?>
</section>
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>