
<div class="modal fade" id="modalToursGalapagos" tabindex="-1" role="dialog" aria-labelledby="modalToursGalapagos" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalToursGalapagos">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="col-12">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6">
						
					<div id="carouselBahia" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_tours/galapagos/tortoise-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/galapagos/pelikan-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/galapagos/cactus-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
							<div class="carousel-item">
							  
							  <img class="" src="https://opeturmo.com/images_tours/galapagos/iguana-galapagos-islands.jpg" alt="Tours Galapagos Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselBahia" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_tours/galapagos/tortoise-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
							<li data-target="#carouselBahia" data-slide-to="1"><img class="" src="https://opeturmo.com/images_tours/galapagos/cactus-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
							<li data-target="#carouselBahia" data-slide-to="2"><img class=" " src="https://opeturmo.com/images_tours/galapagos/pelikan-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
							<li data-target="#carouselBahia" data-slide-to="3"><img class="" src="https://opeturmo.com/images_tours/galapagos/iguana-galapagos-islands.jpg" alt="Tours Galapagos Ecuador"></li>
						  </ol>
						
						</div>
						<div class="row daily-galapagos">
							<div class="col-12">
								<header><h4>Descripci&oacute;n Tours Diarios</h4></header>
							</div>
						</div>
						<div class="accordion" id="accordionToursGalapagos">
						  <div class="card card-tours">
							<div class="card-header" id="IslaSeymour">
							  <h5 class="mb-0">
								<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseIslaSeymour" aria-expanded="true" aria-controls="collapseIslaSeymour">
								  Isla Seymour
								</button>
							  </h5>
							</div>

							<div id="collapseIslaSeymour" class="collapse" aria-labelledby="headingIslaSeymour" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
									<p>
									En el Canal de Itabaca, se abordar&aacute; la embarcaci&oacute;n hacia la isla de seymour con una
									navegaci&oacute;n aproximadamente de 1:15 y en donde podr&aacute; observar colonias de fragatas, piqueros patas azules, lobos e
									iguanas marinas. Visita a playa "Las Bachas" sitio ideal para nadar y hacer snorkel.
									</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingBartolome">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseBartolome" aria-expanded="false" aria-controls="collapseBartolome">
									Bartolome
								</button>
							  </h5>
							</div>
							<div id="collapseBartolome" class="collapse" aria-labelledby="headingBartolome" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
									<p>
									Desde el Canal de Itabaca e iniciar la navegaci&oacute;n hacia Bartolom&eacute;;  tiempo aproximado de navegaci&oacute;n 2
									horas; se observa: piqueros, lava, pinguinos, lobos marinos y hermosa vista panor&aacute;mica a la Isla Santiago. Excelente
									sitio para nadar y hacer snorkel.
									</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingSantaFe">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSantaFe" aria-expanded="false" aria-controls="collapseSantaFe">
									Santa Fe
								</button>
							  </h5>
							</div>
							<div id="collapseSantaFe" class="collapse" aria-labelledby="headingSantaFe" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
									<p>
									Se sale del muelle de turistas en Puerto Ayora en la embarcaci&oacute;n hacia la isla Santa Fe, la misma que se encuentra
									ubicada entre San Crist&oacute;bal y Santa Cruz. Podr&aacute; observar grandes colonias de lobos marinos, iguanas marinas,
									pelicanos, diferentes variedades de peces, piqueros enmascarados, piqueros patas azules. Sitio muy recomendado
									para hacer snorkel y nadar.
									</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingFloreana">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFloreana" aria-expanded="false" aria-controls="collapseFloreana">
								    Floreana
								</button>
							  </h5>
							</div>
							<div id="collapseFloreana" class="collapse" aria-labelledby="headingFloreana" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
									<p>
									Salida desde el muelle de
									turistas en Puerto Ayora y tomar la embarcaci&oacute;n hacia Floreana. Tour de snorkel en: Playa negra y lober&iacute;a, para
									observar seg&uacute;n la temporada: delfines, lobos marinos, tortugas, tiburones, y variedad de peces seg&uacute;n la temporada.
									Visita al Puerto Velasco Ibarra para observar las ruinas de los piratas y vertientes de agua en la parte alta de la Isla.
									Tiempo de navegaci&oacute;n: Puerto Ayora- Floreana 2h50
									.</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingPlazaPunta">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsePlazaPunta" aria-expanded="false" aria-controls="collapsePlazaPunta">
								    Plaza y Punta Carrion
								</button>
							  </h5>
							</div>
							<div id="collapsePlazaPunta" class="collapse" aria-labelledby="headingPlazaPunta" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
								    <p>
									Salida desde el muelle de turistas o Canal de Itabaca. Al arribo podr&aacute; observar una maravillosa vista del lugar, Plaza sur est&aacute; cubierta de una
									vegetaci&oacute;n baja de color rojizo llamada sesuvium lo cual la hace muy atractiva, podemos ver: gaviotas, lagartijas,
									p&aacute;jaro tropical, piqueros de patas azules, piqueros enmascarados, colonia de lobos marinos e iguanas terrestres.
									</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingSanCristobal">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSanCristobal" aria-expanded="false" aria-controls="collapseSanCristobal">
								    San Cristobal
								</button>
							  </h5>
							</div>
							<div id="collapseSanCristobal" class="collapse" aria-labelledby="headingSanCristobal" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
									<p>Hora de salida del muelle 07 a.m.
									Navegaci&oacute;n aproximada de 2 horas hacia Puerto Baquerizo Moreno nos dirigimos en embarcaci&oacute;n hacia Rocas
									Kicker ( Le&oacute;n Dormido) para realizar Snorkell, en este sitio hay mucha actividad se puede observar , tiburones,
									mantas rayas lobos marinos , peces, tortugas marinas, luego se visita Isla Lobos para observar fragatas , piqueros,
									lobos marinos, y descansamos en la playa Manglecito para tomar el box lunch y nadar. Retorno a Puerto Ayora
									6H00 p.m. aproximadamente
									</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingIsabela">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseIsabela" aria-expanded="false" aria-controls="collapseIsabela">
								    Isabela
								</button>
							  </h5>
							</div>
							<div id="collapseIsabela" class="collapse" aria-labelledby="headingIsabela" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
									<p>
									Recojida en Hotel: 06h50am. 
									Navegaci&oacute;n (2 horas de viaje) Arribo a la isla Isabela, donde el gu&iacute;a dar&aacute; indicaciones de las actividades a realizar
									tomamos un bus o taxi que los lleva hacia los Humedales donde se encontrar&aacute;n con: laguna de los Flamingos, centro
									de crianza de tortugas gigantes un peque&ntilde;o centro de interpretaci&oacute;n del Parque Nacional Gal&aacute;pagos y la playa,
									tambi&eacute;n es posible encontrarnos con iguanas marinas, lagartijas de lava, plantas volc&aacute;nicas como el cactus de lava, la
									escalesia, entre otras. Nos dirigimos al muelle para tomar una embarcaci&oacute;n para visitar el islote tintoreras, donde se realiza un desembarque
									seco caminata de 20 minutos para apreciar los tiburones de aleta blanca, iguanas, piqueros patas azules, pinguinos,
									realizamos snorkell fuera del islote y podemos observar, manta rayas, lobos marinos, tortugas marinas.
									</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingTourBahia">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTourBahia" aria-expanded="false" aria-controls="collapseTourBahia">
								    Tour de Bahia
								</button>
							  </h5>
							</div>
							<div id="collapseTourBahia" class="collapse" aria-labelledby="headingTourBahia" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
									<p>
									Se visita la  Isla “la  Lobería” llamada as&iacute; por la gran colonia de lobos marinos  donde se  realiza snorkel con los lobos  marinos, es un sitio  muy especial para  ver gran variedad de peces de colores, diversidad  de especies  del fondo marino.
									Luego nos dirigimos a Punta Estrada  se puede observar  animales tales como  gaviotines, piqueros, fragatas, lobos marinos, manta raya,  ETC.
									Visita canal de los tiburones se observa los tiburones y  desde el Mirador se puede apreciar el puerto y las monta&ntilde;as  de la  parte alta de la Isla
									islote tintoreras, donde se realiza un desembarque seco caminata de 20 minutos para apreciar los tiburones de aleta blanca, iguanas, piqueros patas azules, pingüinos, realizamos snorkell  fuera del islote  y podemos observar, manta  rayas, lobos marinos, tortugas marinas. 
									Visita Playa de los Perros para observar colonia de iguanas marinas, lobos marinos  y vista paisaj&iacute;stica.
									Visita al canal del  amor  hermoso canal de agua dormidas.
									Visita las Grietas, piscina  natural de aguas filtradas, excelente para  hacer snorkell.
									</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingTortugaBay">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTortugaBay" aria-expanded="false" aria-controls="collapseTortugaBay">
								    Tortuga Bay
								</button>
							  </h5>
							</div>
							<div id="collapseTortugaBay" class="collapse" aria-labelledby="headingTortugaBay" data-parent="#accordionToursGalapagos">
							  <div class="card-body">
								<p>Hermosa playa de arena blanca, tiene un recorrido aproximado de un 1 km. ½ de distancia desde Puerto Ayora,
									excelente sendero rodeado de exuberante vegetación y se observan aves del lugar. </p>
								</div>
							</div>
						  </div>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-md-6">
					<div class="row">
							<div class="col-12">
								<header><h4>Descripci&oacute;n Paquetes</h4></header>
							</div>
						</div>
						<div class="row">
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Tour 3 Dias 2 Noches</h5></header>
									<ul>
									<li>Dia 1: Traslado desde  el aeropuerto de Baltra  hacia el Canal de Itabaca. Visita a la parte alta de  Santa Cruz, rancho Primicias  para  ver tortugas en estado  natural, t&uacute;neles de lava y  vegetaci&oacute;n del lugar</li>
									<li>Dia 2: Tour  navegable a otra  isla como: Seymour, Bartolom&eacute;,  Santa Fe, Floreana , isabela /o  Plazas.  (S&oacute;lo se visita  una  isla y est&aacute; sujeto a disponibilidad)</li>
									<li>Dia 3: Traslado desde  el Hotel hacia el aeropuerto de Baltra</li>
									</ul>
								</article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Tour 4 Dias 3 Noches</h5></header>
									<ul>
									<li>Dia 1: Traslado desde  el aeropuerto de Baltra  hacia el Canal de Itabaca. Visita a la parte alta de  Santa Cruz, rancho Primicias  para  ver tortugas en estado  natural, t&uacute;neles de lava y  vegetaci&oacute;n del lugar</li>
									<li>Dia 2: Tour  navegable a otra  isla como: Seymour, Bartolom&eacute;,  Santa Fe, Floreana , isabela /o  Plazas.  (S&oacute;lo se visita  una  isla y est&aacute; sujeto a disponibilidad)</li>
									<li>Dia 3: Visita al centro de interpretaci&oacute;n Charles  Darwin ubicado  en la Isla Santa Cruz. En la  tarde  Tour  de Bah&iacute;a  o  visita a la playa de Tortuga Bay( solo dos  sitios).</li>
									<li>Dia 4: Traslado desde  el Hotel hacia el aeropuerto de Baltra</li>
									</ul>
								</article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Tour 5 Dias 4 Noches</h5></header>
									<ul>
									<li>Dia 1: Traslado desde  el aeropuerto de Baltra  hacia el Canal de Itabaca. Visita a la parte alta de  Santa Cruz, rancho Primicias  para  ver tortugas en estado  natural, t&uacute;neles de lava y  vegetaci&oacute;n del lugar</li>
									<li>Dia 2: Tour  navegable a otra  isla como: Seymour, Bartolom&eacute;,  Santa Fe, Floreana , isabela /o  Plazas.  (S&oacute;lo se visita  una  isla y est&aacute; sujeto a disponibilidad)</li>
									<li>Dia 3: Tour  navegable a otra  isla como: Seymour, Bartolom&eacute;,  Santa Fe, Floreana , isabela /o  Plazas.  (S&oacute;lo se visita  una  isla y est&aacute; sujeto a disponibilidad)</li>
									<li>Dia 4: Visita al centro de interpretaci&oacute;n Charles  Darwin ubicado  en la Isla Santa Cruz. En la  tarde  Tour  de Bah&iacute;a  o  visita a la playa de Tortuga Bay( solo dos  sitios).</li>
									<li>Dia 5: Traslado desde  el Hotel hacia el aeropuerto de Baltra</li>
									</ul>			
								</article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Tour 5 Dias 4 Noches Buceo</h5></header>
									<ul>
									<li>Dia 1: Transfer desde aeropuerto de Baltra/visita cr&aacute;teres gemelos/almuerzo/T&uacute;neles y tortugas en estado Natural.</li>
									<li>Dia 2: Buceo Diario (Incluye box Lunch a bordo)</li>
									<li>Dia 3: Buceo Diario (Incluye box Lunch a bordo)</li>
									<li>Dia 4: Dia Libre.</li>
									<li>Dia 5: Traslado desde  el Hotel hacia el aeropuerto de Baltra</li>
										</ul>		
								</article>
							</section>
						</div>
					</div>
				</div>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>