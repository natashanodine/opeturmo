<div class="modal fade" id="BuceoGalapagosModal" tabindex="-1" role="dialog" aria-labelledby="BuceoGalapagosModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BuceoGalapagosModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											<div class="tours_pax">
												<header><h4>Daphne</h4></header>
												<ul><li>Aqu&iacute; por lo general vemos tiburones de Gal&aacute;pagos, tintoreras,  peces tropicales, esponjas, corales, morenas, tortugas,  pulpos, rayas, etc.</li></ul>
											</div>
										</div>
										<div class="col-12 col-sm-6 col-md-6">
											<div class="tours_pax">
												<header><h4>Gordon Rock</h4></header>
												<ul><li>Aqu&iacute; se puede ver los tiburones martillos que son la atracci&oacute;n principal de Gordon Rock, tambi&eacute;n se puede ver peces de arrecife, rayas, tortugas marinas, estrellas de mar, pulpos, morenas, etc.</li></ul>
												
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											<div class="tours_pax">
												<header><h4>Mosquera</h4></header>
												<ul><li>Aqu&iacute; podremos encontrar Rayas sart&eacute;n, Tortugas, Tiburones, Nudibranqueos, culebras de jardín, Leones marinos, Peces tropicales, Tiburones martillos, Barracudas, etc.</li></ul>
											</div>
										</div>
										<div class="col-12 col-sm-6 col-md-6">
											<div class="tours_pax">
												<header><h4>Seymour</h4></header>
												<ul><li>Aqu&iacute;  podremos encontrar, nudibranquios, tortugas de mar, leones marinos, tiburones de arrecife de punta blanca, tiburones martillo, tambi&eacute;n pueden verse los tiburones de Gal&aacute;pagos, escuelas de peces tropicales, barracudas, etc.</li></ul>
											</div>
										</div>
									</div>
									
 </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>