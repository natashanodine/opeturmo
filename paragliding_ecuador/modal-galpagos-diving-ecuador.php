<div class="modal fade" id="BuceoGalapagosModal" tabindex="-1" role="dialog" aria-labelledby="BuceoGalapagosModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BuceoGalapagosModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											<div class="tours_pax">
												<header><h4>Daphne</h4></header>
												<ul><li>Here we usually see Galapagos sharks, dyers, tropical fish, sponges, corals, moray eels, turtles, octopuses, rays, etc.</li></ul>
											</div>
										</div>
										<div class="col-12 col-sm-6 col-md-6">
											<div class="tours_pax">
												<header><h4>Gordon Rock</h4></header>
												<ul><li>Here you can see the sharks hammers that are the main attraction of Gordon Rock, you can also see reef fish, rays, sea turtles, starfish, octopus, moray eels, etc.</li></ul>
												
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											<div class="tours_pax">
												<header><h4>Mosquera</h4></header>
												<ul><li>Here we can find Frying Pan, Turtles, Sharks, Nudibranches, Garden Snakes, Sea Lions, Tropical Fish, Hammer Sharks, Barracudas, etc.</li></ul>
											</div>
										</div>
										<div class="col-12 col-sm-6 col-md-6">
											<div class="tours_pax">
												<header><h4>Seymour</h4></header>
												<ul><li>Here we can find nudibranchs, sea turtles, sea lions, white-tipped reef sharks, hammerhead sharks, Galapagos sharks, schools of tropical fish, barracudas, etc.</li></ul>
											</div>
										</div>
									</div>
									
 </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>