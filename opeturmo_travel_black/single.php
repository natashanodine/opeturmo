<?php get_header(); ?>
<div class="row blog_row">
	<div class="col-12 col-md-9">
		<?php if (have_posts()) : while (have_posts()) : the_post();     ?>
		<div class="blog_post"<?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header class="header">
				<h3><?php the_title(); ?></h3>
			</header>


			<p><?php include (TEMPLATEPATH . '/inc/meta.php' ); ?></p>

			<div class="entry">
				
				<?php the_content(); ?>

				<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
				
				<?php the_tags( 'Tags: ', ', ', ''); ?>

			</div>
			
			<?php edit_post_link('Edit this entry','','.'); ?>
			
		<?php comments_template(); ?>
		</div>


	<?php endwhile; endif; ?>
	</div>
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>