<!--<div class="content-desktop-tablet">-->

	<div class="row contactos">
		<div class="col-12 col-sm-6 col-md-4">
			<header><h1>Contactos</h1></header>
			<div itemscope itemtype="http://schema.org/LocalBusiness">
			<div itemprop="name"><strong>Oficinas Opeturmo</strong></div>
			<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
			<p><span itemprop="addressLocality">Ruta del Spondylus </span>
			<span itemprop="addressLocality">(Ruta del Sol)</span><br>
			<span itemprop="addressLocality">Playa Bruja </span><br>
			<span itemprop="addressLocality">Libertador Bolivar </span><br>
			<span itemprop="addressLocality">Momta&ntilde;ita </span><br>
			<span itemprop="addressRegion">Santa Elena </span><br>
			<span itemprop="addressCountry">Ecuador</span></p>
			</div>
			</div>
			<p>
			<a href="tel:+593987237776"><i class="fa fa-phone fas-tel-contact"></i> +593 987237776</a><br>	
			<a href="tel:+593993273164"><i class="fa fa-phone fas-tel-contact"></i> +593 993273164</a><br>	
			<a href="tel:+593979003878"><i class="fa fa-phone fas-tel-contact"></i> +593 979003878</a>		
			</p>
						
			
			
		</div>
		<div class="col-12 col-sm-6 col-md-4">
			<header><h4>Comunicate con nosotros:</h4></header>
			<form role="form" class="form" action='https://opeturmo.com/parapente_ecuador/contact_form.php' method='post'>
				  <div class="form-group row">
					<label for="name" class="col-sm-2 col-form-label">Nombre:</label>
					<div class="col-sm-10">
					  <input type="text" class="form-control" name="name"  id="name" placeholder="Nombre" required >
					</div>
				  </div>
					<div class="form-group row">
					<label for="tel" class="col-sm-2 col-form-label">Tel&eacute;fono:</label>
					<div class="col-sm-10">
					  <input type="number" class="form-control" name="tel"  id="tel" placeholder="Tel&eacute;fono" >
					</div>
				  </div>
					<div class="form-group row">
					<label for="email" class="col-sm-2 col-form-label">Email</label>
					<div class="col-sm-10">
					  <input type="email" class="form-control" name="email"  id="email" placeholder="Email" required >
					</div>
				  </div>
					<div class="form-group row ">
					<label for="comment" class="col-sm-2 col-form-label">Mensaje</label>
					<div class="col-sm-10">
						<textarea class='form-control' rows='2' name='comment' placeholder='Mensaje' required ></textarea>
					</div>
				  </div>
				  
					<div class="form-group row ">
					<div class="col-12"> 
					<div class="captcha_wrapper">
						<div class="g-recaptcha" data-sitekey="6LcPkZEUAAAAAPbw2G7UDZ6RSXQBegbV6Fd79PCK"></div>
					</div>
					</div>
				  </div>
				  
				  <div class="form-group row contact-button">
					<div class="col-sm-12">
					  <button type="submit" class="btn button-contact">ENVIAR</button>
					</div>
				  </div>
				</form>
		</div>
		<div class="col-12 col-sm-4 col-md-4">
		 <div class="row">
			<div class="col-12 parapente_img">
		  <img class="content-desktop ecuador-parapente" src="https://opeturmo.com/images_parapente/logos/parapente-ecuador-white.png" alt="parapente ecuador">
		  <img class="content-mobile ecuador-parapente" src="https://opeturmo.com/images_parapente/logos/parapente-ecuador-white-150px.png" alt="parapente ecuador">
		  
		  </div>
		  </div>
		  <div class="row">
			<div class="col-12 licencias_img">
				<img class="appi-ecuador-parapente" src="https://opeturmo.com/images_parapente/social-icons/aep-small.png" alt="parapente ecuador">
				<img class="appi-ecuador-parapente" src="https://opeturmo.com/images_parapente/social-icons/appi_small.png" alt="parapente ecuador">
			</div>		  
		  </div>
		</div>
	
	</div>
	
	<div class="row copyright">
			<div class="col-12 ">
				<p><small>Copyright <i class="fa fa-copyright"></i> 2016 - <?php echo date("Y"); ?> all rights reserved.</small></p>
			</div>
		</div>
	
	
	<!--
</div>


	<div class="content-tablet">
	<div class="row contactos">
		<div class="col-12 col-sm-6">
			<header><h1>Contactos</h1></header>
			<div itemscope itemtype="http://schema.org/LocalBusiness">
			<div itemprop="name"><strong>Oficinas Opeturmo</strong></div>
			<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
			<p><span itemprop="addressLocality">Ruta del Spondylus </span>
			<span itemprop="addressLocality">(Ruta del Sol)</span><br>
			<span itemprop="addressLocality">Playa Bruja </span><br>
			<span itemprop="addressLocality">Libertador Bolivar </span><br>
			<span itemprop="addressRegion">Santa Elena </span><br>
			<span itemprop="addressCountry">Ecuador</span></p>
			</div>
			</div>
			
			<p>
			<a href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i>Tel 1</button>	</a>			
			<a href="tel:+593993273164"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i>Tel 2</button></a>		
			<a href="tel:+593979003878"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i>Tel 3</button></a>		
			</p>
			<p>
			<a href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/>Tel 1</button></a>
			<a href="intent://send/+593993273164#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/>Tel 2</button></a>
			<a href="intent://send/+593979003878#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/>Tel 3</button></a>
			</p>		
			
			
		</div>
		<div class="col-12 col-sm-6">
			<header><h4>Comunicate con nosotros:</h4></header>
			<form role="form" class="form" action='https://opeturmo.com/parapente_ecuador/contact_form.php' method='post'>
				  <div class="form-group row">
					<label for="name" class="col-sm-2 col-form-label">Nombre:</label>
					<div class="col-sm-10">
					  <input type="text" class="form-control" name="name"  id="name" placeholder="Nombre" required >
					</div>
				  </div>
					<div class="form-group row">
					<label for="tel" class="col-sm-2 col-form-label">Tel&eacute;fono:</label>
					<div class="col-sm-10">
					  <input type="number" class="form-control" name="tel"  id="tel" placeholder="Tel&eacute;fono" >
					</div>
				  </div>
					<div class="form-group row">
					<label for="email" class="col-sm-2 col-form-label">Email</label>
					<div class="col-sm-10">
					  <input type="email" class="form-control" name="email"  id="email" placeholder="Email" required >
					</div>
				  </div>
					<div class="form-group row ">
					<label for="comment" class="col-sm-2 col-form-label">Mensaje</label>
					<div class="col-sm-10">
						<textarea class='form-control' rows='2' name='comment' placeholder='Mensaje' required ></textarea>
					</div>
				  </div>
				  				  
					<div class="form-group row ">
					<div class="col-12"> 
					<div class="captcha_wrapper">
						<div class="g-recaptcha" data-sitekey="6LcPkZEUAAAAAPbw2G7UDZ6RSXQBegbV6Fd79PCK"></div>
					</div>
					</div>
				  </div>
				  <div class="form-group row contact-button">
					<div class="col-sm-12">
					  <button type="submit" class="btn button-contact">ENVIAR</button>
					</div>
				  </div>
				</form>
		</div>
		
		 
		  <div class="row">
			<div class="col-12 licencias_img">
				<img class="appi-ecuador-parapente" src="https://opeturmo.com/images_parapente/social-icons/aep-small.png" alt="parapente ecuador">
				<img class="appi-ecuador-parapente" src="https://opeturmo.com/images_parapente/social-icons/appi_small.png" alt="parapente ecuador">
			</div>		  
		  </div>
</div>
	--> 