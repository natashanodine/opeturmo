<!DOCTYPE html>
<html lang="en">
<head>
    <title>Paragliding Flights Ecuador - Adventure Tourism</title>
    <meta charset="utf-8">
	<meta 
	name='description' content="The best tandem paragliding flights, tours and courses in Ecuador. Fly over the coast of Ecuador and in the heights of the Andes. Get a birds eye view, flying through the skies with certified pilots; incredible views of the coastline, pristine beaches and mangroves, mountains, towns and cities. incredible views of the coastline, pristine beaches in a paratrike or paraglider. Flights in Paragliding Montañita, Paragliding Guayaquil, Paragliding Quito, Paragliding Puerto  Lopez, Paragliding Ibarra, Paragliding Baños, Paragliding Cuenca.">
	
	<meta name="author" content="Developer: Natasha Carla Nodine Wyatt, Designer: Amaury Cornejo">
	<link rel="icon" href="https://opeturmo.com/logo_ecuador_parapente.ico" type="image/ico" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://opeturmo.com/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://opeturmo.com/css/main.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<meta name="msvalidate.01" content="380B1B2C798B6CBEBC9C63EECEE117EF" />
	
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	
	<script type="text/javascript">
  (function(config) {
    window._peekConfig = config || {};
    var idPrefix = 'peek-book-button';
    var id = idPrefix+'-js'; if (document.getElementById(id)) return;
    var head = document.getElementsByTagName('head')[0];
    var el = document.createElement('script'); el.id = id;
    var date = new Date; var stamp = date.getMonth()+"-"+date.getDate();
    var basePath = "https://js.peek.com";
    el.src = basePath + "/widget_button.js?ts="+stamp;
    head.appendChild(el); id = idPrefix+'-css'; el = document.createElement('link'); el.id = id;
    el.href = basePath + "/widget_button.css?ts="+stamp;
    el.rel="stylesheet"; el.type="text/css"; head.appendChild(el);
  })({key: '4aaee0fc-19f9-43da-8667-9972fe009432'});
</script>
			
	<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
	$.src="//v2.zopim.com/?3oC2gd9KO6sPaEmter4wep7sCRwDZT9X";z.t=+new Date;$.
	type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
	</script>
	
	
	<!--<script src="https://apis.google.com/js/platform.js" async defer>
	  {lang: 'en-GB'}
	</script>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-78803847-1', 'auto');
  ga('send', 'pageview');

	</script>-->
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78803847-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78803847-1');
</script>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '377908959422554');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=377908959422554&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

	
	
</head>
<body>

 
<div class="content-desktop">
<nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light">
  <a class="navbar-brand" href="#"><img src="https://opeturmo.com/images_parapente/logos/logo_opeturmo_menu.png" width="30" height="30" alt="">Home</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse " id="navbarNavDropdown">
    <ul class="navbar-nav  mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#vuelos_parapente">Flights</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#cursos_parapente">Courses</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#pedida_mano">Marriage Proposal</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#tours">Tours</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#contactos">Contacts</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://opeturmo.com/blog/en/">Blog</a>
      </li>
	  
      <li class="nav-item">
        <a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/3PML" class="peek-book-btn-blue peek-book-button-flat peek-book-button-sm" data-button-text="Book Now">Parapente</a>
      </li>
	  </ul>
	   <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="https://opeturmo.com"><img class="" src="https://opeturmo.com/images_parapente/language/spanish.png" alt="English"></a>
      </li>
    </ul>
	 
  </div>
</nav>
</div>


<div class="content-mobile">

<nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light">
  <a class="navbar-brand" href="#"><img src="https://opeturmo.com/images_parapente/logos/logo_opeturmo_menu.png" width="30" height="30" alt="">Home</a>
  
        <a class="nav-link" href="https://opeturmo.com"><img class="" src="https://opeturmo.com/images_parapente/language/spanish.png" alt="English"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse " id="navbarNavDropdown">
    <ul class="navbar-nav  mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#vuelos_parapente">Flights</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#cursos_parapente">Courses</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#pedida_mano">Marriage Proposal</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#tours">Tours</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#contactos">Contacts</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://opeturmo.com/blog/en/">Blog</a>
      </li>
	  
      <li class="nav-item">
        <a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/3PML" class="peek-book-btn-blue peek-book-button-flat peek-book-button-sm" data-button-text="Book Now">Parapente</a>
      </li>
	  </ul>
  </div>
</nav>

</div>


<div class="container-fluid">
	<a class="hash"  name="index"></a>
    <?php include 'paragliding-parapente-ecuador.php'; ?>
    <?php include 'about-us-ecuador-parapente.php'; ?>

    
	<a class="hash"  name="vuelos_parapente"></a>
    <?php include 'paragliding-flights-ecuador.php'; ?>

	<a class="hash"  name="cursos_parapente"></a>
	<?php include 'paragliding-courses-ecuador.php'; ?>
	<a class="hash"  name="pedida_mano"></a>
    <?php include 'marriage-proposal.php'; ?>

	<a class="hash"  name="tours"></a>
    <?php include 'tours-paragliding-ecuador.php'; ?>
	
	
	<a class="hash"  name="contactos"></a>
	<?php include 'contacts.php'; ?>
	
	
	
	
	
	
</div>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://opeturmo.com/js/bootstrap.min.js"></script>
  <script src="https://opeturmo.com/js/lightbox.js"></script>
  <script src="https://opeturmo.com/js/scroll.js"></script>

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
</body>
</html>