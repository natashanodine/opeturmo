
	<div class="row header_row">
		<div class="col-12">
			<header><h1>Escuela Parapente Ecuador</h1></header>
		</div>
	</div>
	
	
	
	
	
	<div class="row cursos">
		<div class="col-12 col-sm-12 col-md-8">
			<header><h2>Cursos de Parapente</h2></header>
			
			<p>En nuestra escuela de parapente puedes aprender de este deporte de aventura con un instructor certificado por APPI (Associacion of Paraglider Pilots and Instructors). Tenemos diferentes cursos de parapente dependiendo de tu nivel.</p>
			<h4>Iniciacion</h4>
			<p>Es el primer grado en el Sistema de Educaci&oacute;n APPI de los cursos de parapente. El estudiante aprende como instalar y controlar el ala en el suelo y tienen su primera experiencia de vuelo solitario. En este nivel, se le permite al estudiante a volar s&oacute;lo con comunicaci&oacute;n por radio y bajo la supervisi&oacute;n del instructor. De 10 a 15 vuelos solos (3 en tandem instructivo si es necesario)</p>
			<p>Se usa el despegadero de Playa Bruja para este nivel.</p>
			<p><a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/zgp9v"  class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Reservar" >Reservar</a></p>
			<h4>Progresi&oacute;n 1</h4>
			<p>En este curso de parapente el estudiante es capaz de volar en parapente desde el despegue hasta la zona de aterrizaje en condiciones meteorol&oacute;gicas estables. En este nivel, el estudiante todavía se permite volar s&oacute;lo con comunicaci&oacute;n por radio y bajo la supervisi&oacute;n del instructor.</p>
			<p>Se usa el despegadero de Playa Bruja para este nivel.</p>
			<p><a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/kxk9q"  class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Reservar" >Reservar</a></p>
			<h4>Progresi&oacute;n 2</h4>
			<p>En este curso de parapente el estudiante ha alcanzado el nivel para convertirse en un piloto independiente. </p>
			<p>Ellos son capaces de volar sin supervisión</p>
			<ol>
			<li>Analizar las diferentes condiciones climáticas</li>
			<li>Tener una especialidad en alpino, o soaring</li>
			</ol>
			<p>Antes de volar en diferentes sitios, se recomienda a los pilotos APPI elegir los lugares donde pueden recibir la ayuda de una escuela APPI. Aqu&iacute;, reciben informaci&oacuate;n importante, apoyo y mayor seguridad.</p>
			<p>Se usa el despegadero de Playa Bruja o Puerto Lopez para este nivel.</p>
			<p><a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/kxk9q"  class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Reservar" >Reservar</a></p>
			<h4>Cross Country</h4>
			<p>En este nivel de los cursos de parapente se ense&ntilde;a c&oacute;mo encontrar y girar en t&eacute;rmicas. Se usa el despegadero de Guayaquil para este nivel.</p>
			<p><a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/kxk9q"  class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Reservar" >Reservar</a></p>
			<p>Al terminar cualquier curso de arriba contara con la certificación APPI. </p>
			
		</div>
		<div class="col-12 col-sm-4 col-md-4 ">
		 
		 
		 
			<div id="carouselCursosParapente" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/cursos-parapente/cursos_de_parapente_playa_ecuador.jpg" alt="Paragliding Courses Ecuador">
								<div class="carousel-caption">
								  <h2>Cursos de Parapente Ecuador</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/cursos-parapente/escuela_parapente_ecuador.jpg" alt="Paragliding Courses Ecuador">
							<div class="carousel-caption">
								  <h2>Cursos de Parapente Ecuador</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/cursos-parapente/cursos_parapente_ecuador_playa.jpg" alt="Paragliding Courses Ecuador">
							<div class="carousel-caption">
								  <h2>Cursos de Parapente Ecuador</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/cursos-parapente/escuela_parapente_playa.jpg" alt="Paragliding Courses Ecuador">
							<div class="carousel-caption">
								  <h2>Cursos de Parapente Ecuador</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/cursos-parapente/curso_parapente_ecuador.jpg" alt="Paragliding Courses Ecuador">
							<div class="carousel-caption">
								  <h2>Cursos de Parapente Ecuador</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselCursosParapente" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselCursosParapente" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselCursosParapente" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/cursos-parapente/cursos_de_parapente_playa_ecuador.jpg" alt="Paragliding Courses Ecuador"></li>
							<li data-target="#carouselCursosParapente" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/cursos-parapente/escuela_parapente_ecuador.jpg" alt="Paragliding Courses Ecuador"></li>
							<li data-target="#carouselCursosParapente" data-slide-to="2"><img class=" " src="https://opeturmo.com/images_parapente/cursos-parapente/cursos_parapente_ecuador_playa.jpg" alt="Paragliding Courses Ecuador"></li>
							<li data-target="#carouselCursosParapente" data-slide-to="3"><img class=" " src="https://opeturmo.com/images_parapente/cursos-parapente/escuela_parapente_playa.jpg" alt="Paragliding Courses Ecuador"></li>
							<li data-target="#carouselCursosParapente" data-slide-to="4"><img class=" " src="https://opeturmo.com/images_parapente/cursos-parapente/curso_parapente_ecuador.jpg" alt="Paragliding Courses Ecuador"></li>
						  </ol>
						
						</div>
			
			
		</div>
	</div>
	
	
	
	