
<div class="col-12 col-md-3 sidebar">
	 <p>
		<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Ecuador,+Ruta+del+Spondylus+E15" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>
		<a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Montanita_Santa_Elena_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
		<a href="https://plus.google.com/+Opeturmoplayabruja" target="blank"><img alt="google+" src="https://opeturmo.com/images_parapente/social-icons/google_plus_xxsmall.png"/></a>
		<a href="https://www.instagram.com/parapentemontanita/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
		<a href="https://www.facebook.com/ParapenteMontanitaEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
		<a href="https://www.youtube.com/channel/UCaL6nuUBRgBITlKDkS2rTpg" target="blank"><img alt="youtube"  src="https://opeturmo.com/images_parapente/social-icons/youtube_xxsmall.png"/></a>
		<a href="https://www.pinterest.com/opeturmo/" target="blank"><img alt="pinterest"  src="https://opeturmo.com/images_parapente/social-icons/pinterest_xxsmall.png"/></a>
	</p>
    <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Sidebar Widgets')) : else : ?>
   
<!-- All this stuff in here only shows up if you DON'T have any widgets active in this zone -->

    	<?php get_search_form(); ?>
    
    
    <header>
    	<h4>Archives</h4>
		</header>
    	<ul>
    		<?php wp_get_archives('type=monthly'); ?>
    	</ul>
		<header>
        <h4>Categories</h4>
		</header>
    	   <?php wp_list_categories('show_count=1&title_li='); ?>
			<?php wp_list_bookmarks(); ?>
    
    	
    	<h2>Subscribe</h2>
    	<ul>
    		<li><a href="<?php bloginfo('rss2_url'); ?>">Entries (RSS)</a></li>
    		<li><a href="<?php bloginfo('comments_rss2_url'); ?>">Comments (RSS)</a></li>
    	</ul>
	
	<?php endif; ?>

</div>