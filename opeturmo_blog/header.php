<!DOCTYPE html>
<html lang="en">
<head>
<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>
    <title>Blog Parapente Ecuador - Turismo de aventura </title>
    <meta charset="utf-8">
		<meta name='description' content="Los mejores Vuelos Tandem, Tours y Cursos de  Parapente en Ecuador. Volar por las costas de Ecuador y en las alturas de los Andes.
		Volar por los cielos y ver el mundo a vista de pájaro con pilotos certificados; increíbles vistas de la costa, sus  playas y manglares, montañas, pueblos y ciudades.
		Una manera única para descubrir los mejores lugares turisticos del Ecuador, desde las costas hasta las alturas del los Andes.
		Descubrir los mejores lugares turisticos del Ecuador, desde las costas hasta las alturas del los Andes en un tour con paramotor.Buceo, Ballenas, Surf, Ciclismo, Pesca, La Isla de la Plata, Los Frailes - disfruta de todo en nuestros tours de aventura por el Parque Nacional Machalilla. Buceo, Ballenas, Surf, Ciclismo, Pesca,  - disfruta de todo en nuestros tours de aventura por el Parque Nacional Galapagos">
	<meta name="author" content="Developer: Natasha Carla Nodine Wyatt, Designer: Amaury Cornejo">
	<link rel="icon" href="https://opeturmo.com/parapentemontanita.ico" type="image/ico" >
	<link rel="icon" href="parapentemontanita.ico" type="image/ico" >  
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://opeturmo.com/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
	$.src="//v2.zopim.com/?3oC2gd9KO6sPaEmter4wep7sCRwDZT9X";z.t=+new Date;$.
	type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
	</script>
	
	
	<script src="https://apis.google.com/js/platform.js" async defer>
	  {lang: 'en-GB'}
	</script>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-78803847-1', 'auto');
  ga('send', 'pageview');

	</script>
	
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php if ( is_singular() ) wp_enqueue_script('comment-reply'); ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<nav class="navbar navbar-expand-lg navbar-light fixed-top">
  <a class="navbar-brand" href="https://opeturmo.com/"><img src="https://opeturmo.com/images_parapente/logo_opeturmo_menu.png" width="30" height="30" alt=""></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse " id="navbarNavDropdown">
    <ul class="navbar-nav  mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="https://opeturmo.com/">Inicio</a>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="https://opeturmo.com/#vuelos_parapente">Vuelos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://opeturmo.com/#tours">Tours</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://opeturmo.com/#cursos_parapente">Cursos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://opeturmo.com/#contactos">Contactos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://opeturmo.com/blog/es/">Blog</a>
      </li>
	   <li class="nav-item">
        <a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/rwRb" class="peek-book-btn-blue peek-book-button-flat peek-book-button-sm" data-button-text="Reservar">Parapente</a>
      </li>
	  </ul>
	   <ul class="navbar-nav">
      <li class="nav-item language">
        <?php pll_the_languages(array('show_flags'=>1,'show_names'=>0));?>
      </li>
    </ul>
	 
  </div>
</nav>



<div class="container-fluid">