<div class="modal fade" id="reserveTandemModal" tabindex="-1" role="dialog" aria-labelledby="reserveTandemModal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="reserveTandemModal">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
				<form role="form" class="form" action='https://opeturmo.com/parapente_ecuador/tandem_form.php' method='post'>
				  <div class="form-group row">
					<label for="name" class="col-sm-2 col-form-label">Nombre:</label>
					<div class="col-sm-10">
					  <input type="text" class="form-control" name="name"  id="name" placeholder="Nombre" required >
					</div>
				  </div>
					<div class="form-group row">
					<label for="tel" class="col-sm-2 col-form-label">Tel&eacute;fono:</label>
					<div class="col-sm-10">
					  <input type="number" class="form-control" name="tel"  id="tel" placeholder="Tel&eacute;fono" >
					</div>
				  </div>
					<div class="form-group row">
					<label for="email" class="col-sm-2 col-form-label">Email</label>
					<div class="col-sm-10">
					  <input type="email" class="form-control" name="email"  id="email" placeholder="Email" required >
					</div>
				  </div>
					<div class="form-group row ">
					<label for="comment" class="col-sm-2 col-form-label">Mensaje</label>
					<div class="col-sm-10">
						<textarea class='form-control' rows='2' name='comment' placeholder='Mensaje' required ></textarea>
					</div>
				  </div>
				  <div class="form-group row">
						<label for="date" class="col-sm-2 col-form-label">Fecha:</label>
						<div class="col-sm-10">
						<input type="date" id="date" name="date">
						</div>										
					</div>	
				  
				  <p>¿Donde quisieras volar en parapente?</p>
					<div class="row">
					<div class="col-12 col-sm-6 col-md-6">
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Monta&ntilde;ita">Parapente Monta&ntilde;ita</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Guayaqui">Parapente Guayaquil</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Puerto Lopez">Parapente Puerto L&oacute;pez</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Quito">Parapente Quito</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Ibarra">Parapente Ibarra</label>
					</div>
					</div>
					<div class="col-12 col-sm-6 col-md-6">

					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Crucita">Parapente Crucita</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Canoa">Parapente Canoa</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Ba&ntilde;os">Parapente Ba&ntilde;os</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Cuenca">Parapente Cuenca</label>
					</div>
					</div>
					</div>
					
					<div class="form-group row ">
					<div class="col-12"> 
					<div class="captcha_wrapper">
						<div class="g-recaptcha" data-sitekey="6LcPkZEUAAAAAPbw2G7UDZ6RSXQBegbV6Fd79PCK"></div>
					</div>
					</div>
				  </div>
				  
					
					<div class="form-group row contact-button">
					<div class="col-sm-12">
					  <button type="submit" class="btn button-contact">ENVIAR</button>
					</div>
				  </div>
				</form>
			 </div>
      <div class="modal-footer">
				
		
				
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
 
 
 
 
 
 
 
 
 
 
 
 
<div class="content-desktop">
 
 <div class="row header_row">
		<div class="col-12">
		<header><h1>Vuelos Parapente Ecuador</h1></header>
		</div>
	</div>
<div class="row">
	<div class="accordion" id="accordion_vuelos">
		 <div class="row  vuelos">
			<div class="col-2 col-sm-2 col-md-2 vuelos_box">
			<div class="card card-vuelos" data-toggle="collapse" data-target="#collapseMontanita" aria-expanded="true" aria-controls="collapseMontanita">
			    <header><h1>Montanita</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/vuelos-parapente-playa-montanita-ecuador.png" alt="Parapente Montanita Ecuador">
				<button>Info...</button>
			</div>
			</div>
			<div class="col-2  col-sm-2 col-md-2 vuelos_box">
			<div class="card  card-vuelos collapsed" data-toggle="collapse" data-target="#collapsePLopez" aria-expanded="false" aria-controls="collapsePLopez">
				 <header><h1>Puerto Lopez</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/vuelos-parapente-puerto-lopez-ecuador.png" alt="Parapente Puerto Lopez Ecuador">
				<button>Info...</button>
			</div>
			</div>
			<div class="col-2  col-sm-2 col-md-2 vuelos_box">
			<div class="card card-vuelos collapsed" data-toggle="collapse" data-target="#collapseGuayaquil" aria-expanded="false" aria-controls="collapseGuayaquil">
				 <header><h1>Guayaquil</h1></header>	  
				<img class=" " src="https://opeturmo.com/images_parapente/parapente-guayaquil/vuelos-parapente-guayaquil-ecuador.png" alt="Parapente Guayaquil Ecuador">
				<button>Info...</button>
			</div>
			</div>
			<div class="col-2  col-sm-2 col-md-2 vuelos_box">
			<div class="card card-vuelos collapsed" data-toggle="collapse" data-target="#collapseQuito" aria-expanded="false" aria-controls="collapseQuito">
				 <header><h1>Quito</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-quito-ecuador.png" alt="Parapente Quito Ecuador">
				<button>Info...</button>
			</div>
			</div>
			<div class="col-2  col-sm-2 col-md-2 vuelos_box">
			<div class="card  card-vuelos collapsed" data-toggle="collapse" data-target="#collapseIbarra" aria-expanded="false" aria-controls="collapseIbarra">
				  <header><h1>Ibarra</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-ibarra-ecuador.png" alt="Parapente Ibarra Ecuador">
				<button>Info...</button>
			</div>
			</div>
			<div class="col-2 col-sm-2  col-md-2 vuelos_box">
			<div class="card  card-vuelos collapsed" data-toggle="collapse" data-target="#collapseOtros" aria-expanded="false" aria-controls="collapseOtros">
				 <header><h1>Otros lugares</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-crucita/vuelos-parapente-crucita-ecuador.png" alt="Parapente Canoa Ecuador">
				<button>Info...</button>
			</div>
		</div>
		</div>
		
		 <div class="row vuelos_details">
			<div class="card col-md-12">
				<div id="collapseMontanita" class="collapse show" aria-labelledby="headingMontanita" data-parent="#accordion_vuelos">
				  <div class="card-body row">
					<div class="col-md-4">
						
						
						<div id="carouselMontanita" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-playa-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador">
								<div class="carousel-caption">
								  <h2>Parapente en Monta&ntilde;ita</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/playa-parapente-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Monta&ntilde;ita</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-montanita.jpg" alt="Parapente Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Monta&ntilde;ita</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-con-motor-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Monta&ntilde;ita con motor</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-a-motor-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Monta&ntilde;ita con motor</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselMontanita" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-playa-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador"></li>
							<li data-target="#carouselMontanita" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/playa-parapente-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador"></li>
							<li data-target="#carouselMontanita" data-slide-to="2"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-montanita.jpg" alt="Parapente Montanita Ecuador"></li>
							<li data-target="#carouselMontanita" data-slide-to="3"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-con-motor-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador"></li>
							<li data-target="#carouselMontanita" data-slide-to="4"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-a-motor-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador"></li>
						  </ol>
						
						</div>
						  
											
						
					</div>
					<div class="col-md-8">
					<header><h1>Parapente Monta&ntilde;ita</h1></header>
					<p>Volamos en parapente con el piloto gu&iacute;a, que te lleva a realizar la actividad de aventura, frente al mar sobre la Ruta del Spondylus (Ruta del Sol). El gu&iacute;a se encarga de toda la operaci&oacute;n, solo tienes que disfrutar y cumplir tu sue&ntilde;o de volar. La experiencia del vuelo dura entre 12- 15 minutos. </p>
					<li><b>Parapente Libre</b> Vuelos en parapente frente al mar es un vuelo din&aacute;mico que se realiza desplaz&aacute;ndose por los cielos aprovechando la ladera de la monta&ntilde;a el viento del mar nos permite poder sustentarnos en el aire y volar como los p&aacute;jaros. </li>
					<p><b>Promociones:</b></p>
					<ul>
					<li>Vuelan en Parapente 6 personas pagan 5</li>
					<li>Cumplea&ntilde;eros gratis las fotos</li>
					</ul>
					<li><b>Parapente a Motor</b> Un vuelo en Parapente motorizado se puede recorrer mayor distancia en el mismo tiempo, ascendemos a mayor altura para apreciar el increíble océano pacífico y el continente del perfil ecuatoriano</li>
					<p>Atendemos desde las 11:00am hasta las 6:00pm, pero este horario puede variar dependiendo la epoca del a&ntilde;o y del dia. </p>
										
				 
					<div class="row ">
						
						<div class="col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">    
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a class="content-tablet" href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/bDMZ" class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Reservar">Parapente Montanita</a>
							
							<!--
							<img src="https://opeturmo.com/images_parapente/social-icons/Paypal-Button.png" alt="PAyPal" data-toggle="modal" data-target="#PaypalMontanitaModal">

							<div class="modal fade" id="PaypalMontanitaModal" tabindex="-1" role="dialog" aria-labelledby="PaypalMontanitaModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<h5 class="modal-title" id="PaypalMontanitaModalLabel">Modal title</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									  <span aria-hidden="true">&times;</span>
									</button>
								  </div>
								  <div class="modal-body">
									  
																		  <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
									<input type="hidden" name="cmd" value="_s-xclick">
									<input type="hidden" name="hosted_button_id" value="BZGUJPZ5EXJR6">
									<table>
									<tr><td><input type="hidden" name="on0" value="Parapente">Parapente</td></tr><tr><td><select name="os0">
										<option value="Parapente">Parapente $45.00 USD</option>
										<option value="Parapente a Motor">Parapente a Motor $55.00 USD</option>
									</select> </td></tr>
									</table>
									<input type="hidden" name="currency_code" value="USD">
									<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
									<img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1">
									</form>

									  
								  </div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary">Save changes</button>
								  </div>
								</div>
							  </div>
							</div>
							
							
			-->				
							
							
							
							</p>
						<p id="social_icons_detail">	
							<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Ecuador,+Ruta+del+Spondylus+E15" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>
														
							<a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Montanita_Santa_Elena_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapentemontanita/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteMontanitaEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						<div class="col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluye" role="button" aria-expanded="false" aria-controls="collapseIncluye">
								INCLUYE: <img class="down-icon" src="svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluye">
							    <div class="card card-body">
								<ul>			
									<li>Piloto guía</li>
									<li>Equipo completo</li>
									<li>Seguro de aventura</li>
									<li>Transporte local a la pista. </li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				  </div>
				</div>
			</div>
		  <div class="card col-md-12">
		  <div id="collapsePLopez" class="collapse" aria-labelledby="headingPLopez" data-parent="#accordion_vuelos">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselPLopez" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							    <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez.jpg" alt="Parapente Puerto Lopez Ecuador">
								<div class="carousel-caption">
								  <h2>Parapente en Puerto Lopez</h2>
								</div>
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/vuelos-parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
								<div class="carousel-caption">
								  <h2>Parapente en Puerto Lopez</h2>
								</div>						
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
								<div class="carousel-caption">
								  <h2>Parapente en Puerto Lopez</h2>
								</div>
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
							
								<div class="carousel-caption">
								  <h2>Parapente en Puerto Lopez</h2>
								</div>
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-flights-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
								<div class="carousel-caption">
								  <h2>Parapente en Puerto Lopez</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselPLopez" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselPLopez" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselPLopez" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
							<li data-target="#carouselPLopez" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/vuelos-parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
							<li data-target="#carouselPLopez" data-slide-to="2"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
							<li data-target="#carouselPLopez" data-slide-to="3"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
							<li data-target="#carouselPLopez" data-slide-to="4"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-flights-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Parapente Puerto L&oacute;pez</h1></header>
						<p>Volamos en parapente con el piloto gu&iacute;a, que te lleva a realizar la actividad de aventura del parapente.  El gu&iacute;a se encarga de toda la operaci&oacute;n, solo tienes que disfrutar y cumplir tu sue&ntilde;o de volar. </p>
						<p>Salimos del punto de encuentro hacia la pista de parapente de puerto L&oacute;pez en el veh&iacute;culo 4x4, el cual est&aacute; ubicado dentro del parque nacional Machalilla y estos nos toma unos 20 minutos. Desde arriba en el despegue se observa la bah&iacute;a de puerto L&oacute;pez y gran parte del parque como la isla de Salango, el rio de agua blanca, etc. y cuando est&aacute; despejado el cielo se observa la isla de la plata.</p>
						<p>Recibimos una charla t&eacute;cnica del gu&iacute;a antes del vuelo para explicarles un poco como se realiza la actividad.</p>
						<p>Volamos desde el parque nacional Machalilla sobre la ruta del spondylus y aterrizamos en la playa dentro de la bah&iacute;a de puerto L&oacute;pez la capital del cielo.</p>
						<p>En caso de las condiciones meteorol&oacute;gicas no est&eacute;n aptas para volar en Puerto L&oacute;pez tendremos la opci&oacute;n de trasladarnos a otro lugar para realizar el vuelo en parapente a 10 minutos de Puerto Lop&eacute;z.</p>
						<p>Atendemos desde las 10:30am hasta las 17:00 </p>
						
					<div class="row ">
						<div class="col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a class="content-tablet" href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/VxkO" class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md"  data-button-text="Reservar" >Parapente Puerto Lopez</a>
							</p>
							<p id="social_icons_detail">
							<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Puerto+Lopez" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>	</a>			
															
							<a href="https://www.tripadvisor.com/Attraction_Review-g635730-d11872462-Reviews-Opeturmo-Puerto_Lopez_Manabi_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapentepuertolopez/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapentePuertoLopezEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						
						<div class="col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluye2" role="button" aria-expanded="false" aria-controls="collapseIncluye2">
								INCLUYE: <img class="down-icon" src="svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluye2">
							    <div class="card card-body">
								<ul>			
									<li>Piloto guía</li>
									<li>Equipo completo</li>
									<li>Seguro de aventura</li>
									<li>Transporte local a la pista. </li>
								</ul>
								</div>
							</div>
						</div>
					</div>
						
						</div>
				  </div>
			</div>
		  
		  
			
			
			
		  </div>
		  <div class="card col-md-12">
			
			<div id="collapseGuayaquil" class="collapse" aria-labelledby="headingGuayaquil" data-parent="#accordion_vuelos">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselGuayaquil" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-guayaquil/vuelos-parapente-guayaquil.jpg" alt="Parapente Guayaquil Ecuador">
						       <div class="carousel-caption">
								  <h2>Parapente en Guayaquil</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-guayaquil/parapente-guayaquil-ecuador.jpg" alt="Parapente Guayaquil Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Guayaquil</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselGuayaquil" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselGuayaquil" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselGuayaquil" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-guayaquil/vuelos-parapente-guayaquil.jpg" alt="Parapente Guayaquil Ecuador"></li>
							<li data-target="#carouselGuayaquil" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-guayaquil/parapente-guayaquil-ecuador.jpg" alt="Parapente Guayaquil Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Parapente Guayaquil</h1></header>
						<p>Estamos en Vía a la Costa 13km y medio. La aventura que empieza desde la subida en el carro 4x4 por la cordillera Chongon - Colonche “cerro blanco” hacia la pista de parapente “Bototillo” que tiene una altura de 300 metros sobre el nivel del mar, volando aquí podemos observar la cuidad de Guayaquil, sus manglares, golfo y el Rio Guayas.</p>
						<p>Recibimos una charla de técnica antes del vuelo.</p>
						<p>Atendemos desde las 10:30am hasta las 17:00 </p>
						<div class="row ">
						
						<div class="col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a class="content-tablet" href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/Oa0q" class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Reservar">Parapente Guayaquil</a>
							</p>
							<p id="social_icons_detail">
							<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Guayaquil" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>		</a>													
							<a href="https://www.tripadvisor.com/Attraction_Review-g303845-d11851293-Reviews-Opeturmo-Guayaquil_Guayas_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapenteguayaquil/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteGuayaquilEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						
						<div class="col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluye1" role="button" aria-expanded="false" aria-controls="collapseIncluye1">
								INCLUYE: <img class="down-icon" src="svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluye1">
							    <div class="card card-body">
								<ul>			
									<li>Piloto guía</li>
									<li>Equipo completo</li>
									<li>Seguro de aventura</li>
									<li>Transporte local a la pista. </li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				  </div>
			</div>
			
			
		  </div>
		  <div class="card col-md-12">
			<div id="collapseQuito" class="collapse" aria-labelledby="headingQuito" data-parent="#accordion_vuelos">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselQuito" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-quito-ecuador.jpg" alt="Parapente Quito Ecuador">
								<div class="carousel-caption">
								  <h2>Parapente en Quito</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-paragliding-quito-ecuador.jpg" alt="Parapente Quito Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Quito</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselQuito" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselQuito" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselQuito" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-quito-ecuador.jpg" alt="Parapente Quito Ecuador"></li>
							<li data-target="#carouselQuito" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-paragliding-quito-ecuador.jpg" alt="Parapente Quito Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Parapente Quito</h1></header>
						<p>Despegamos desde el cerro de Lumbisi hacia la Ciudad, observamos el paisaje de la ciudad de Quito se puede observar las monta&ntilde;as. Aterrizamos en la falda del cerro.</p>
						<p>El punto de  encuentro es en vía a Lumbisí km 1 en la entrada a La Primavera a las 8:30 am. Desde ahí subimos en un vehículo a la montaña.</p>
						
					
					<div class="row ">
						<div class="col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a class="content-tablet" href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/2VMj" class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Reservar">Parapente Quito</a>
						</p>
							<p id="social_icons_detail">
							<!-- <a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Ecuador,+Ruta+del+Spondylus+E15" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>	</a>					
							-->
							<a href="https://www.tripadvisor.com/Attraction_Review-g294308-d10113679-Reviews-Opeturmo-Quito_Pichincha_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapentequitoecuador/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteQuitoEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						
						<div class="col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluye3" role="button" aria-expanded="false" aria-controls="collapseIncluye3">
								INCLUYE: <img class="down-icon" src="svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluye3">
							    <div class="card card-body">
								<ul>			
									<li>Piloto guía</li>
									<li>Equipo completo</li>
									<li>Seguro de aventura</li>
									<li>Transporte local a la pista. </li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				  </div>
			</div>
		  </div>
		  
		  <div class="card col-md-12">
			<div id="collapseIbarra" class="collapse" aria-labelledby="headingIbarra" data-parent="#accordion_vuelos">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselIbarra" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-ibarra.jpg" alt="Parapente Ibarra Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Ibarra</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-paragliding-ibarra-ecuador.jpg" alt="Parapente Ibarra Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Ibarra</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselIbarra" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselIbarra" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselIbarra" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-ibarra.jpg" alt="Parapente Ibarra Ecuador"></li>
							<li data-target="#carouselIbarra" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-paragliding-ibarra-ecuador.jpg" alt="Parapente Ibarra Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Parapente Ibarra</h1></header>
						<p>Volando en Ibarra se puede apreciar desde el airela laguna de yahuarcocha, las hermosas monta&ntilde;as de la sierra ecuatoriana, los Volcanes Imbabura y Cotacachi.</p>
						<p>Los vuelos en Ibarra son temprano en la ma&ntilde;ana.</p>
						
					
					<div class="row ">
						<div class="col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a class="content-tablet" href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/NBb9" class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Reservar">Parapente Ibarra</a>
							</p>
							<p id="social_icons_detail">
							<!--<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Ecuador,+Ruta+del+Spondylus+E15" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/></a>
								-->							
							<a href="https://www.tripadvisor.com/Attraction_Review-g312858-d11855169-Reviews-Opeturmo-Ibarra_Imbabura_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapenteibarra/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteIbarraEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						
						<div class="col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluye4" role="button" aria-expanded="false" aria-controls="collapseIncluye4">
								INCLUYE: <img class="down-icon" src="svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluye4">
							    <div class="card card-body">
								<ul>			
									<li>Piloto guía</li>
									<li>Equipo completo</li>
									<li>Seguro de aventura</li>
									<li>Transporte local a la pista. </li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		  </div>
		  <div class="card col-md-12">
			<div id="collapseOtros" class="collapse" aria-labelledby="headingOtros" data-parent="#accordion_vuelos">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselOtros" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-canoa/vuelos-parapente-canoa-ecuador.jpg" alt="Parapente Canoa Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Canoa</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-cuenca/vuelos-parapente-cuenca.jpg" alt="Parapente Cuenca Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Ba&ntilde;os</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselOtros" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselOtros" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselOtros" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-canoa/vuelos-parapente-canoa-ecuador.jpg" alt="Parapente Canoa Ecuador"></li>
							<li data-target="#carouselOtros" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-cuenca/vuelos-parapente-cuenca.jpg" alt="Parapente Cuenca Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
						
						<header><h1>Parapente Ba&ntilde;os</h1></header>
						<p>Volamos en Niton observando el paisaje Ba&ntilde;os donde se puede ver el volc&aacute;n Tungurahua.</p>
						<header><h1>Parapente Cuenca</h1></header>
						<p>En Cuenca se vuela en Paute sobre las monta&ntilde;as de la sierra ecuatoriana.</p>
						<header><h1>Parapente Ancon - Salinas</h1></header>
						<p>Volamos en los riscos de Anc&oacute;n cerca de Salinas</p>						
						<header><h1>Parapente San Pedro</h1></header>
						<p>Volamos Playa Bruja cerca de San Pedro en Santa Elena</p>
						<header><h1>Parapente Canoa</h1></header>
						<p>Volamos sobre la playa de Canoa en Manab&iacute; se puede ver bahia de caraquez y su bah&iacute; junto las monta&ntilde;as con su vegetacion y la hermosa vista de la playa con el mar.</p>
						<header><h1>Parapente Crucita</h1></header>
						<p>Volamos en viendo a Crucita junto con su exntensa playa y el mar junto al perfil costanero de Manab&iacute;.</p>
						
					<div class="row ">
						<div class="col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a class="content-tablet" href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/0BeV" class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Reservar">Parapente Canoa</a></p>
							
							<div class="row">
															
								<div class="accordion" id="accordionSocial">

								<div class="row">
								<div class="col-3">
								  <div id="headingDir">
									  <h5 class="mb-0">
										<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseDir" aria-expanded="true" aria-controls="collapseDir">
										<img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>				
										</button>
									  </h5>
									</div>
								  </div>
								  <div class="col-3">
								  <div id="headingTripadvisor">
									  <h5 class="mb-0">
										<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTripadvisor" aria-expanded="true" aria-controls="collapseTripadvisor">
										 <img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/>					
										</button>
									  </h5>
									</div>
								  </div>
								  
								<div class="col-3">
									<div  id="headingInstagram">
									  <h5 class="mb-0">
										<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseInstagram" aria-expanded="false" aria-controls="collapseInstagram">
										<img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>
															
										</button>
									  </h5>
									</div>
								  
								</div>  
								<div class="col-3">
								  
									<div  id="headingFace">
									  <h5 class="mb-0">
										<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFace" aria-expanded="false" aria-controls="collapseFace">
										<img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>
															
										</button>
									  </h5>
									</div>
								</div>
								</div>
								  
								  <div class="row">
								  <div class="col-12">
								  <div class="card">
									

									<div id="collapseDir" class="collapse " aria-labelledby="headingDir" data-parent="#accordionSocial">
									  <div class="card-body">
											<div class="row">
											  <div class="col-6">
												<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Canoa" target="blank"><p><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>Canoa</p></a>
											  </div>
										</div>				
										
									  </div>
									</div>
								  </div>
								  <div class="card">
									

									<div id="collapseTripadvisor" class="collapse " aria-labelledby="headingTripadvisor" data-parent="#accordionSocial">
									  <div class="card-body">
										  <div class="row">
											  <div class="col-6">
											  <a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Banos_Tungurahua_Province.html" target="blank"><p><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/>Ba&ntilde;os</p></a>
											  <a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Cuenca_Azuay_Province.html" target="blank"><p><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/>Cuenca</p></a>
											  </div>
											  <div class="col-6">
											 <!-- <p><a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Montanita_Santa_Elena_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>Canoa</p>
											  <p><a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Montanita_Santa_Elena_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>Crucita</p>
											 --> </div>
										</div>
									</div>
								  </div>
								  </div>
								  <div class="card">
									<div id="collapseInstagram" class="collapse" aria-labelledby="headingInstagram" data-parent="#accordionSocial">
									  <div class="card-body">
									   <div class="row">
											  <div class="col-6">
											  <a href="https://www.instagram.com/parapentebanos/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Ba&ntilde;os</p></a>
											  <a href="https://www.instagram.com/cuencaparapente/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Cuenca</p></a>
											  <a href="https://www.instagram.com/parapentesalinas/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Ancon</p></a>
											  </div>
											  <div class="col-6">
											  <a href="https://www.instagram.com/parapentemontanita/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>San Pedro</p></a>
											  <a href="https://www.instagram.com/parapentecanoa/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Canoa</p></a>
											  <a href="https://www.instagram.com/parapentecrucita/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Crucita</p></a>
											  </div>
										</div>
									</div>
								  </div>
								  </div>
								  <div class="card">
									<div id="collapseFace" class="collapse" aria-labelledby="headingFace" data-parent="#accordionSocial">
									  <div class="card-body">
									  <div class="row">
									  <div class="col-6">
									 <a href="https://www.facebook.com/ParapenteBanosEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Ba&ntilde;os</p></a>
									 <a href="https://www.facebook.com/ParapenteCuencaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Cuanca</p></a>
									 <a href="https://www.facebook.com/ParapenteAnconSalinasEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Ancon</p></a>
									  </div>
									  <div class="col-6">
									 <a href="https://www.facebook.com/ParapenteMontanitaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>San Pedro</p></a>
									 <a href="https://www.facebook.com/ParapenteCanoaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Canoa</p></a>
									 <a href="https://www.facebook.com/ParapenteCrucitaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Crucita</p></a>
									  </div>
									  </div>
									  </div>
									</div>
								  </div>
								</div>
								</div>
								</div>							
							
							</div>							
							
							
							
							
						</div>
							
							
							<div class="col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluye5" role="button" aria-expanded="false" aria-controls="collapseIncluye5">
								INCLUYE: <img class="down-icon" src="svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluye5">
							    <div class="card card-body">
								<ul>			
									<li>Piloto guía</li>
									<li>Equipo completo</li>
									<li>Seguro de aventura</li>
									<li>Transporte local a la pista. </li>
								</ul>
								</div>
							</div>
						</div>
							
							
							
						</div>
						
						</div>
						
				</div>
			</div>
		  </div>
		  
		  </div>
		  
		</div>
    </div>
	
	
</div> 
<!-- end desktop -->









<div class="content-mobile">




 
 <div class="row header_row">
		<div class="col-12">
		<header><h1>Vuelos Parapente Ecuador</h1></header>
		</div>
	</div>
<div class="row">
	<div class="accordion" id="accordion_vuelos_mobile">
		 <div class="row vuelos">
			<div class="col-6 col-sm-2 col-md-2 vuelos_box">
			<div class="card card-vuelos collapsed" data-toggle="collapse" data-target="#collapseMontanitaMobile" aria-expanded="true" aria-controls="collapseMontanitaMobile">
			    <header><h1>Montanita</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/vuelos-parapente-playa-montanita-ecuador.png" alt="Parapente Montanita Ecuador">
				<button>Info...</button>
			</div>
			</div>
			<div class="col-6 col-sm-2 col-md-2 vuelos_box">
			
			<div class="card  card-vuelos collapsed" data-toggle="collapse" data-target="#collapsePLopezMobile" aria-expanded="false" aria-controls="collapsePLopezMobile">
				 <header><h1>Puerto Lopez</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/vuelos-parapente-puerto-lopez-ecuador.png" alt="Parapente Puerto Lopez Ecuador">
				<button>Info...</button>
			</div>
			
			
			</div>
			</div>
			
			
		 <div class="row vuelos_details">
			<div class="card col-md-12">
				<div id="collapseMontanitaMobile" class="collapse" aria-labelledby="headingMontanita" data-parent="#accordion_vuelos_mobile">
				  <div class="card-body row">
					<div class="col-md-4">
						
						
						<div id="carouselMontanitaMobile" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-playa-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Monta&ntilde;ita</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/playa-parapente-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Monta&ntilde;ita</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-montanita.jpg" alt="Parapente Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Monta&ntilde;ita</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-con-motor-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Monta&ntilde;ita con motor</h2>
								</div>							 
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-a-motor-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Monta&ntilde;ita con motor</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselMontanitaMobile" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-playa-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador"></li>
							<li data-target="#carouselMontanitaMobile" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/playa-parapente-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador"></li>
							<li data-target="#carouselMontanitaMobile" data-slide-to="2"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-montanita.jpg" alt="Parapente Montanita Ecuador"></li>
							<li data-target="#carouselMontanitaMobile" data-slide-to="3"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-con-motor-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador"></li>
							<li data-target="#carouselMontanitaMobile" data-slide-to="4"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-a-motor-montanita-ecuador.jpg" alt="Parapente Montanita Ecuador"></li>
						  </ol>
						
						</div>
						  
											
						
					</div>
					<div class="col-md-8">
					<header><h1>Parapente Monta&ntilde;ita</h1></header>
					<!--<p>Volamos Parapente frente al mar sobre la ruta del Spondylus en el cerro Playa Bruja en la comuna de Libertador Bolívar a 10 minutos antes de Montanita.</p>-->
					<p>Volamos en parapente con el piloto gu&iacute;a, que te lleva a realizar la actividad de aventura, frente al mar sobre la Ruta del Spondylus (Ruta del Sol). El gu&iacute;a se encarga de toda la operaci&oacute;n, solo tienes que disfrutar y cumplir tu sue&ntilde;o de volar. La experiencia del vuelo dura entre 12- 15 minutos. </p>
					<li><b>Parapente Libre</b> Vuelos en parapente frente al mar es un vuelo din&aacute;mico que se realiza desplaz&aacute;ndose por los cielos aprovechando la ladera de la monta&ntilde;a el viento del mar nos permite poder sustentarnos en el aire y volar como los p&aacute;jaros. </li>
					<p><b>Promociones:</b></p>
					<ul>
					<li>Vuelan en Parpente 6 personas pagan 5</li>
					<li>Cumplea&ntilde;eros gratis las fotos</li>
					</ul>
					<li><b>Parapente a Motor</b> Un vuelo en Parapente motorizado se puede recorrer mayor distancia en el mismo tiempo, ascendemos a mayor altura para apreciar el increíble océano pacífico y el continente del perfil ecuatoriano</li>
					<p>Atendemos desde las 11:00am hasta las 6:00pm, pero este horario puede variar dependiendo la epoca del a&ntilde;o y del dia. </p>
										
								
					<div class="row ">
						<div class="col-12 col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/bDMZ" class="peek-book-btn-blue peek-book-button-flat peek-book-button-sm" data-button-text="Reservar">Parapente Montanita</a>
							</p>
						
							<p id="social_icons_detail">	
							<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Ecuador,+Ruta+del+Spondylus+E15" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>
														
							<a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Montanita_Santa_Elena_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapentemontanita/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteMontanitaEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						<div class="col-12 col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluyeMobile" role="button" aria-expanded="false" aria-controls="collapseIncluyeMobile">
								INCLUYE: <img class="down-icon" src="svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluyeMobile">
							    <div class="card card-body">
								<ul>			
									<li>Piloto guía</li>
									<li>Equipo completo</li>
									<li>Seguro de aventura</li>
									<li>Transporte local a la pista. </li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				  </div>
				</div>
			</div>
		  <div class="card col-md-12">
		  <div id="collapsePLopezMobile" class="collapse" aria-labelledby="headingPLopez" data-parent="#accordion_vuelos_mobile">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselPLopezMobile" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez.jpg" alt="Parapente Puerto Lopez Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Puerto Lopez</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/vuelos-parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Puerto Lopez</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Puerto Lopez</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Puerto Lopez</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-flights-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Puerto Lopez</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselPLopezMobile" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselPLopezMobile" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselPLopezMobile" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
							<li data-target="#carouselPLopezMobile" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/vuelos-parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
							<li data-target="#carouselPLopezMobile" data-slide-to="2"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
							<li data-target="#carouselPLopezMobile" data-slide-to="3"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
							<li data-target="#carouselPLopezMobile" data-slide-to="4"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-flights-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
						  </ol>
					
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Parapente Puerto L&oacute;pez</h1></header>
						<p>Volamos en parapente con el piloto gu&iacute;a, que te lleva a realizar la actividad de aventura del parapente.  El gu&iacute;a se encarga de toda la operaci&oacute;n, solo tienes que disfrutar y cumplir tu sue&ntilde;o de volar. </p>
						<p>Salimos del punto de encuentro hacia la pista de parapente de puerto L&oacute;pez en el veh&iacute;culo 4x4, el cual est&aacute; ubicado dentro del parque nacional Machalilla y estos nos toma unos 20 minutos. Desde arriba en el despegue se observa la bah&iacute;a de puerto L&oacute;pez y gran parte del parque como la isla de Salango, el rio de agua blanca, etc. y cuando est&aacute; despejado el cielo se observa la isla de la plata.</p>
						<p>Recibimos una charla t&eacute;cnica del gu&iacute;a antes del vuelo para explicarles un poco como se realiza la actividad.</p>
						<p>Volamos desde el parque nacional Machalilla sobre la ruta del spondylus y aterrizamos en la playa dentro de la bah&iacute;a de puerto L&oacute;pez la capital del cielo.</p>
						<p>En caso de las condiciones meteorol&oacute;gicas no est&eacute;n aptas para volar en Puerto L&oacute;pez tendremos la opci&oacute;n de trasladarnos a otro lugar para realizar el vuelo en parapente a 10 minutos de Puerto Lop&eacute;z.</p>
						<p>Atendemos desde las 10:30am hasta las 17:00 </p>
						
						
					<div class="row ">
						<div class="col-12 col-md-6 col-sm-6 col-xs-6 buttons_row">
						<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/VxkO" class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-sm"  data-button-text="Reservar" >Reservar</a>
							</p>
						
							<p id="social_icons_detail">
							<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Puerto+Lopez" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>	</a>			
															
							<a href="https://www.tripadvisor.com/Attraction_Review-g635730-d11872462-Reviews-Opeturmo-Puerto_Lopez_Manabi_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapentepuertolopez/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapentePuertoLopezEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						
						<div class="col-12 col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluyeMobile2" role="button" aria-expanded="false" aria-controls="collapseIncluyeMobile2">
								INCLUYE: <img class="down-icon" src="svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluyeMobile2">
							    <div class="card card-body">
								<ul>			
									<li>Piloto guía</li>
									<li>Equipo completo</li>
									<li>Seguro de aventura</li>
									<li>Transporte local a la pista. </li>

								</ul>
								</div>
							</div>
						</div>
					</div>
						
						</div>
				  </div>
			</div>
		  
			
			
			
		  </div>
		  </div>
		  
			
			
			<div class="row vuelos">
			<div class="col-6 col-sm-2 col-md-2 vuelos_box">
			
			<div class="card card-vuelos collapsed" data-toggle="collapse" data-target="#collapseGuayaquilMobile" aria-expanded="false" aria-controls="collapseGuayaquilMobile">
				 <header><h1>Guayaquil</h1></header>	  
				<img class=" " src="https://opeturmo.com/images_parapente/parapente-guayaquil/vuelos-parapente-guayaquil-ecuador.png" alt="Parapente Guayaquil Ecuador">
				<button>Info...</button>
			</div>
			</div>
			<div class="col-6 col-sm-2 col-md-2 vuelos_box">
			<div class="card card-vuelos collapsed" data-toggle="collapse" data-target="#collapseQuitoMobile" aria-expanded="false" aria-controls="collapseQuitoMobile">
				 <header><h1>Quito</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-quito-ecuador.png" alt="Parapente Quito Ecuador">
				<button>Info...</button>
			</div>
			</div>
			</div>
			
			
			

		   <div class="row vuelos_details">
		  <div class="card col-md-12">
		  <div id="collapseGuayaquilMobile" class="collapse" aria-labelledby="headingGuayaquil" data-parent="#accordion_vuelos_mobile">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselGuayaquilMobile" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							   <img class="" src="https://opeturmo.com/images_parapente/parapente-guayaquil/vuelos-parapente-guayaquil.jpg" alt="Parapente Guayaquil Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Guayaquil</h2>
								</div>							
							</div>
							<div class="carousel-item">
							   <img class="" src="https://opeturmo.com/images_parapente/parapente-guayaquil/parapente-guayaquil-ecuador.jpg" alt="Parapente Guayaquil Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Guayaquil</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselGuayaquil" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselGuayaquil" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselGuayaquilMobile" data-slide-to="0" class="active"> <img class="" src="https://opeturmo.com/images_parapente/parapente-guayaquil/vuelos-parapente-guayaquil.jpg" alt="Parapente Guayaquil Ecuador"></li>
							<li data-target="#carouselGuayaquilMobile" data-slide-to="1"> <img class="" src="https://opeturmo.com/images_parapente/parapente-guayaquil/parapente-guayaquil-ecuador.jpg" alt="Parapente Guayaquil Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Parapente Guayaquil</h1></header>
						<p>Estamos en Vía a la Costa 13km y medio. La aventura que empieza desde la subida en el carro 4x4 por la cordillera Chongon - Colonche “cerro blanco” hacia la pista de parapente “Bototillo” que tiene una altura de 300 metros sobre el nivel del mar, volando aquí podemos observar la cuidad de Guayaquil, sus manglares, golfo y el Rio Guayas.</p>
						<p>Recibimos una charla de técnica antes del vuelo.</p>
						<p>Atendemos desde las 10:30am hasta las 17:00 </p>
						
					<div class="row ">
						<div class="col-12 col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/Oa0q" class="peek-book-btn-blue peek-book-button-flat peek-book-button-sm" data-button-text="Reservar">Parapente Guayaquil</a>
						</p>
						
							<p id="social_icons_detail">
							<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Guayaquil" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>	</a>	
							
															
							<a href="https://www.tripadvisor.com/Attraction_Review-g303845-d11851293-Reviews-Opeturmo-Guayaquil_Guayas_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapenteguayaquil/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteGuayaquilEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						
						<div class="col-12 col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluyeMobile1" role="button" aria-expanded="false" aria-controls="collapseIncluyeMobile1">
								INCLUYE: <img class="down-icon" src="svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluyeMobile1">
							    <div class="card card-body">
								<ul>			
									<li>Piloto guía</li>
									<li>Equipo completo</li>
									<li>Seguro de aventura</li>
									<li>Transporte local a la pista. </li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				  </div>
			</div>
		  
			
			
			
		  </div>
		  <div class="card col-md-12">
			<div id="collapseQuitoMobile" class="collapse" aria-labelledby="headingQuito" data-parent="#accordion_vuelos_mobile">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselQuitoMobile" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-quito-ecuador.jpg" alt="Parapente Quito Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Quito</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-paragliding-quito-ecuador.jpg" alt="Parapente Quito Ecuador">
								<div class="carousel-caption">
								  <h2>Parapente en Quito</h2>
								</div>							
							</div>
							</div>
						  <!-- <a class="carousel-control-prev" href="#carouselQuito" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselQuito" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselQuitoMobile" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-quito-ecuador.jpg" alt="Parapente Quito Ecuador"></li>
							<li data-target="#carouselQuitoMobile" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-paragliding-quito-ecuador.jpg" alt="Parapente Quito Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Parapente Quito</h1></header>
						<p>Despegamos desde el cerro de Lumbisi hacia la Ciudad, observamos el paisaje de la ciudad de Quito se puede observar las monta&ntilde;as. Aterrizamos en la falda del cerro.</p>
						<p>El punto de  encuentro es en vía a Lumbisí km 1 en la entrada a La Primavera a las 8:30 am. Desde ahí subimos en un vehículo a la montaña.</p>
						
					
					<div class="row ">
						<div class="col-12 col-12 col-md-6 col-sm-6 col-xs-6 buttons_row">
						<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/2VMj" class="peek-book-btn-blue peek-book-button-flat peek-book-button-sm" data-button-text="Reservar">Parapente Quito</a>
							</p>
						
								<p id="social_icons_detail">
								<!--
								<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Ecuador,+Ruta+del+Spondylus+E15" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>	
							-->
														
							<a href="https://www.tripadvisor.com/Attraction_Review-g294308-d10113679-Reviews-Opeturmo-Quito_Pichincha_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapentequitoecuador/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteQuitoEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						<div class="col-12 col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluyeMobile3" role="button" aria-expanded="false" aria-controls="collapseIncluyeMobile3">
								INCLUYE: <img class="down-icon" src="svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluyeMobile3">
							    <div class="card card-body">
								<ul>			
									<li>Piloto guía</li>
									<li>Equipo completo</li>
									<li>Seguro de aventura</li>
									<li>Transporte local a la pista. </li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				  </div>
			</div>
		  </div>
		  
		  </div>
		  
		  
		  
			<div class="row vuelos">
			<div class="col-6 col-sm-2 col-md-2 vuelos_box">
			<div class="card  card-vuelos collapsed" data-toggle="collapse" data-target="#collapseIbarraMobile" aria-expanded="false" aria-controls="collapseIbarraMobile">
				  <header><h1>Ibarra</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-ibarra-ecuador.png" alt="Parapente Ibarra Ecuador">
				<button>Info...</button>
			</div>
			</div>
			<div class="col-6 col-sm-2  col-md-2 vuelos_box">
			<div class="card  card-vuelos collapsed" data-toggle="collapse" data-target="#collapseOtrosMobile" aria-expanded="false" aria-controls="collapseOtrosMobile">
				 <header><h1>Otros lugares</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-crucita/vuelos-parapente-crucita-ecuador.png" alt="Parapente Canoa Ecuador">
				<button>Info...</button>
			</div>
		</div>
		</div>
		

		  
		   <div class="row vuelos_details">
		  <div class="card col-md-12">
			<div id="collapseIbarraMobile" class="collapse" aria-labelledby="headingIbarra" data-parent="#accordion_vuelos_mobile">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselIbarraMobile" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-ibarra.jpg" alt="Parapente Ibarra Ecuador">
								<div class="carousel-caption">
								  <h2>Parapente en Ibarra</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-paragliding-ibarra-ecuador.jpg" alt="Parapente Ibarra Ecuador">
							<div class="carousel-caption">
								  <h2>Parapente en Ibarra</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselIbarra" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselIbarra" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselIbarraMobile" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-ibarra.jpg" alt="Parapente Ibarra Ecuador"></li>
							<li data-target="#carouselIbarraMobile" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-paragliding-ibarra-ecuador.jpg" alt="Parapente Ibarra Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Parapente Ibarra</h1></header>
						<p>Volando en Ibarra se puede apreciar desde el airela laguna de yahuarcocha, las hermosas monta&ntilde;as de la sierra ecuatoriana, los Volcanes Imbabura y Cotacachi.</p>
						<p>Los vuelos en Ibarra son temprano en la ma&ntilde;ana.</p>
						
					
					<div class="row ">
						<div class="col-12 col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/NBb9" class="peek-book-btn-blue peek-book-button-flat peek-book-button-sm" data-button-text="Reservar">Parapente Ibarra</a>
							</p>
						
						<p id="social_icons_detail">
															
							<a href="https://www.tripadvisor.com/Attraction_Review-g312858-d11855169-Reviews-Opeturmo-Ibarra_Imbabura_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapenteibarra/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteIbarraEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						
						<div class="col-12 col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluyeMobile4" role="button" aria-expanded="false" aria-controls="collapseIncluyeMobile4">
								INCLUYE: <img class="down-icon" src="svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluyeMobile4">
							    <div class="card card-body">
								<ul>			
									<li>Piloto guía</li>
									<li>Equipo completo</li>
									<li>Seguro de aventura</li>
									<li>Transporte local a la pista. </li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		  </div>
		  <div class="card col-md-12">
			<div id="collapseOtrosMobile" class="collapse" aria-labelledby="headingOtros" data-parent="#accordion_vuelos_mobile">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselOtrosMobile" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-canoa/vuelos-parapente-canoa-ecuador.jpg" alt="Parapente Canoa Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Canoa</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-cuenca/vuelos-parapente-cuenca.jpg" alt="Parapente Cuenca Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Ba&ntilde;os</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselOtros" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselOtros" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselOtrosMobile" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-canoa/vuelos-parapente-canoa-ecuador.jpg" alt="Parapente Canoa Ecuador"></li>
							<li data-target="#carouselOtrosMobile" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-cuenca/vuelos-parapente-cuenca.jpg" alt="Parapente Cuenca Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
						
						<header><h1>Parapente Ba&ntilde;os</h1></header>
						<p>Volamos en Niton observando el paisaje Ba&ntilde;os donde se puede ver el volc&aacute;n Tungurahua.</p>
						<header><h1>Parapente Cuenca</h1></header>
						<p>En Cuenca se vuela en Paute sobre las monta&ntilde;as de la sierra ecuatoriana.</p>
						<header><h1>Parapente Ancon - Salinas</h1></header>
						<p>Volamos en los riscos de Anc&oacute;n cerca de Salinas</p>
						<header><h1>Parapente San Pedro</h1></header>
						<p>Volamos Playa Bruja cerca de San Pedro en Santa Elena</p>
						<header><h1>Parapente Canoa</h1></header>
						<p>Volamos sobre la playa de Canoa en Manab&iacute; apreciando las monta&ntilde;as con su vegetacion y la hermosa vista de la playa con el mar.</p>
						<header><h1>Parapente Crucita</h1></header>
						<p>Volamos en viendo a Crucita junto con su exntensa playa y el mar junto al perfil costanero de Manab&iacute;.</p>
						
					
					<div class="row ">
						<div class="col-12 col-sm-6 col-md-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/0BeV" class="peek-book-btn-blue peek-book-button-flat peek-book-button-sm" data-button-text="Reservar">Parapente Canoa</a>
							</p>
						
							<div class="row">
							<div class="col-12">							
								
							<div class="accordion" id="accordionSocialMobile">

								<div class="row">
								<div class="col-3">
								  <div id="headingDirMobile">
									  <h5 class="mb-0">
										<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseDirMobile" aria-expanded="true" aria-controls="collapseDirMobile">
										<img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>				
										</button>
									  </h5>
									</div>
								  </div>
								  <div class="col-3">
								  <div id="headingTripadvisorMobile">
									  <h5 class="mb-0">
										<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTripadvisorMobile" aria-expanded="true" aria-controls="collapseTripadvisorMobile">
										 <img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/>					
										</button>
									  </h5>
									</div>
								  </div>
								  
								<div class="col-3">
									<div  id="headingInstagramMobile">
									  <h5 class="mb-0">
										<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseInstagramMobile" aria-expanded="false" aria-controls="collapseInstagramMobile">
										<img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>
															
										</button>
									  </h5>
									</div>
								  
								</div>  
								<div class="col-3">
								  
									<div  id="headingFaceMobile">
									  <h5 class="mb-0">
										<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFaceMobile" aria-expanded="false" aria-controls="collapseFaceMobile">
										<img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>
															
										</button>
									  </h5>
									</div>
								</div>
								</div>
								  
								  <div class="row">
								  <div class="col-12">
								  <div class="card">
									

									<div id="collapseDirMobile" class="collapse " aria-labelledby="headingDirMobile" data-parent="#accordionSocialMobile">
									  <div class="card-body">
											<div class="row">
											 <div class="col-6">
												<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Canoa" target="blank"><p><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>Canoa</p></a>
											  </div>
										</div>				
										
									  </div>
									</div>
								  </div>
								  <div class="card">
									

									<div id="collapseTripadvisorMobile" class="collapse " aria-labelledby="headingTripadvisorMobile" data-parent="#accordionSocialMobile">
									  <div class="card-body">
										  <div class="row">
											   <div class="col-6">
											  <a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Banos_Tungurahua_Province.html" target="blank"><p><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/>Ba&ntilde;os</p></a>
											  <a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Cuenca_Azuay_Province.html" target="blank"><p><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/>Cuenca</p></a>
											  </div>
											  <div class="col-6">
											 <!-- <p><a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Montanita_Santa_Elena_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>Canoa</p>
											  <p><a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Montanita_Santa_Elena_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>Crucita</p>
											 --> </div>
										</div>
									</div>
								  </div>
								  </div>
								  <div class="card">
									<div id="collapseInstagramMobile" class="collapse" aria-labelledby="headingInstagramMobile" data-parent="#accordionSocialMobile">
									  <div class="card-body">
									   <div class="row">
											    <div class="col-6">
											  <a href="https://www.instagram.com/parapentebanos/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Ba&ntilde;os</p></a>
											  <a href="https://www.instagram.com/cuencaparapente/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Cuenca</p></a>
											  <a href="https://www.instagram.com/parapentesalinas/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Ancon</p></a>
											  </div>
											  <div class="col-6">
											  <a href="https://www.instagram.com/parapentemontanita/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>San Pedro</p></a>
											  <a href="https://www.instagram.com/parapentecanoa/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Canoa</p></a>
											  <a href="https://www.instagram.com/parapentecrucita/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Crucita</p></a>
											  </div>
										</div>
									</div>
								  </div>
								  </div>
								  <div class="card">
									<div id="collapseFaceMobile" class="collapse" aria-labelledby="headingFaceMobile" data-parent="#accordionSocialMobile">
									  <div class="card-body">
									  <div class="row">
									  <div class="col-6">
									 <a href="https://www.facebook.com/ParapenteBanosEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Ba&ntilde;os</p></a>
									 <a href="https://www.facebook.com/ParapenteCuencaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Cuanca</p></a>
									 <a href="https://www.facebook.com/ParapenteAnconSalinasEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Ancon</p></a>
									  </div>
									  <div class="col-6">
									 <a href="https://www.facebook.com/ParapenteMontanitaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>San Pedro</p></a>
									 <a href="https://www.facebook.com/ParapenteCanoaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Canoa</p></a>
									 <a href="https://www.facebook.com/ParapenteCrucitaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Crucita</p></a>
									  </div>
									  </div>
									  </div>
									</div>
								  </div>
								</div>
								</div>
								</div>
								
							
							
							
							</div>							
							</div>							
							
							<div class="col-12 col-md-4 col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluyeMobile5" role="button" aria-expanded="false" aria-controls="collapseIncluyeMobile5">
								INCLUYE: <img class="down-icon" src="svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluyeMobile5">
							    <div class="card card-body">
								<ul>			
									<li>Piloto guía</li>
									<li>Equipo completo</li>
									<li>Seguro de aventura</li>
									<li>Transporte local a la pista. </li>
								</ul>
								</div>
							</div>
						</div>
							
							
						</div>
						
							
						</div>
							
							
							
							
							
							
						</div>
						
						
					</div>
					</div>
				</div>
			</div>
		  </div>
		  
		  </div>
		  
		</div>
    </div>
	



