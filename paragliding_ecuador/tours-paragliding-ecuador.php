
				<?php include 'modal-tour-paragliding-ecuador-mini.php'; ?>				
				<?php include 'modal-tour-paragliding-ecuador-coast.php'; ?>
				<?php include 'modal-tour-paragliding-ecuador-complete.php'; ?>	
				<?php include 'modal-tour-paragliding-ecuador-two-hemispheres.php'; ?>	
	
				<?php include 'modal-machalilla-ecuador.php'; ?>
				<?php include 'modal-montanita-ecuador.php'; ?>
				<?php include 'modal-galpagos-ecuador.php'; ?>
				<?php include 'modal-galpagos-diving-ecuador.php'; ?>
	
	
	
	
	<div class="row header_row">
		<div class="col-12">
			<header><h1>Adventure Tours Ecuador</h1></header>
			<header><h3>Adventure Touristic Activities</h3></header>
		</div>
	</div>
	
	

 
 
<div class="content-desktop">
	
	<div class="row tours">
		<div class="row tours_row">
			<div class="col-12 col-md-6 col-md-6 tours_img">
				<header><h3>Puerto Lopez</h3></header>
				<img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tours-isla-de-la-plata-parque-nacional-machalilla-puerto-lopez-ecuador.jpg" alt="Parque Nacional Machalilla Puerto Lopez Ecuador">
				
			</div>
			
			<div class="col-12 col-sm-6  col-md-6 ">
				<ul>
				<li  class="options_tours " data-toggle='modal' data-target='#IslaPlataModal' >Tour Isla de la Plata</li>
				<li  class="options_tours " data-toggle='modal' data-target='#CosteroModal' >Salango Costal Tour</li>
				<li  class="options_tours " data-toggle='modal' data-target='#BallenasModal' >Tour Whale Watching (June - August)</li>
				<li  class="options_tours" data-toggle='modal' data-target='#CabalgataMachalillaModal' >Tour Humed Forest Horseback Ride</li>
				<li  class="options_tours " data-toggle='modal' data-target='#BuceoPlataModal' >Diving Isla de la Plata</li>
				</ul>
			</div>
		</div>
		<div class="row tours_row">
			<div class="col-12 col-sm-6  col-md-6 ">
				<ul>
				<li  class="options_tours tours_li_left" data-toggle='modal' data-target='#DosMangasModal' >Tour Horseback Ride Dos Mangas</li>
				<li  class="options_tours tours_li_left" data-toggle='modal' data-target='#SurfModal' >Surf Clases</li>
				</ul>
			</div>
			<div class="col-12 col-md-6 col-md-6 tours_img">
				<header><h3>Monta&ntilde;ita</h3></header>
				<img class="" src="https://opeturmo.com/images_tours/montanita/montanita-ecuador.jpg" alt="Montanita Ecuador">
				
			</div>
			
		</div>
		<div class="row tours_row">
			<div class="col-md-6 tours_img">
				<header><h3>Galapagos</h3></header>
				<img class="" src="https://opeturmo.com/images_tours/galapagos/galapagos-island2.jpg" alt="Galapagos Ecuador">
			</div>
			<div class="col-md-6">
				<ul>
				<li class="options_tours" data-toggle='modal' data-target='#modalToursGalapagos' >Tours Galapagos</li>
				<li class="options_tours" data-toggle='modal' data-target='#BuceoGalapagosModal' >Tours Diving</li>
				</ul>
			</div>
		</div>
		
		<div class="row tours_row">
			
			<div class="col-12 col-sm-6 col-md-6">
				<ul>
				<li class="options_tours tours_li_left" data-toggle='modal' data-target='#modalToursParapenteDosHemosferios' >Paragliding Ecuador Two Hemispheres</li>
				<li class="options_tours tours_li_left" data-toggle='modal' data-target='#modalToursParapenteCompleto' >Complete Paragliding Ecuador Tourism</li>
				<li class="options_tours tours_li_left" data-toggle='modal' data-target='#CostaParapenteModal' >Tourism Paragliding Ecuador's Coast</li>
				<li class="options_tours tours_li_left" data-toggle='modal' data-target='#MiniParapenteModal' >Tourism Paragliding Mini Ecuador</li>
				</ul>
			</div>
			<div class="col-12 col-sm-6 col-md-6 tours_img">
				<header><h3>Paragliding Tours</h3></header>
				<img class="" src="https://opeturmo.com/images_parapente/tour_parapente/tours-parapente-ecuador.jpg" alt="Tours Parapente Ecuador">
			</div>
		</div>
		
	</div>
	
	
	
</div>	
	

	
	
	
	
	
	
	
	
	
	
	
 
 
<div class="content-mobile">
	
	
	
	
	
	
	<div class="row tours">
		<div class="row tours_row">
			<div class="col-12 col-md-6 col-md-6 tours_img">
				<header><h3>Puerto Lopez</h3></header>
				<img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tours-isla-de-la-plata-parque-nacional-machalilla-puerto-lopez-ecuador.jpg" alt="Parque Nacional Machalilla Puerto Lopez Ecuador">
				
			</div>
			
			<div class="col-12 col-sm-6  col-md-6 ">
				<ul>
					<li  class="options_tours "data-toggle='modal' data-target='#IslaPlataModal' >Tour Isla de la Plata</li>
					<li  class="options_tours "data-toggle='modal' data-target='#CosteroModal' >Salango Costal Tour</li>
					<li  class="options_tours "data-toggle='modal' data-target='#BallenasModal' >Tour Whale Watching (June - August)</li>
					<li  class="options_tours" data-toggle='modal' data-target='#CabalgataMachalillaModal' >Tour Humed Forest Horseback Ride</li>
					<li  class="options_tours "data-toggle='modal' data-target='#BuceoPlataModal' >Diving Isla de la Plata</li>
					</ul>
			</div>
		</div>
		<div class="row tours_row">
			
			<div class="col-12 col-md-6 col-md-6 tours_img">
				<header><h3>Monta&ntilde;ita</h3></header>
				<img class="" src="https://opeturmo.com/images_tours/montanita/montanita-ecuador.jpg" alt="Montanita Ecuador">
				
			</div>
			<div class="col-12 col-sm-6  col-md-6 ">
				<ul>
				<li  class="options_tours "data-toggle='modal' data-target='#DosMangasModal' >Tour Horseback Ride Dos Mangas</li>
				<li  class="options_tours "data-toggle='modal' data-target='#SurfModal' >Surf Clases</li>
				</ul>
			</div>
			
		</div>
		<div class="row tours_row">
			<div class="col-md-6 tours_img">
				<header><h3>Galapagos</h3></header>
				<img class="" src="https://opeturmo.com/images_tours/galapagos/galapagos-island2.jpg" alt="Galapagos Ecuador">
			</div>
			<div class="col-md-6">
				<ul>
				<li class="options_tours" data-toggle='modal' data-target='#modalToursGalapagos' >Tours Galapagos</li>
				<li class="options_tours" data-toggle='modal' data-target='#BuceoGalapagosModal' >Tours Diving</li>
				</ul>
			</div>
		</div>
		
		<div class="row tours_row">
			<div class="col-12 col-sm-6 col-md-6 tours_img">
				<header><h3>Tours de Parapente</h3></header>
				<img class="" src="https://opeturmo.com/images_parapente/tour_parapente/tours-parapente-ecuador.jpg" alt="Tours Parapente Ecuador">
			</div>
			<div class="col-12 col-sm-6 col-md-6">
				<ul>
				<li class="options_tours" data-toggle='modal' data-target='#modalToursParapenteDosHemosferios' >Paragliding Ecuador Two Hemispheres</li>
				<li class="options_tours" data-toggle='modal' data-target='#modalToursParapenteCompleto' >Complete Paragliding Ecuador Tourism</li>
				<li class="options_tours" data-toggle='modal' data-target='#CostaParapenteModal' >Tourism Paragliding Ecuador's Coast</li>
				<li class="options_tours" data-toggle='modal' data-target='#MiniParapenteModal' >Tourism Paragliding Mini Ecuador</li>
				</ul>
			</div>
			
		</div>
		
	</div>
	
	
	
</div>