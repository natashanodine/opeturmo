<div class="modal fade" id="IslaPlataModal" tabindex="-1" role="dialog" aria-labelledby="IslaPlataModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="IslaPlataModalLabel">Isla de la plata</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselIslaPlata" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour-isla-de-la-plata-parque-nacional-machalilla-puerto-lopez-ecuador.jpg" alt="Isla de la Plata Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour_puerto_lopez_isla_de_la_plata.jpg" alt="Isla de la Plata Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselIslaPlata" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour-isla-de-la-plata-parque-nacional-machalilla-puerto-lopez-ecuador.jpg" alt="Isla de la Plata Ecuador"></li>
							<li data-target="#carouselIslaPlata" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour_puerto_lopez_isla_de_la_plata.jpg" alt="Isla de la Plata Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Isla de la Plata</h2></header>
											<p>The "Isla de la Plata" forms part of the Machalilla National Park. Here you can observe the same bird species as on the Galapagos Islands; explore coral reefs and the great diversity of marine life; and enjoy beautiful surroundings, a perfect complement to the day's activities. </p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="CosteroModal" tabindex="-1" role="dialog" aria-labelledby="CosteroModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="CosteroModalLabel">Tour Costero Salango</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselCostero" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/snorkelling-isla-salango-ecuador.jpg" alt="Snorkelling Isla Salango Puerto Lopez Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/snorkelling-isla-salango-puerto-lopez-ecuador.jpg" alt="Snorkelling Isla Salango Puerto Lopez Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselCostero" data-slide-to="0" class="active"><img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/snorkelling-isla-salango-ecuador.jpg" alt="Snorkelling Isla Salango Puerto Lopez Ecuador">
</li>
							<li data-target="#carouselCostero" data-slide-to="1"><img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/snorkelling-isla-salango-puerto-lopez-ecuador.jpg" alt="Snorkelling Isla Salango Puerto Lopez Ecuador">
</li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Salango Costal Tour</h2></header>
											<p>Explore the coastline of the Machalilla National Park, where you can observe birds and stunning scenery, snorkel and kayak around Salango island</p>
																
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="BallenasModal" tabindex="-1" role="dialog" aria-labelledby="BallenasModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BallenasModalLabel">Avistamento Ballenas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselBallenas" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/avistamento-ballenas-puerto-lopez.jpg" alt="Avistamento Ballenas Puerto Lopez Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/humpback-whale-watching-ecuador.jpg" alt="Avistamento Ballenas Puerto Lopez Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselBallenas" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/avistamento-ballenas-puerto-lopez.jpg" alt="Avistamento Ballenas Puerto Lopez Ecuador"></li>
							<li data-target="#carouselBallenas" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/humpback-whale-watching-ecuador.jpg" alt="Avistamento Ballenas Puerto Lopez Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Avistamento Ballenas</h2></header>
											<p>Observe humpback whales throughout the marine portion of the Machalilla National Park, where they come to mate and give birth before returning south to the colder waters of the Antarctic regions. We also stop at Isla Salango to do bird watching and snorkelling.</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>





<div class="modal fade" id="CabalgataMachalillaModal" tabindex="-1" role="dialog" aria-labelledby="CabalgataMachalillaModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="CabalgataMachalillaModalLabel">Cabalgata Bosque Humedo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselCabalgataMachalilla" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/cabalgata_parque_nacional_machalilla_puerto_lopez.jpg" alt="Cabalgata Bosque Humedo Parque Nacional Machalilla Puerto Lopez Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour_cabalgata_parque_nacional_machalilla_puerto_lopez.jpg" alt="Cabalgata Bosque Humedo Parque Nacional Machalilla Puerto Lopez Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselCabalgataMachalilla" data-slide-to="0" class="active"> <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/cabalgata_parque_nacional_machalilla_puerto_lopez.jpg" alt="Cabalgata Bosque Humedo Parque Nacional Machalilla Puerto Lopez Ecuador"></li>
							<li data-target="#carouselCabalgataMachalilla" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour_cabalgata_parque_nacional_machalilla_puerto_lopez.jpg" alt="Cabalgata Bosque Humedo Parque Nacional Machalilla Puerto Lopez Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Cabalgata Bosque Humedo</h2></header>
												<p>In the Pital forest, Bola de Oro de garúa is in the heart of the Machalilla National Park at it's highest point of 800mts. Accessed by densley vegetated trails, it is home to more than 100 endemic species including monkeys, toucans, falcons and hummingbirds.</p>
																
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="BuceoPlataModal" tabindex="-1" role="dialog" aria-labelledby="BuceoPlataModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BuceoPlataModalLabel">Buceo Isla de la Plata</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselBuceoPlata" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour-buceo-isla-de-la-plata-ecuador.jpg" alt="Buceo Isla de la Plata Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour-buceo-isla-de-la-plata-puerto-lopez-ecuador.jpg" alt="Buceo Isla de la Plata Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselBuceoPlata" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour-buceo-isla-de-la-plata-ecuador.jpg" alt="Buceo Isla de la Plata Ecuador"></li>
							<li data-target="#carouselBuceoPlata" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour-buceo-isla-de-la-plata-puerto-lopez-ecuador.jpg" alt="Buceo Isla de la Plata Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Buceo Isla de la Plata</h2></header>
											<p>Diving with a guide at isla de la plata.</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

