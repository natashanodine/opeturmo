<?php get_header(); ?>
<div class="row blog_row">
<section class="col-12 col-md-9">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="blog_post"<?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header class="header">
				<h3><?php the_title(); ?></h3>
			</header>
			<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>

			<article class="entry row">
				<div class="row">
				<div class="col-md-12 images">
										 <?php
										preg_match_all('/(<img [^>]*>)/', get_the_content(), $images);
					for($i=0; isset($images[1]) && $i < count($images[1]); $i++) {
						echo $images[1][$i];
					}
										?>
										<p><?php $content = preg_replace('/(<img [^>]*>)/', '', get_the_content());
					$content = apply_filters('the_content', $content);
					echo $content;
				?></p>
				
				</div>				
				</div>	
				<div class="row">
				<div class="col-md-12 comments">
				<?php comments_template(); ?>
				</div>
				</div>
			</article>
			
			<p><?php edit_post_link('Edit this entry','','.'); ?></p>
			
		</div>


	<?php endwhile; endif; ?>
</section>	
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>