<?php get_header(); ?>



<div class="row blog_row">
	<div class="col-12 col-md-9">
	<?php if (have_posts()) : ?>

		<h2>Search Results</h2>

		<p><?php include (TEMPLATEPATH . '/inc/nav.php' ); ?></p>

		<?php while (have_posts()) : the_post(); ?>

			
		<div class="blog_post"<?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header class="header">
				<h3><?php the_title(); ?></h3>
			</header>


				<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>

				<div class="entry">
					<?php the_excerpt(); ?>
				</div>

			</div>

		<?php endwhile; ?>

		<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

	<?php else : ?>

		<div class="notfound"><h2>No posts found.</h2></div>

	<?php endif; ?>
	</div>
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>