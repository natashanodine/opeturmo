 <div class="row">
			<div class="col-12 header-pedida-mano">
				<header><h1>Marriage Proposal</h1></header>
			</div>
		</div>
   
   <div class="row pedida_mano">
			<div class="col-12 col-sm-6 col-md-6">
				<p> We make your request for an unforgettable moment for your partner and you to remember him for a lifetime. With the passage of time, the way you proposed marriage to your friends and strangers will be told, so keep in mind that it will only happen once in your life, it must be memorable and of course, with its great romantic touch, which is fundamental in the proposition. </p>

			<p> Free paragliding flight for 1 person </b> </p>
			<p> An approximate flight of 10 to 15 min per person. Landing on the beach, where the boy will be waiting for you with a bouquet of flowers and a heart made of white stones on the sand. It is optional if you wish to bring your own written legend. </p>

			<p> <b> Free paragliding flight for 2 people </b> </p>
			<p> They leave both at the same time, with different guide pilots. On an approximate flight of 10 to 15 min per person. Landing on the beach, first land who makes the proposal to wait for the girl, with a bouquet of flowers and a heart made of white stones on the sand. The written legend is visible from a height while the girl performs the activity. <p>

			<p> It can be done in Guayaquil (season from June to December) and the beach in Libertador Bolívar / Playa Bruja 10 minutes before Montañita. </p>

			<p> <b> Includes: </b> <p>
			<ul>
			<li> Photos and video 3 different high resolution cameras with editing. </li>
			<li> Local transportation of the equipment to be used, but not of the customers. </li>
			<li> The design of the art comes from the client. </li>
			</ul>
				
				</div>
			<div class="col-12 col-sm-6 col-md-6">
				<div class="pedida-mano-col">
					<div id="carouselPadidaMano" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/pedida-mano/parapente-a-motor-pedida-de-mano.jpg" alt="Corporate Promotions Paragliding">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/pedida-mano/parapente-a-motor-pedida-de-matrimonio.jpg" alt="Corporate Promotions Paragliding">
							</div>
						  </div>
						   <a class="carousel-control-prev" href="#carouselPadidaMano" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselPadidaMano" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> 
						  <ol class="carousel-indicators">
							<li data-target="#carouselPadidaMano" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/pedida-mano/parapente-a-motor-pedida-de-mano.jpg" alt="Corporate Promotions Paragliding"></li>
							<li data-target="#carouselPadidaMano" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/pedida-mano/parapente-a-motor-pedida-de-matrimonio.jpg" alt="Corporate Promotions Paragliding"></li>
						  </ol>
						
						</div>
				</div>
							  
			</div>
	</div>