<div class="modal fade" id="IslaPlataModal" tabindex="-1" role="dialog" aria-labelledby="IslaPlataModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="IslaPlataModalLabel">Isla de la plata</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselIslaPlata" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour-isla-de-la-plata-parque-nacional-machalilla-puerto-lopez-ecuador.jpg" alt="Isla de la Plata Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour_puerto_lopez_isla_de_la_plata.jpg" alt="Isla de la Plata Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselIslaPlata" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour-isla-de-la-plata-parque-nacional-machalilla-puerto-lopez-ecuador.jpg" alt="Isla de la Plata Ecuador"></li>
							<li data-target="#carouselIslaPlata" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour_puerto_lopez_isla_de_la_plata.jpg" alt="Isla de la Plata Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Isla de la Plata</h2></header>
											<p>La Isla de la Plata pertenece al Parque Nacional Machalilla. All&iacute; se puede observar todas las mismas especies que se encuentra en las islas Gal&aacute;pagos (piqueros patas azules, enmascarados de nazca, patas rojas fragatas	y albatros y aves tropicales); explorar arrecifes coralinos con una gran biodiversidad de especies marinas (buceo de superficie para observación de mantarrayas, peces, tortugas y arrecifes coralinos).; y disfrutar del bello paisaje que forma un complemente perfecto de las actividades que se realizan. 
																</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="CosteroModal" tabindex="-1" role="dialog" aria-labelledby="CosteroModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="CosteroModalLabel">Tour Costero Salango</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselCostero" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/snorkelling-isla-salango-ecuador.jpg" alt="Snorkelling Isla Salango Puerto Lopez Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/snorkelling-isla-salango-puerto-lopez-ecuador.jpg" alt="Snorkelling Isla Salango Puerto Lopez Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselCostero" data-slide-to="0" class="active"><img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/snorkelling-isla-salango-ecuador.jpg" alt="Snorkelling Isla Salango Puerto Lopez Ecuador">
</li>
							<li data-target="#carouselCostero" data-slide-to="1"><img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/snorkelling-isla-salango-puerto-lopez-ecuador.jpg" alt="Snorkelling Isla Salango Puerto Lopez Ecuador">
</li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Tour Costero Salango</h2></header>
											<p>Recorrer los diferentes islotes del perfil de la costa sur del Parque Nacional Machalilla, como la Isla Salango y El Pancito donde podremos observar aves como piqueros patas azules y fragatas ademas realizar las actividaded de snorkelling y kayaking. Visitaremos la playa Los Frailes donde podra disfrutar de la playa, caminar o hacer snorkelling</p>
																
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="BallenasModal" tabindex="-1" role="dialog" aria-labelledby="BallenasModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BallenasModalLabel">Avistamento Ballenas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselBallenas" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/avistamento-ballenas-puerto-lopez.jpg" alt="Avistamento Ballenas Puerto Lopez Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/humpback-whale-watching-ecuador.jpg" alt="Avistamento Ballenas Puerto Lopez Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselBallenas" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/avistamento-ballenas-puerto-lopez.jpg" alt="Avistamento Ballenas Puerto Lopez Ecuador"></li>
							<li data-target="#carouselBallenas" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/humpback-whale-watching-ecuador.jpg" alt="Avistamento Ballenas Puerto Lopez Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Avistamento Ballenas</h2></header>
											<p>Se observa las ballenas jorobadas en toda la parte marina del Parque Nacional Machalilla, donde se vienen a reproducir.
												Tambien incluye una parada en la Isla Salango para observacion de aves (piqueros patas azules) y hacer snorkelling.</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>





<div class="modal fade" id="CabalgataMachalillaModal" tabindex="-1" role="dialog" aria-labelledby="CabalgataMachalillaModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="CabalgataMachalillaModalLabel">Cabalgata Bosque Humedo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselCabalgataMachalilla" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/cabalgata_parque_nacional_machalilla_puerto_lopez.jpg" alt="Cabalgata Bosque Humedo Parque Nacional Machalilla Puerto Lopez Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour_cabalgata_parque_nacional_machalilla_puerto_lopez.jpg" alt="Cabalgata Bosque Humedo Parque Nacional Machalilla Puerto Lopez Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselCabalgataMachalilla" data-slide-to="0" class="active"> <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/cabalgata_parque_nacional_machalilla_puerto_lopez.jpg" alt="Cabalgata Bosque Humedo Parque Nacional Machalilla Puerto Lopez Ecuador"></li>
							<li data-target="#carouselCabalgataMachalilla" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour_cabalgata_parque_nacional_machalilla_puerto_lopez.jpg" alt="Cabalgata Bosque Humedo Parque Nacional Machalilla Puerto Lopez Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Cabalgata Bosque Humedo</h2></header>
												<p>El Bosque pital - Bola de Oro de garúa es el corazón del Parque Nacional Machalilla,su parte más alta llega a 800 msnm, accesible por senderos de densa vegetación. Hogar de más de 100 especies endémicas como monos, tucanes, halcones, colibrí, etc.</p>
																
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="BuceoPlataModal" tabindex="-1" role="dialog" aria-labelledby="BuceoPlataModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BuceoPlataModalLabel">Buceo Isla de la Plata</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselBuceoPlata" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour-buceo-isla-de-la-plata-ecuador.jpg" alt="Buceo Isla de la Plata Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour-buceo-isla-de-la-plata-puerto-lopez-ecuador.jpg" alt="Buceo Isla de la Plata Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselBuceoPlata" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour-buceo-isla-de-la-plata-ecuador.jpg" alt="Buceo Isla de la Plata Ecuador"></li>
							<li data-target="#carouselBuceoPlata" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tour-buceo-isla-de-la-plata-puerto-lopez-ecuador.jpg" alt="Buceo Isla de la Plata Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Buceo Isla de la Plata</h2></header>
											<p>inmersi&oacute;n con el guía de buceo, en dos lugares de inmersi&oacute;n.</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

