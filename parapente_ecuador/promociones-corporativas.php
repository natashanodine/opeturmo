 <div class="row">
			<div class="col-12 header-prom-corp">
				<header><h1>Promocines Corporativas</h1></header>
			</div>
		</div>
   
   <div class="row prom_corp">
			<div class="col-12 col-sm-6 col-md-6">
				<p>Le ofrecemos promociones corporativas de paquetes de vuelos en parapente, para instituciones educativas e instituciones corporativas. Esto le permitir&aacute; recargar sus bater&iacute;as, reducir el estr&eacute;s, fomentar la integraci&oacute;n con el objetivo de mejorar el trabajo en equipo y aumentar la productividad.</p>
			</div>
			<div class="col-12 col-sm-6 col-md-6">
				<div class="prom-corp-col">
					<div id="carouselPromCorp" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/promociones-corporativas/promociones_corpovativas_parapente.jpg" alt="Corporate Promotions Paragliding">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/promociones-corporativas/promocion_corpovativa_parapente.jpg" alt="Corporate Promotions Paragliding">
							</div>
						  </div>
						   <a class="carousel-control-prev" href="#carouselPromCorp" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselPromCorp" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> 
						  <ol class="carousel-indicators">
							<li data-target="#carouselPromCorp" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/promociones-corporativas/promociones_corpovativas_parapente.jpg" alt="Corporate Promotions Paragliding"></li>
							<li data-target="#carouselPromCorp" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/promociones-corporativas/promocion_corpovativa_parapente.jpg" alt="Corporate Promotions Paragliding"></li>
						  </ol>
						
						</div>
				</div>
							  
			</div>
	</div>