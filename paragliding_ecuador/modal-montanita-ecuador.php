<div class="modal fade" id="DosMangasModal" tabindex="-1" role="dialog" aria-labelledby="DosMangasModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="DosMangasModalLabel">Dos Mangas Manglaralto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselDosMangas" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_tours/montanita/dos-mangas-ecuador-montanita.jpg" alt="Montanita dos Mangas Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/montanita/dos_mangas_montanita_ecuador.jpg" alt="Montanita dos Mangas Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselDosMangas" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_tours/montanita/dos-mangas-ecuador-montanita.jpg" alt="Montanita dos Mangas Ecuador"></li>
							<li data-target="#carouselDosMangas" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_tours/montanita/dos_mangas_montanita_ecuador.jpg" alt="Montanita dos Mangas Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Dos Mangas Manglaralto</h2><header>
											<p>Tour Dos Mangas 6,5km from Manglaralto horseback riding along the Cascade route or the route of the pools where you can observe a variety of flora and fauna with the possibility of observing birds and monkeys. </p>
											<p> Cascade Route: It is considered a primary forest of timber, fruit and medicinal species, and they inhabit a large number of howler monkeys, which can be observed very easily in the winter season (December to June)</p>
											<p> Route of natural pools: This path has a certain degree of difficulty and slope that crosses several pools formed naturally in the rocky river that runs through the rain forest with small rock formations that contain stalactites and stalagmites. </ p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="SurfModal" tabindex="-1" role="dialog" aria-labelledby="SurfModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="SurfModalLabel">Surf Clases</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselSurf" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class=" " src="https://opeturmo.com/images_tours/montanita/surf-classes-montanita.jpg" alt="Surf classes Montanita">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/montanita/Surf-Montanita.jpg" alt="Surf classes Montanita">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselSurf" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_tours/montanita/surf-classes-montanita.jpg" alt="Surf classes Montanita"></li>
							<li data-target="#carouselSurf" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_tours/montanita/Surf-Montanita.jpg" alt="Surf classes Montanita"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Clases de Surf</h2><header>
											<p>
											A class of two hours, the first 45 minutes are used in the revision of the theory in the
											beach, so that we can then practice at sea for one hour and 15 minutes and get
											what every student eagerly wants to do - stand on the board and ride a wave.</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>