<div class="row somos">
		<div class="col-12 col-sm-5 col-md-5 parapente_img">
		  <img class="content-desktop ecuador-parapente" src="https://opeturmo.com/images_parapente/logos/parapente-ecuador.png" alt="parapente ecuador">
		  <img class="content-mobile ecuador-parapente" src="https://opeturmo.com/images_parapente/logos/parapente-ecuador-150px.png" alt="parapente ecuador">
		  
		</div>
		<div class="col-12 col-sm-7 col-md-7">
		<header><h2>About Us</h2></header>
		<p>We are <b>Ecuador Parapente</b>, a tourism operator specialized in paragliding adventure activity. We carry out paragliding activities with guide, paragliding school and adventure tours.<br>
The passion for flying is what motivates us to teach tourists the magic of flying paragliding, entrusting us to feel confident and safe when flying with us.</p></div>
    </div>