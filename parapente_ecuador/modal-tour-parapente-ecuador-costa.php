<div class="modal fade" id="CostaParapenteModal" tabindex="-1" role="dialog" aria-labelledby="CostaParapenteModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="CostaParapenteModalLabel">Tour Parapente Turismo por la Costa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<div class="row">
										<div class="col-md-6 col-sm-12 tour_col_left">
											<div class="tours_pax">
												<p>DIA 1</p>
													<ul>
											   <li>Llegada al aeropuerto de Guayaquil y traslado a Santa Elena</li>
												<li>Visita a las aguas termales de San Vicente Manantial es rico en minerales, también se ofreceran masajes.</li>
												<li>Vuelo en parapente en Ancon</li>
												<li>Traslado a Monta&ntilde;ita</li> 
													</ul>
											</div>
											<div class="tours_pax">
												<p>DIA 2</p>
													<ul>
												<li>Vuelo en parapente en Monta&ntilde;ita</li>
												<li>Almuerzo en Ayangue</li>
												<li>Vuelo en parapente en Playa Bruja</li>
												<li>Traslado a Puerto Lopez</li>
													</ul>
											</div>
											
											<div class="tours_pax">
												<p>DIA 3</p>
													<ul>
												<li>Vuelo en parapente Salango</li>
												<li>Almuerzo</li>
												<li>Visita Playa Los Frailers (Parque Nacional Machalilla)</li>
												<li>Salida a Crucita</li>
													</ul>
											</div>

										</div>

										<div class="col-md-6 col-sm-12 tour_col_right">
												
											<div class="tours_pax">
												<p>DIA 4</p>
													<ul>
												<li>Vuelo en parapente Crucita</li> 
												<li>Almuerzo</li>
													</ul>
											</div>
												
											<div class="tours_pax">
												<p>DIA 5</p>
													<ul>
												<li>Vuelo en parapente La Silla</li>
												<li>Almuerzo y Salida</li>
												<li>Vuelo en parapente Canoa</li>
													</ul>
											</div>
												
											<div class="tours_pax">
												<p>DIA 6</p>
													<ul>
												<li>Vuelo en parapente Guayaquil</li>
												<li>Hotel y Aeropuerto</li>
													</ul>
											</div>
										</div>
									</div>
	  
	  
	  
	  
									
 </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>