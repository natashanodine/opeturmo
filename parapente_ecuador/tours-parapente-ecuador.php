
				<?php include 'parapente_ecuador/modal-tour-parapente-ecuador-mini.php'; ?>				
				<?php include 'parapente_ecuador/modal-tour-parapente-ecuador-costa.php'; ?>
				<?php include 'parapente_ecuador/modal-tour-parapente-ecuador-completo.php'; ?>	
				<?php include 'parapente_ecuador/modal-tour-parapente-dos-hemisferios.php'; ?>	
	
				<?php include 'parapente_ecuador/modal-machalilla-ecuador.php'; ?>
				<?php include 'parapente_ecuador/modal-montanita-ecuador.php'; ?>
				<?php include 'parapente_ecuador/modal-galpagos-ecuador.php'; ?>
				<?php include 'parapente_ecuador/modal-galpagos-buceo-ecuador.php'; ?>
	
	
	
	
	<div class="row header_row">
		<div class="col-12">
			<header><h1>Tours Aventura Ecuador</h1></header>
			<header><h3>Actividades Turisticas de Aventura</h3></header>
		</div>
	</div>
	
	

 
 
<div class="content-desktop">
	
	<div class="row tours">
		<div class="row tours_row">
			<div class="col-12 col-md-6 col-md-6 tours_img">
				<header><h3>Puerto Lopez</h3></header>
				<img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tours-isla-de-la-plata-parque-nacional-machalilla-puerto-lopez-ecuador.jpg" alt="Parque Nacional Machalilla Puerto Lopez Ecuador">
				
			</div>
			
			<div class="col-12 col-sm-6  col-md-6 ">
				<ul>
				<li  class="options_tours "data-toggle='modal' data-target='#IslaPlataModal' >Tour Isla de la Plata</li>
				<li  class="options_tours "data-toggle='modal' data-target='#CosteroModal' >Tour Costero Salango</li>
				<li  class="options_tours "data-toggle='modal' data-target='#BallenasModal' >Tour Avistamento Ballenas (Junio - Agosto)</li>
				<li  class="options_tours" data-toggle='modal' data-target='#CabalgataMachalillaModal' >Tour Cabalgata Bosque Humedo</li>
				<li  class="options_tours "data-toggle='modal' data-target='#BuceoPlataModal' >Buceo en la Isla de la Plata</li>
				</ul>
			</div>
		</div>
		<div class="row tours_row">
			<div class="col-12 col-sm-6  col-md-6 ">
				<ul>
				<li  class="options_tours tours_li_left"data-toggle='modal' data-target='#DosMangasModal' >Tour Cabalgata Dos Mangas</li>
				<li  class="options_tours tours_li_left"data-toggle='modal' data-target='#SurfModal' >Clases de Surf</li>
				</ul>
			</div>
			<div class="col-12 col-md-6 col-md-6 tours_img">
				<header><h3>Monta&ntilde;ita</h3></header>
				<img class="" src="https://opeturmo.com/images_tours/montanita/montanita-ecuador.jpg" alt="Montanita Ecuador">
				
			</div>
			
		</div>
		<div class="row tours_row">
			<div class="col-md-6 tours_img">
				<header><h3>Galapagos</h3></header>
				<img class="" src="https://opeturmo.com/images_tours/galapagos/galapagos-island2.jpg" alt="Galapagos Ecuador">
			</div>
			<div class="col-md-6">
				<ul>
				<li class="options_tours" data-toggle='modal' data-target='#modalToursGalapagos' >Tours Galapagos</li>
				<li class="options_tours" data-toggle='modal' data-target='#BuceoGalapagosModal' >Tours Buceo</li>
				</ul>
			</div>
		</div>
		
		<div class="row tours_row">
			
			<div class="col-12 col-sm-6 col-md-6">
				<ul>
				<li class="options_tours tours_li_left" data-toggle='modal' data-target='#modalToursParapenteDosHemosferios' >Turismo Parapente Dos Hemisferios</li>
				<li class="options_tours tours_li_left" data-toggle='modal' data-target='#modalToursParapenteCompleto' >Turismo Parapente Ecuador Completo</li>
				<li class="options_tours tours_li_left" data-toggle='modal' data-target='#CostaParapenteModal' >Turismo Parapente Costa Ecuador</li>
				<li class="options_tours tours_li_left" data-toggle='modal' data-target='#MiniParapenteModal' >Turismo Parapente Mini Ecuador</li>
				</ul>
			</div>
			<div class="col-12 col-sm-6 col-md-6 tours_img">
				<header><h3>Tours de Parapente</h3></header>
				<img class="" src="https://opeturmo.com/images_parapente/tour_parapente/tours-parapente-ecuador.jpg" alt="Tours Parapente Ecuador">
			</div>
		</div>
		
	</div>
	
	
	
</div>	
	

	
	
	
	
	
	
	
	
	
	
	
 
 
<div class="content-mobile">
	
	
	
	
	
	
	<div class="row tours">
		<div class="row tours_row">
			<div class="col-12 col-md-6 col-md-6 tours_img">
				<header><h3>Puerto Lopez</h3></header>
				<img class="" src="https://opeturmo.com/images_tours/puerto-lopez-parque-nacional-machalilla/tours-isla-de-la-plata-parque-nacional-machalilla-puerto-lopez-ecuador.jpg" alt="Parque Nacional Machalilla Puerto Lopez Ecuador">
				
			</div>
			
			<div class="col-12 col-sm-6  col-md-6 ">
				<ul>
				<li  class="options_tours "data-toggle='modal' data-target='#IslaPlataModal' >Isla de la Plata</li>
				<li  class="options_tours "data-toggle='modal' data-target='#CosteroModal' >Tour Costero Salango</li>
				<li  class="options_tours "data-toggle='modal' data-target='#BallenasModal' >Avistamento Ballenas (Junio - Agosto)</li>
				<li  class="options_tours" data-toggle='modal' data-target='#CabalgataMachalillaModal' >Cabalgata Bosque Humedo</li>
				<li  class="options_tours "data-toggle='modal' data-target='#BuceoPlataModal' >Buceo en la Isla de la Plata</li>
				</ul>
			</div>
		</div>
		<div class="row tours_row">
			
			<div class="col-12 col-md-6 col-md-6 tours_img">
				<header><h3>Monta&ntilde;ita</h3></header>
				<img class="" src="https://opeturmo.com/images_tours/montanita/montanita-ecuador.jpg" alt="Montanita Ecuador">
				
			</div>
			<div class="col-12 col-sm-6  col-md-6 ">
				<ul>
				<li  class="options_tours "data-toggle='modal' data-target='#DosMangasModal' >Cabalgata Dos Mangas</li>
				<li  class="options_tours "data-toggle='modal' data-target='#SurfModal' >Clases de Surf</li>
				</ul>
			</div>
			
		</div>
		<div class="row tours_row">
			<div class="col-md-6 tours_img">
				<header><h3>Galapagos</h3></header>
				<img class="" src="https://opeturmo.com/images_tours/galapagos/galapagos-island2.jpg" alt="Galapagos Ecuador">
			</div>
			<div class="col-md-6">
				<ul>
				<li class="options_tours" data-toggle='modal' data-target='#modalToursGalapagos' >Tours Galapagos</li>
				<li class="options_tours" data-toggle='modal' data-target='#BuceoGalapagosModal' >Tours Buceo</li>
				</ul>
			</div>
		</div>
		
		<div class="row tours_row">
			<div class="col-12 col-sm-6 col-md-6 tours_img">
				<header><h3>Tours de Parapente</h3></header>
				<img class="" src="https://opeturmo.com/images_parapente/tour_parapente/tours-parapente-ecuador.jpg" alt="Tours Parapente Ecuador">
			</div>
			<div class="col-12 col-sm-6 col-md-6">
				<ul>
				<li class="options_tours " data-toggle='modal' data-target='#modalToursParapenteDosHemosferios' >Turismo Parapente Dos Hemisferios</li>
				<li class="options_tours " data-toggle='modal' data-target='#modalToursParapenteCompleto' >Turismo Parapente Ecuador Completo</li>
				<li class="options_tours " data-toggle='modal' data-target='#CostaParapenteModal' >Turismo Parapente Costa Ecuador</li>
				<li class="options_tours " data-toggle='modal' data-target='#MiniParapenteModal' >Turismo Parapente Mini Ecuador</li>
				</ul>
			</div>
			
		</div>
		
	</div>
	
	
	
</div>