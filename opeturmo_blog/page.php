<?php get_header(); ?>
<div class="row blog_row">
<section class=" col-12 col-md-9">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
		<div class="post post_single" id="post-<?php the_ID(); ?>">
			<header class="header" >
				<h2><?php the_title(); ?></h2>
			</header>
			
			<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>

			<div class="entry">

				<?php the_content(); ?>

				<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>

			</div>

			<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>

		</div>
		
		<?php comments_template(); ?>

		<?php endwhile; endif; ?>
</section>
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>