<div class="modal fade" id="modalToursParapenteCompleto" tabindex="-1" role="dialog" aria-labelledby="modalToursParapenteCompletoLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalToursParapenteCompletoLabel">Tour Parapente Turismo Ecuador Completo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

	  
	  
	  <div class="row">
											<div class="col-md-6 col-sm-12 tour_col_left">
												<div class="tours_pax">
													<p>DIA 1</p>
													<ul>
													<li>Llegada al aeropuerto de Guayaquil</li>
													<li>Traslado a La Troncal (Agua Termales Yanayacu)</li>
													</ul>
												</div>						
												<div class="tours_pax">
													<p>DIA 2</p>
													<ul>
													<li>Vuelo en parapente en Cochacay</li>
													<li>Traslado a Alausi (Tren comunidad indigena)</li>
													</ul>
												</div>						
												<div class="tours_pax">
													<p>DIA 3</p>
													<ul>
													<li>Desayuno y salida </li>
													<li>Vuelo en parapente en Cochabamba</li>
													</ul>
												</div>
												<div class="tours_pax">
													<p>DIA 4</p>
													<ul>
													<li>Salida a la Riobamba</li>
													<li>Vuelo en parapente en Tunshi</li> 
													<li>Salida a Baños de Agua Santa</li>
													</ul>
												</div>							
												<div class="tours_pax">
													<p>DIA 5</p>
													<ul>
													<li>Desayuno</li>
													<li>Vuelo en parapente Niton</li>
													<li>Retorno a Riobamba</li>
													</ul>
												</div>							
												<div class="tours_pax">
													<p>DIA 6</p>
													<ul>
													<li>Recorrido en Tren con el tour denominado el Tren de los ancestros.</li>
													<li>Salida a Guayaquil</li>
													</ul>
												</div>
												<div class="tours_pax">
													<p>DIA 7</p>
													<ul>
													<li>Traslado a Santa Elena</li>
													<li>Visita a los Baños de San Vicente. Este manantial de agua termal rica en minerales</li>
													<li>Masaje</li>
													<li>Vuelo en parapente en Ancon</li>
													<li>Salida a Monta&ntilde;ita</li>
													</ul>
												</div>			
											</div>
											<div class="col-md-6 col-sm-12 tour_col_right">						
												<div class="tours_pax">
													<p>DIA 8</p>
													<ul>
													<li>Vuelo en parapente Playa Bruja</li>
													<li>Almuerzo Ayangue</li>
													<li>Salida a Puerto Lopez</li>
													</ul>
												</div>
												<div class="tours_pax">
													<p>DIA 9</p>
													<ul>
													<li>Tour Isla de la Plata. </li>
													<li>Llegada a isla de la Plata</li>
													<li>Caminata por los senderos para la observación de aves marinas.</li>
													<li>Practica de Snorkeling.</li>
													</ul>
												</div>
												<div class="tours_pax">
													<p>DIA 10</p>
													<ul>
													<li>Vuelo en parapente Salango</li>
													<li>Almuezo</li>
													<li>Visita Los Frailes</li>
													<li>Visita Agua Blanca un pueblo ancestral</li>
													<li>Salida Crucita</li>
													</ul>
												</div>
												<div class="tours_pax">
													<p>DIA 11</p>
													<ul>
													<li>Vuelo en parapente Crucita</li>
													<li>Almuerzo</li>
													</ul>
												</div>
												<div class="tours_pax">
													<p>DIA 12</p>
													<ul>
													<li>Salida</li>
													<li>Vuelo en parapente La Silla</li>
													<li>Salida Canoa</li>
													<li>Vuelo en parapente en Canoa</li>
													</ul>
												</div>
												<div class="tours_pax">
													<p>DIA 13</p>
													<ul>
													<li>Salida Guayaquil</li>
													<li>Vuelo en parapente Guayaquil</li>
													<li>Salida Aeropuerto</li>
													</ul>
												</div>
											</div>
										</div>
	  
	  
	  
									
 </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>