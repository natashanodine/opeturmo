
<div class="modal fade" id="modalToursParapenteDosHemosferios" tabindex="-1" role="dialog" aria-labelledby="modalToursParapenteDosHemosferios" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalToursParapenteDosHemosferios">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="col-12">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6">
						
					<div id="carouselDosHemisferios" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/tour_parapente/tour-parapente-ecuador.jpg" alt="Paragliding Tours Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/tour_parapente/parapente-tours-ecuador.jpg" alt="Paragliding Tours Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/tour_parapente/paragliding-tours-ecuador.jpg" alt="Paragliding Tours Ecuador">
							</div>
							<div class="carousel-item">
							  
							  <img class="" src="https://opeturmo.com/images_parapente/tour_parapente/paragliding-tour-ecuador.jpg" alt="Paragliding Tours Ecuador">
							</div>
							<div class="carousel-item">
							  
							  <img class="" src="https://opeturmo.com/images_parapente/tour_parapente/tours-parapente-en-ecuador.jpg" alt="Paragliding Tours Ecuador">
							</div>
							<div class="carousel-item">
							  
							  <img class="" src="https://opeturmo.com/images_parapente/tour_parapente/parapente-tours-in-ecuador.jpg" alt="Paragliding Tours Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselDosHemisferios" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/tour_parapente/tour-parapente-ecuador.jpg" alt="Paragliding Tours Ecuador"></li>
							<li data-target="#carouselDosHemisferios" data-slide-to="1"><img class="" src="https://opeturmo.com/images_parapente/tour_parapente/parapente-tours-ecuador.jpg" alt="Paragliding Tours Ecuador"></li>
							<li data-target="#carouselDosHemisferios" data-slide-to="2"><img class=" " src="https://opeturmo.com/images_parapente/tour_parapente/paragliding-tours-ecuador.jpg" alt="Paragliding Tours Ecuador"></li>
							<li data-target="#carouselDosHemisferios" data-slide-to="3"><img class="" src="https://opeturmo.com/images_parapente/tour_parapente/paragliding-tour-ecuador.jpg" alt="Paragliding Tours Ecuador"></li>
							<li data-target="#carouselDosHemisferios" data-slide-to="4"><img class="" src="https://opeturmo.com/images_parapente/tour_parapente/tours-parapente-en-ecuador.jpg" alt="Paragliding Tours Ecuador"></li>
							<li data-target="#carouselDosHemisferios" data-slide-to="5"><img class="" src="https://opeturmo.com/images_parapente/tour_parapente/parapente-tours-in-ecuador.jpg" alt="Paragliding Tours Ecuador"></li>
						  </ol>
						
						</div>
					</div>
				</div>	
				</div>	
				</div>	
			
        <div class="row">
			<div class="col-12">
			
					<div class="row">
							<div class="col-12">
								<header><h4>Paragliding Tour Two Hemispheres</h4></header>
							</div>
						</div>
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6">
						<div class="row">
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Day 1 - Flights in Playa Bruja - Libertador Bolívar </h5> </header>
									<p> Take off from Playa Bruja and San Pedro, flight on the slope of Playa Bruja. (Dynamic flight, height of the hill 72m), in the afternoon flight in San Pedro 80 m. and 3 km. long, you can climb to 200 m. </p>
									<p> Libertador Bolívar is 10 minutes before Montañita, north of the province of Santa Elena, 2 hours from Guayaquil. </p>
									<p> Accommodation in Montañita Hostal by the sea, Montañita is characterized by having a night party. </p>
								</article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Day 2 - Flights in Puerto López - Salango. </h5> </header>
									<p> After breakfast, transfer to Puerto López. Flight on the big hill 300m. of height (thermal) can be raised to 800 meters or Salango 100 m. (dynamic). You can climb to 300 m, takeoff that has a wonderful view of the island of Salango, piqueo at noon. Flights in the afternoon. Visit to the Palo Santo Oil Factory. </p>
									<p> Puerto López is a fishing village located in the extreme southwest of the Province of Manabí, in Ecuador. It is located within the Machalilla National Park with a reserve of 56,184 hectares. </p>
									<p> Accommodation in Itapoa hostel facing the sea. </p>
								</article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Day 3 </h5> </header>
									<p> After breakfast 8:00 am possibility of flying on the big hill or Salango, 11am coastal tour: snorkeling and kayaking activity that takes place within the marine protected area of ​​Puerto López with a great biodiversity of marine life, return 3pm. Transfer to Crucita with probability of flying in San José de Montecristi 1 hour from Puerto López. </ P>
									<p> Lodging in Crucita Hostal Voladores with swimming pool and next to the sea. </p>
								</article>
							</section>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-md-6">
						<div class="row">
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Day 4 - Crucita flights </h5> </header>
									<p> After breakfast at Hostal Voladores. Flight on the slope of Crucita, possibility of climbing 350 meters 14km long back and forth. </ P>
									<p> Crucita is a fishing and tourist village by the sea. On the hillside you can play all day, very easy for the landing above or Top landing, after landing on the beach, accommodation in the same Hostel flying, at night visit of the city of Montecristi capital of the original Panamanian hat of Ecuador , visit of the city of Manta. </p>
								</article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Day 5 </h5> </header>
									<p> Short flights in the morning on the hill of Crucita, departure to Canoa at 1:30 min, direct to the flight hill, you can climb to 500 m. of height and 40 km. round trip, lodging in "Sol y Luna" Hostal with pool and facing the sea, you can land in front of the hotel. </p>
									<p> At night Canoe's boardwalk. </p>
								</article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Day 6 - NORTH HEMISPHERE </ h5> </ header>
									<p> After breakfast 8am, transfer to Pedernales, 2 hours, stop 10 to 15 minutes on the equator, we will demonstrate the Coriolis effect on the Ecuador line with water. </p>
									<p> Flights on the slope of Pedernales can be climbed 200 m. and 20 km. back and forth. Piqueo in Cojimíes 10 minutes from takeoff and lodging in the same locality. </p>
								</ article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5> Day 7 </h5> </header>
									<p> After breakfast 8am the group will decide which flight zone wants to fly back to the South: Pedernales, Canoa, Crucita, Puerto López. 5 hours back to Puerto López. Transfer to Guayaquil (coordinate with the group). </p></article>
							</section>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class= "col-12 col-md-6">
				<p> <b> Includes: </b> </p>
				<ul>
					<li> Tourist type accommodation. </li>
					<li> Breakfast. </li>
					<li> Transportation. </li>
					<li> Guide / Driver. </li>
					<li> Service by operation (Opeturmo S.A.) </li>
					<li> Tour logistics. </li>
					<li> Coastal Tour or Whale Watching Tour </li>
					<li> APPI P2 Certification </li>
				</ul>
			</div>
			<div class= "col-12 col-md-6">
				<p> <b> Does not include: </b> </p>
				<ul>
					<li> Food and drinks. </li>
					<li> Value of tickets to possible National parks or reserves. </li>
				</ul>
				 <p> <b> Requirements for the pilot: </b> </p>
				<ul>
					<li> The pilot must have a minimum of one year or 60 flight hours. </li>
					<li> Complete paragliding. </li>
					<li> Emergency parachute. </li>
					<li> Harness with dorsal protection. </li>
					<li> UHF open frequency radio. </li>
					<li> Helmet approved for free flight. </li>
					<li> Boots or high-top shoes. </li>
				</ul>
			</div>
		</div>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>