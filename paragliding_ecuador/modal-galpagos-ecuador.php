
<div class="modal fade" id="modalToursGalapagos" tabindex="-1" role="dialog" aria-labelledby="modalToursGalapagos" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalToursGalapagos">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="col-12">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6">
						
					<div id="carouselBahia" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_tours/galapagos/tortoise-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/galapagos/pelikan-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_tours/galapagos/cactus-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
							<div class="carousel-item">
							  
							  <img class="" src="https://opeturmo.com/images_tours/galapagos/iguana-galapagos-islands.jpg" alt="Tours Galapagos Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselBahia" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_tours/galapagos/tortoise-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
							<li data-target="#carouselBahia" data-slide-to="1"><img class="" src="https://opeturmo.com/images_tours/galapagos/cactus-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
							<li data-target="#carouselBahia" data-slide-to="2"><img class=" " src="https://opeturmo.com/images_tours/galapagos/pelikan-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
							<li data-target="#carouselBahia" data-slide-to="3"><img class="" src="https://opeturmo.com/images_tours/galapagos/iguana-galapagos-islands.jpg" alt="Tours Galapagos Ecuador"></li>
						  </ol>
						
						</div>
						<div class="row daily-galapagos">
							<div class="col-12">
								<header><h4>Daily tour description</h4></header>
							</div>
						</div>
						<div class="accordion" id="accordionToursGalapagos">
						  <div class="card card-tours">
							<div class="card-header" id="IslaSeymour">
							  <h5 class="mb-0">
								<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseIslaSeymour" aria-expanded="true" aria-controls="collapseIslaSeymour">
								  Seymour Island
								</button>
							  </h5>
							</div>

							<div id="collapseIslaSeymour" class="collapse" aria-labelledby="headingIslaSeymour" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
									<p>
									In the Itabaca Channel, the boat will be taken to the island of seymour with a navigation of approximately 1:15 and where you can see colonies of frigates, blue footed boobies, wolves and marine iguanas. Visit to "Las Bachas" beach, an ideal place to swim and snorkel.
									</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingBartolome">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseBartolome" aria-expanded="false" aria-controls="collapseBartolome">
									Bartolome
								</button>
							  </h5>
							</div>
							<div id="collapseBartolome" class="collapse" aria-labelledby="headingBartolome" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
									<p>From the Itabaca Canal and start the navigation towards Bartolomé; approximate navigation time 2 hours; It is observed: piqueros, lava, penguins, sea lions and beautiful panoramic view of Santiago Island. Excellent place to swim and snorkel.
									</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingSantaFe">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSantaFe" aria-expanded="false" aria-controls="collapseSantaFe">
									Santa Fe
								</button>
							  </h5>
							</div>
							<div id="collapseSantaFe" class="collapse" aria-labelledby="headingSantaFe" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
									<p>You leave the tourists dock in Puerto Ayora on the boat to Santa Fe Island, the same one located between San Cristóbal and Santa Cruz. You can see large colonies of sea lions, marine iguanas, pelicans, different varieties of fish, masked boobies, blue footed boobies. Site highly recommended for snorkeling and swimming.
									</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingFloreana">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFloreana" aria-expanded="false" aria-controls="collapseFloreana">
								    Floreana
								</button>
							  </h5>
							</div>
							<div id="collapseFloreana" class="collapse" aria-labelledby="headingFloreana" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
									<p>Departure from the tourist pier in Puerto Ayora and take the boat to Floreana. Snorkel tour in: Black beach and wolves, to observe according to the season: dolphins, sea lions, turtles, sharks, and variety of fish according to the season. Visit to Puerto Velasco Ibarra to observe the ruins of the pirates and water springs in the upper part of the island. Sailing time: Puerto Ayora- Floreana 2h50.
									.</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingPlazaPunta">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsePlazaPunta" aria-expanded="false" aria-controls="collapsePlazaPunta">
								    Plaza y Punta Carrion
								</button>
							  </h5>
							</div>
							<div id="collapsePlazaPunta" class="collapse" aria-labelledby="headingPlazaPunta" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
								    <p>Departure from the tourists dock or Canal de Itabaca. Upon arrival you can see a wonderful view of the place, South Plaza is covered with a reddish low vegetation called sesuvium which makes it very attractive, we can see: seagulls, lizards, tropical bird, blue footed boobies, masked boobies, colony of sea ​​lions and land iguanas.
									</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingSanCristobal">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSanCristobal" aria-expanded="false" aria-controls="collapseSanCristobal">
								    San Cristobal
								</button>
							  </h5>
							</div>
							<div id="collapseSanCristobal" class="collapse" aria-labelledby="headingSanCristobal" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
									<p>Departure time of the pier 07 a.m. Sailing approximately 2 hours to Puerto Baquerizo Moreno we went by boat to Kicker Rocks (León Dormido) to Snorkell, in this place there is a lot of activity you can see, sharks, striped rays, sea lions, fish, sea turtles, then visit Isla Wolves to observe frigates, pikemen, sea lions, and we rest at the Manglecito beach to take the box lunch and swim. Return to Puerto Ayora 6H00 p.m. approximately
									</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingIsabela">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseIsabela" aria-expanded="false" aria-controls="collapseIsabela">
								    Isabela
								</button>
							  </h5>
							</div>
							<div id="collapseIsabela" class="collapse" aria-labelledby="headingIsabela" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
									<p>Pick up at Hotel: 06h50am. Navigation (2 hours trip) Arrival to Isabela Island, where the guide will give indications of the activities to be done, we will take a bus or taxi that will take you to the Wetlands where you will find: Laguna de los Flamingos, giant turtle breeding center a small interpretation center of the Galapagos National Park and the beach, it is also possible to find marine iguanas, lava lizards, volcanic plants such as the lava cactus, the scalesia, among others. We go to the dock to take a boat to visit the islet tintoreras, where a dry landing is made 20 minutes walk to appreciate the white fin sharks, iguanas, blue footed boobies, penguins, snorkeling out of the islet and we can observe, blanket rays, sea lions, sea turtles.
									</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingTourBahia">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTourBahia" aria-expanded="false" aria-controls="collapseTourBahia">
								    Tour de Bahia
								</button>
							  </h5>
							</div>
							<div id="collapseTourBahia" class="collapse" aria-labelledby="headingTourBahia" data-parent="#accordionToursGalapagos">
							    <div class="card-body">
									<p>We visit the Island "la Lobería", named for the great sea lion colony where snorkeling is done with sea lions. It is a very special place to see a great variety of colorful fish, a diversity of seafloor species. Then we go to Punta Estrada you can see animals such as terns, pikemen, frigates, sea lions, ray blanket, ETC. Visit the sharks channel you can see the sharks and from the viewpoint you can see the port and the mountains of the upper part of Isla islote tintoreras, where a dry landing is made, a 20-minute walk to see the white-tipped sharks, iguanas , blue footed boobies, penguins, we snorkel out of the islet and we can observe, manta rays, sea lions, sea turtles. Visit Playa de los Perros to observe colony of marine iguanas, sea lions and landscape view. Visit the beautiful channel of love asleep water canal. Visit the Cracks, natural pool with filtered water, excellent for snorkeling.
									</p>
								</div>
							</div>
						  </div>
						  <div class="card card-tours">
							<div class="card-header" id="headingTortugaBay">
							  <h5 class="mb-0">
								<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTortugaBay" aria-expanded="false" aria-controls="collapseTortugaBay">
								    Tortuga Bay
								</button>
							  </h5>
							</div>
							<div id="collapseTortugaBay" class="collapse" aria-labelledby="headingTortugaBay" data-parent="#accordionToursGalapagos">
							  <div class="card-body">
								<p>Beautiful white sand beach, it has an approximate distance of 1 km. ½ of distance from Puerto Ayora, excellent path surrounded by lush vegetation and birds of the place are observed.</p>
								</div>
							</div>
						  </div>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-md-6">
					<div class="row">
							<div class="col-12">
								<header><h4>Description Packages</h4></header>
							</div>
						</div>
						<div class="row">
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Tour 3 Days 2 Nights</h5></header>
									<ul>
									<li>Day 1: Transfer from Baltra airport to the Channel Itabaca. Visit the highlands of Santa Cruz, ranch Primicias to see turtles in their natural state, lava tunels and vegetation of the place</li>
									<li>Day 2: Tour to a island like: Seymour, Bartolomé, Santa Fe, Floreana , isabela /o Plazas. (Wo only visit one island)</li>
									<li>Day 3: Transfer from the hotel to Baltra airport.</li>
									</ul>
								</article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Tour 4 Days 3 Nights</h5></header>
									<ul>
									<li>Day 1: Transfer from Baltra airport to the Channel Itabaca. Visit the highlands of Santa Cruz, ranch Primicias to see turtles in their natural state, lava tunels and vegetation of the place</li>
									<li>Day 2: Tour to a island like: Seymour, Bartolomé, Santa Fe, Floreana , isabela /o Plazas. (Wo only visit one island)</li>
									<li>Day 3: Visit to Charles Darwin interpretation center located in Santa Cruz Island. In the afternoon Tour de Bahia or visit Tortuga Bay Beach (only two places).</li>
									<li>Day 4: Transfer from the hotel to Baltra airport.</li>
									</ul>
								</article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Tour 5 Days 4 Nights</h5></header>
									<ul>
									<li>Day 1: Transfer from Baltra airport to the Channel Itabaca. Visit the highlands of Santa Cruz, ranch Primicias to see turtles in their natural state, lava tunels and vegetation of the place</li>
									<li>Day 2: Tour to a island like: Seymour, Bartolomé, Santa Fe, Floreana , isabela /o Plazas. (Wo only visit one island)</li>
									<li>Day 3: Tour to a island like: Seymour, Bartolomé, Santa Fe, Floreana , isabela /o Plazas. (Wo only visit one island)</li>									
									<li>Day 4: Visit to Charles Darwin interpretation center located in Santa Cruz Island. In the afternoon Tour de Bahia or visit Tortuga Bay Beach (only two places).</li>
									<li>Day 5: Transfer from the hotel to Baltra airport.</li>
									</ul>			
								</article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Tour 5 Days 4 Nights Buceo</h5></header>
									<ul>
									<li>Day 1: Transfer from Baltra airport / visit twin craters / lunch / Tunnels and turtles in natural state</li>
									<li>Day 2: Diving Daily (Includes box lunch on board).</li>
									<li>Day 3: Diving Daily (Includes box lunch on board).</li>
									<li>Day 4: Day off.</li>
									<li>Day 5: Transfer from the hotel to Baltra airport.</li>
										</ul>		
								</article>
							</section>
						</div>
					</div>
				</div>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>