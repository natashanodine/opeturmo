  <div class="row">
		<div class="col-12 col-sm-12 col-md-12">
		<div id="carouselIndex" class="carousel slide carousel-fade" data-ride="carousel">
		  <ol class="carousel-indicators">
			<li data-target="#carouselIndex" data-slide-to="0" class="active"></li>
			<li data-target="#carouselIndex" data-slide-to="1"></li>
			<li data-target="#carouselIndex" data-slide-to="2"></li>
			<li data-target="#carouselIndex" data-slide-to="3"></li>
			<li data-target="#carouselIndex" data-slide-to="4"></li>
			<li data-target="#carouselIndex" data-slide-to="5"></li>
			<li data-target="#carouselIndex" data-slide-to="6"></li>
			<li data-target="#carouselIndex" data-slide-to="7"></li>
			<li data-target="#carouselIndex" data-slide-to="8"></li>
		  </ol>
		  <div class="carousel-inner">
			<div class="carousel-item active">
		<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/parapente-a-motor-puerto-lopez-ecuador.jpg" alt="Paragliding Ecuador - Playa Bruja">
			<div class="carousel-caption">
			  <h2>Paragliding with a motor Puerto Lopez</h2>
			  <p>Flying over the Machalilla National Park</p>
			</div>
			
		</div>
		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/cursos_parapente_ecuador.jpg" alt="Paragliding Courses Ecuador">
			<div class="carousel-caption">
			  <h2>Paragliding Courses Ecuador</h2>
			  <p>Learning to fly a paraglider.</p>
			</div>
		</div>

		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/parapente-montanita-ecuador.jpg" alt="Paragliding Ecuador">
			<div class="carousel-caption">
			  <h2>Paragliding Ecuador</h2>
			  <p>Flying over the beach and ocean.</p>
			</div>
		</div>

		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/parapente-extremo-aventura.jpg" alt="View of the ecuador coastline while Paragliding Baños">
			<div class="carousel-caption">
			  <h2>Paragliding Ba&ntilde;os Ecuador</h2>
			  <p>Flying over pelileo, a unforgetable adventure</p>
			</div>
		</div>

		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/parapente-turismo-aventura.jpg" alt="Paragliding - A extreme adventure Sport">
			<div class="carousel-caption">
			  <h2>Adventure Spot Ecuador</h2>
			  <p>Flying Paragliding in the sunset of the ecuadorian coast</p>
			</div>
		</div>
		
		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente-guayaquil/parapente-guayaquil.jpg" alt="Paragliding Guayaquil Ecuador- Adventure Tourism">
			<div class="carousel-caption">
			  <h2>Paragliding Guayaquil Ecuador</h2>
			  <p>Flying on the road to the coast in Guayaquil</p>
			</div>
		</div>
		
		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/curso_parapente_ecuador.jpg" alt="Paragliding Courses Ecuador">
			<div class="carousel-caption">
			  <h2>Paragliding Courses Ecuador</h2>
			  <p>Learning to fly a paraglider.</p>
			</div>
		</div>

		<div class="carousel-item ">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/parapente-parque-nacinal-machalilla.jpg" alt="Paragliding Montanita on ruta del spondylus">
			<div class="carousel-caption">
			  <h2>Paragliding on Ruta del Spondylus, Ecuador</h2>
			  <p>Flying over the sea Puerto Lopez</p>
			</div>
		</div>
		<div class="carousel-item ">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/parapente-ruta-spondulus.jpg" alt="Paragliding por la ruta del spondylus">
			<div class="carousel-caption">
			  <h2>Paragliding Montañita, Ecuador</h2>
			  <p>Flying with friends at the beach</p>
			</div>
		</div>
		
		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/paragliding-parapente.jpg" alt="Paragliding Ibarra Ecuador - Turismo de aventura">
			<div class="carousel-caption">
			  <h2>Paragliding  Ibarra Ecuador - Adventure Tourism</h2>
			  <p>Flying Paragliding over the lake of Yahuarcocha-Ibarra</p>
			</div>
		</div>
		
		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/parapente-ecuador.jpg" alt="Parapente Montanita Ecuador Deporte de Aventura">
			<div class="carousel-caption">
			  <h2>Paragliding Montanita Ecuador </h2>
			  <p>En el mirador de Playa Bruja volando en las mejores lugares tur&iacute;sticos</p>
			</div>
		</div>


		  </div>
		  <a class="carousel-control-prev" href="#carouselIndex" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#carouselIndex" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		  </a>
		</div>
		</div>
    </div>
	