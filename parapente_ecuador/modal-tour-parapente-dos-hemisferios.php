
<div class="modal fade" id="modalToursParapenteDosHemosferios" tabindex="-1" role="dialog" aria-labelledby="modalToursParapenteDosHemosferios" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalToursParapenteDosHemosferios">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="col-12">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6">
						
						
					<div id="carouselDosHemisferios" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/tour_parapente/tour-parapente-ecuador.jpg" alt="Tours Parapente Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/tour_parapente/parapente-tours-ecuador.jpg" alt="Tours Parapente Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/tour_parapente/paragliding-tours-ecuador.jpg" alt="Tours Parapente Ecuador">
							</div>
							<div class="carousel-item">
							  
							  <img class="" src="https://opeturmo.com/images_parapente/tour_parapente/paragliding-tour-ecuador.jpg" alt="Tours Parapente Ecuador">
							</div>
							<div class="carousel-item">
							  
							  <img class="" src="https://opeturmo.com/images_parapente/tour_parapente/tours-parapente-en-ecuador.jpg" alt="Tours Parapente Ecuador">
							</div>
							<div class="carousel-item">
							  
							  <img class="" src="https://opeturmo.com/images_parapente/tour_parapente/parapente-tours-in-ecuador.jpg" alt="Tours Parapente Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselDosHemisferios" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/tour_parapente/tour-parapente-ecuador.jpg" alt="Tours Parapente Ecuador"></li>
							<li data-target="#carouselDosHemisferios" data-slide-to="1"><img class="" src="https://opeturmo.com/images_parapente/tour_parapente/parapente-tours-ecuador.jpg" alt="Tours Parapente Ecuador"></li>
							<li data-target="#carouselDosHemisferios" data-slide-to="2"><img class=" " src="https://opeturmo.com/images_parapente/tour_parapente/paragliding-tours-ecuador.jpg" alt="Tours Parapente Ecuador"></li>
							<li data-target="#carouselDosHemisferios" data-slide-to="3"><img class="" src="https://opeturmo.com/images_parapente/tour_parapente/paragliding-tour-ecuador.jpg" alt="Tours Parapente Ecuador"></li>
							<li data-target="#carouselDosHemisferios" data-slide-to="4"><img class="" src="https://opeturmo.com/images_parapente/tour_parapente/tours-parapente-en-ecuador.jpg" alt="Tours Parapente Ecuador"></li>
							<li data-target="#carouselDosHemisferios" data-slide-to="5"><img class="" src="https://opeturmo.com/images_parapente/tour_parapente/parapente-tours-in-ecuador.jpg" alt="Tours Parapente Ecuador"></li>
						  </ol>
						
						</div>
					</div>
				</div>	
				</div>	
				</div>	
			
        <div class="row">
			<div class="col-12">
			
					<div class="row">
							<div class="col-12">
								<header><h4>Tour Parapente Dos Hemisferios</h4></header>
							</div>
						</div>
				<div class="row">
					<div class="col-12 col-sm-6 col-md-6">
						<div class="row">
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Dia 1 - Vuelos en Playa Bruja - Libertador Bolívar</h5></header>
									<p>Despegue de Playa Bruja y San Pedro, vuelo en la ladera de Playa Bruja. (Vuelo dinámico, altura del cerro 72m), por la tarde vuelo en San Pedro 80 m. y 3 km. de largo, se puede subir a 200 m.  </p>
									<p>Libertador Bolívar está a 10 minutos antes de Montañita, al norte de la provincia de Santa Elena a 2 horas y media de Guayaquil. </p>
									<p>Alojamiento en Montañita Hostal junto al mar, Montañita se caracteriza por tener fiesta nocturna.</p>
								</article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Dia 2 - Vuelos en Puerto López - Salango. </h5></header>
									<p>  Después del desayuno, traslado hacia Puerto López. Vuelo en la loma grande 300m. de altura (térmico) se puede subir a 800 metros o de Salango 100 m. (dinámico). Se puede subir a 300 m, despegue que tiene una maravillosa vista de la isla de Salango, piqueo al medio día. Vuelos en la tarde. Visita de la Fábrica de Aceite de Palo Santo. </p> 
									<p>Puerto López es una población pesquera situada en el extremo suroeste de la Provincia de Manabí, en Ecuador. Se encuentra dentro del Parque Nacional Machalilla con una reserva de 56.184 hectáreas. </p>
									<p>Alojamiento en hostal Itapoa frente al mar. </p>
								</article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Dia 3 </h5></header>
									<p>Después del desayuno 8:00 am posibilidad de volar en la loma grande o Salango, 11am tour costero: snorkeling y kayak actividad que se realiza dentro del área protegida marina de Puerto López con una gran biodiversidad de vida marina, retorno 3pm. Traslado a Crucita con probabilidad de volar en San José de Montecristi a 1 hora de Puerto López.</p> 
									<p>Alojamiento en Crucita Hostal Voladores con piscina y junto al mar. </p>
								</article>
							</section>
						</div>
					</div>
					<div class="col-12 col-sm-6 col-md-6">
						<div class="row">
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Dia 4 - Vuelos en Crucita </h5></header>									 
									<p>Después del desayuno en el Hostal Voladores. Vuelo en la ladera de Crucita, posibilidad de subir 350 metros 14km de largo de ida y vuelta.</p>
									<p>Crucita es un pueblo de pescadores y turista junto al mar. En la ladera se puede jugar todo el día, muy fácil para el aterrizaje arriba o Top landing, después de aterrizar en la playa, alojamiento en el mismo Hostal voladores, por la noche visita de la ciudad de Montecristi capital del sombrero panameño original de Ecuador, visita de la ciudad de Manta.</p> 
								</article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Dia 5</h5></header>
									<p> Vuelos cortos en la mañana en la loma de Crucita, salida a Canoa a 1:30 min, directo a la loma de vuelo, se puede subir a 500 m. de altura y 40 km. de ida y vuelta, hospedaje en "Sol y Luna" Hostal con piscina y frente al mar, se puede aterrizar frente al hotel.  </p>
									<p>Por la noche malecón de Canoa. </p>
								</article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Dia 6 - HEMISFERIO NORTE </h5></header>
									<p>Después del desayuno 8am, traslado a Pedernales, 2 horas, parada de 10 a 15 minutos en la línea ecuatorial, demostraremos el efecto Coriolis en la línea Ecuador con agua.  </p>
									<p>Vuelos en la ladera de Pedernales se puede subir a 200 m. y 20 km. de ida y vuelta. Piqueo en Cojimíes a 10 min del despegue y alojamiento en la misma localidad. </p>
								</article>
							</section>
							<section class="col-md-12">
								<article class="tours_pax">
									<header><h5>Dia 7</h5></header>
									<p> Después del desayuno 8am el grupo decidirá en que zona de vuelo quiere volar de regreso al Sur: Pedernales, Canoa, Crucita, Puerto López. 5 horas de retorno hasta Puerto López. Transfer a Guayaquil (coordinar con el grupo). </p>
								</article>
							</section>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class= "col-12 col-md-6">
				<p><b>Incluye: </b></p>
				<ul>
				<li>Hospedaje tipo turista.</li>
				<li>Desayuno. </li>
				<li> Transporte. </li>
				<li> Guía / Chofer. </li>
				<li>Servicio por operación (Opeturmo S.A.) </li>
				<li>Logística del tour. </li>
				<li>Tour Costero o Tour Avistamiento de Ballenas.</li>
				<li> Certificación APPI P2 </li>
				</ul>
			</div>
			<div class= "col-12 col-md-6">
				<p><b>No incluye:</b></p>
				<ul>
				<li> Alimentación y bebidas. </li>
				<li> Valor de entradas a posibles parques Nacionales o reservas. </li>
				</ul>
 
				<p><b>Requerimientos para el piloto: </b></p>
				<ul>				
				<li>El piloto debe tener mínimo un año o 60 horas de vuelo.</li>
				<li> Parapente completo.</li>
				<li>Paracaídas de emergencia.</li>
				<li>Arnés con protección dorsal.</li>
				<li>Radio con frecuencia abierta UHF.</li>
				<li>Casco homologado para vuelo libre.</li>
				<li> Botas o zapatos caña alta. </li>
				</ul>
			</div>
		</div>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>