<?php get_header(); ?>

<div class="row blog_row">
	<div class="col-12 col-md-9">
		<?php if (have_posts()) : while (have_posts()) : the_post();     ?>
		<div class="blog_post"<?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header class="header">
				<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
			</header>


			<p><?php include (TEMPLATEPATH . '/inc/meta.php' ); ?></p>

			<div class="entry">
				<?php the_content(); ?>
			</div>

			<div class="postmetadata">
				<?php the_tags('Tags: ', ', ', '<br />'); ?>
				Posted in <?php the_category(', ') ?> | 
				<?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
			</div>

		</div>

		<?php endwhile; ?>

		<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

		<?php else : ?>

			<h2>Not Found</h2>

		<?php endif; ?>
	</div>
	
	<?php get_sidebar(); ?>


</div>

	<?php get_footer(); ?>