<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>

	<title>
		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
	</title>
	
	<link rel="shortcut icon" href="/favicon.ico">
	
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php if ( is_singular() ) wp_enqueue_script('comment-reply'); ?>
	
		<meta name="author" content="Developer: Natasha Carla Nodine Wyatt">
	<link rel="icon" href="https://opeturmo.com/logo_ecuador_parapente.ico" type="image/ico" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://opeturmo.com/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<meta name="msvalidate.01" content="380B1B2C798B6CBEBC9C63EECEE117EF" />
	<script type="text/javascript">
  (function(config) {
    window._peekConfig = config || {};
    var idPrefix = 'peek-book-button';
    var id = idPrefix+'-js'; if (document.getElementById(id)) return;
    var head = document.getElementsByTagName('head')[0];
    var el = document.createElement('script'); el.id = id;
    var date = new Date; var stamp = date.getMonth()+"-"+date.getDate();
    var basePath = "https://js.peek.com";
    el.src = basePath + "/widget_button.js?ts="+stamp;
    head.appendChild(el); id = idPrefix+'-css'; el = document.createElement('link'); el.id = id;
    el.href = basePath + "/widget_button.css?ts="+stamp;
    el.rel="stylesheet"; el.type="text/css"; head.appendChild(el);
  })({key: '4aaee0fc-19f9-43da-8667-9972fe009432'});
</script>
	
	<script >
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
	$.src="//v2.zopim.com/?3oC2gd9KO6sPaEmter4wep7sCRwDZT9X";z.t=+new Date;$.
	type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
	</script>
	
	
	<!--<script src="https://apis.google.com/js/platform.js" async defer>
	  {lang: 'en-GB'}
	</script>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-78803847-1', 'auto');
  ga('send', 'pageview');

	</script>-->
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78803847-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78803847-1');
</script>

	<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '377908959422554');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=377908959422554&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

	
	
	
	
	
	
	
	
	

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	

		
		<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
  <a class="navbar-brand" href="https://opeturmo.com/"><img src="https://opeturmo.com/images_parapente/logos/logo_opeturmo_menu.png" width="30" height="30" alt="">Inicio</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse " id="navbarNavDropdown">
    <ul class="navbar-nav  mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="https://opeturmo.com/#vuelos_parapente">Vuelos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://opeturmo.com/#pedida_mano">Pedida de Mano</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://opeturmo.com/#tours">Tours</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://opeturmo.com/#cursos_parapente">Cursos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://opeturmo.com/#contactos">Contactos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://opeturmo.com/blog/es/">Blog</a>
      </li>
      <li class="nav-item">
        <a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/rwRb" class="peek-book-btn-blue peek-book-button-flat peek-book-button-sm" data-button-text="Reservar">Reservar</a>
      </li>
	  </ul>
	   <ul class="navbar-nav">
      <li class="nav-item language">
        <?php  pll_the_languages(array('show_flags'=>1,'show_names'=>0));?>
      </li>
    </ul>
	 
  </div>
</nav>



	<div class="container-fluid">