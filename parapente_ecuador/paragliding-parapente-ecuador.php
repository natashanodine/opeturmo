  <div class="row">
		<div class="col-12 col-sm-12 col-md-12">
		<div id="carouselIndex" class="carousel slide carousel-fade" data-ride="carousel">
		  <ol class="carousel-indicators">
			<li data-target="#carouselIndex" data-slide-to="0" class="active"></li>
			<li data-target="#carouselIndex" data-slide-to="1"></li>
			<li data-target="#carouselIndex" data-slide-to="2"></li>
			<li data-target="#carouselIndex" data-slide-to="3"></li>
			<li data-target="#carouselIndex" data-slide-to="4"></li>
			<li data-target="#carouselIndex" data-slide-to="5"></li>
			<li data-target="#carouselIndex" data-slide-to="6"></li>
			<li data-target="#carouselIndex" data-slide-to="7"></li>
			<li data-target="#carouselIndex" data-slide-to="8"></li>
		  </ol>
		  <div class="carousel-inner">
			<div class="carousel-item active">
		<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/parapente-a-motor-puerto-lopez-ecuador.jpg" alt="Parapente Ecuador - Playa Bruja">
			<div class="carousel-caption">
			  <h2>Parapente a Motor Puerto Lopez</h2>
			  <p>Volando sobre el parque nacinal machalilla mirando los Frailers</p>
			</div>
			
		</div>
		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/parapente-montanita-ecuador.jpg" alt="Paisaje de la costa ecuatoriana en Vuelo Parapente Ecuador">
			<div class="carousel-caption">
			  <h2>Parapente Ecuador</h2>
			  <p>volando sobre la playa y el mar.</p>
			</div>
		</div>

		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/cursos_parapente_ecuador.jpg" alt="Cursos Parapente Ecuador">
			<div class="carousel-caption">
			  <h2>Cursos Parapente Ecuador</h2>
			  <p>Aprendiendo a volar parapente.</p>
			</div>
		</div>

		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/parapente-extremo-aventura.jpg" alt="Vuelos Parapente Baños Ecuador">
			<div class="carousel-caption">
			  <h2>Parapente Ba&ntilde;os Ecuador</h2>
			  <p>volando sobre pelileo, una aventura inolvidable</p>
			</div>
		</div>

		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/parapente-turismo-aventura.jpg" alt="Parapente - Un deporte extremo de aventura">
			<div class="carousel-caption">
			  <h2>Deporte de Aventura Ecuador</h2>
			  <p>Volando Parapente en el atardecer en la playa ecuatoriana</p>
			</div>
		</div>
		
		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente-guayaquil/parapente-guayaquil.jpg" alt="Parapente Guayaquil Ecuador- Turismo de Aventura">
			<div class="carousel-caption">
			  <h2>Parapente Guayaquil Ecuador</h2>
			  <p>Volando sobre via la costa en el cerro blanco</p>
			</div>
		</div>
		
		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/curso_parapente_ecuador.jpg" alt="Cursos Parapente Ecuador">
			<div class="carousel-caption">
			  <h2>Cursos Parapente Ecuador</h2>
			  <p>Aprendiendo a volar parapente.</p>
			</div>
		</div>


		<div class="carousel-item ">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/parapente-parque-nacinal-machalilla.jpg" alt="Parapente por la ruta del spondylus">
			<div class="carousel-caption">
			  <h2>Parapente en la Ruta del Spondylus, Ecuador</h2>
			  <p>Volando sobre el mar en Puerto Lopez</p>
			</div>
		</div>
		<div class="carousel-item ">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/parapente-ruta-spondulus.jpg" alt="Parapente Montañita Ecuador">
			<div class="carousel-caption">
			  <h2>Parapente Montañita, Ecuador</h2>
			  <p>Volando con amigos en la playa</p>
			</div>
		</div>
		
		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/paragliding-parapente.jpg" alt="Parapente Ibarra Ecuador - Turismo de aventura">
			<div class="carousel-caption">
			  <h2>Parapente  Ibarra Ecuador - Turismo de aventura</h2>
			  <p>Volando Parapente sobre la laguna de Yahuarcocha-Ibarra</p>
			</div>
		</div>

		<div class="carousel-item">
			<img class="d-block w-100"  src="https://opeturmo.com/images_parapente/parapente_paragliding_index/parapente-ecuador.jpg" alt="Parapente Montañita Ecuador Deporte de Aventura">
			<div class="carousel-caption">
			  <h2>Parapente Montanita Ecuador </h2>
			  <p>In the viewpoint of Playa Bruja flying in the best tourist places</p>
			</div>
		</div>

		  </div>
		  <a class="carousel-control-prev" href="#carouselIndex" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#carouselIndex" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		  </a>
		</div>
		</div>
    </div>
	