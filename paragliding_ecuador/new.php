<div class="modal fade" id="IslaSeymourModal" tabindex="-1" role="dialog" aria-labelledby="IslaSeymourModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="IslaSeymourModalLabel">Isla Seymour</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselIslaSeymour" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="images_tours/galapagos/galapagos_ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="images_tours/galapagos/lobo_galapagos_ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselIslaSeymour" data-slide-to="0" class="active"><img class=" " src="images_tours/galapagos/galapagos_ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
							<li data-target="#carouselIslaSeymour" data-slide-to="1"><img class=" " src="images_tours/galapagos/lobo_galapagos_ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Isla Seymour</h2><header>
											<p>
									En el Canal de Itabaca, se abordar&aacute; la embarcaci&oacute;n hacia la isla de seymour con una
									navegaci&oacute;n aproximadamente de 1:15 y en donde podr&aacute; observar colonias de fragatas, piqueros patas azules, lobos e
									iguanas marinas. Visita a playa "Las Bachas" sitio ideal para nadar y hacer snorkel.
									</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="BartolomeModal" tabindex="-1" role="dialog" aria-labelledby="BartolomeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BartolomeModalLabel">Bartolome</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselBartolome" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="images_tours/galapagos/pinguino_galapagos_ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="images_tours/galapagos/galapagos-islands.jpg" alt="Tours Galapagos Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselBartolome" data-slide-to="0" class="active"><img class=" " src="images_tours/galapagos/pinguino_galapagos_ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
							<li data-target="#carouselBartolome" data-slide-to="1"><img class=" " src="images_tours/galapagos/galapagos-islands.jpg" alt="Tours Galapagos Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Bartolome</h2><header>
											<p>
									Desde el Canal de Itabaca e iniciar la navegaci&oacute;n hacia Bartolom&eacute;;  tiempo aproximado de navegaci&oacute;n 2
									horas; se observa: piqueros, lava, pinguinos, lobos marinos y hermosa vista panor&aacute;mica a la Isla Santiago. Excelente
									sitio para nadar y hacer snorkel.
									</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>





<div class="modal fade" id="SantaFeModal" tabindex="-1" role="dialog" aria-labelledby="BartolomeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BartolomeModalLabel">Santa Fe</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselSantaFe" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="images_tours/galapagos/iguana-galapagos-islands.jpg" alt="Tours Galapagos Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="images_tours/galapagos/tour-galapagos-islands-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselSantaFe" data-slide-to="0" class="active"><img class=" " src="images_tours/galapagos/iguana-galapagos-islands.jpg" alt="Tours Galapagos Ecuador"></li>
							<li data-target="#carouselSantaFe" data-slide-to="1"><img class=" " src="images_tours/galapagos/tour-galapagos-islands-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Santa Fe</h2><header>
											<p>
									Se sale del muelle de turistas en Puerto Ayora en la embarcaci&oacute;n hacia la isla Santa Fe, la misma que se encuentra
									ubicada entre San Crist&oacute;bal y Santa Cruz. Podr&aacute; observar grandes colonias de lobos marinos, iguanas marinas,
									pelicanos, diferentes variedades de peces, piqueros enmascarados, piqueros patas azules. Sitio muy recomendado
									para hacer snorkel y nadar.
									</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="FloreanaModal" tabindex="-1" role="dialog" aria-labelledby="BartolomeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BartolomeModalLabel">Floreana</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselFloreana" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="images_tours/galapagos/galapagos-islands-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="images_tours/galapagos/tours-islas-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselFloreana" data-slide-to="0" class="active"><img class=" " src="images_tours/galapagos/galapagos-islands-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
							<li data-target="#carouselFloreana" data-slide-to="1"><img class=" " src="images_tours/galapagos/tours-islas-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Floreana</h2><header>
											<p>
									Salida desde el muelle de
									turistas en Puerto Ayora y tomar la embarcaci&oacute;n hacia Floreana. Tour de snorkel en: Playa negra y lober&iacute;a, para
									observar seg&uacute;n la temporada: delfines, lobos marinos, tortugas, tiburones, y variedad de peces seg&uacute;n la temporada.
									Visita al Puerto Velasco Ibarra para observar las ruinas de los piratas y vertientes de agua en la parte alta de la Isla.
									Tiempo de navegaci&oacute;n: Puerto Ayora- Floreana 2h50
									.</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="PlazaPuntaCarrionModal" tabindex="-1" role="dialog" aria-labelledby="BartolomeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BartolomeModalLabel"> Plaza y Punta Carrion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselPlazaPuntaCarrion" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="images_tours/galapagos/albatrosses-tour-galapagos-islands-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="images_tours/galapagos/booby-galapagos-islands.jpg" alt="Tours Galapagos Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselPlazaPuntaCarrion" data-slide-to="0" class="active"><img class=" " src="images_tours/galapagos/albatrosses-tour-galapagos-islands-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
							<li data-target="#carouselPlazaPuntaCarrion" data-slide-to="1"><img class=" " src="images_tours/galapagos/booby-galapagos-islands.jpg" alt="Tours Galapagos Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2> Plaza y Punta Carrion</h2><header>
											 <p>
									Salida desde el muelle de turistas o Canal de Itabaca. Al arribo podr&aacute; observar una maravillosa vista del lugar, Plaza sur est&aacute; cubierta de una
									vegetaci&oacute;n baja de color rojizo llamada sesuvium lo cual la hace muy atractiva, podemos ver: gaviotas, lagartijas,
									p&aacute;jaro tropical, piqueros de patas azules, piqueros enmascarados, colonia de lobos marinos e iguanas terrestres.
									</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="SanCristobalModal" tabindex="-1" role="dialog" aria-labelledby="BartolomeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BartolomeModalLabel">San Cristobal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselSanCristobal" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="images_tours/galapagos/tours-galapagos-islands-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="images_tours/galapagos/tours-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselSanCristobal" data-slide-to="0" class="active"><img class=" " src="images_tours/galapagos/tours-galapagos-islands-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
							<li data-target="#carouselSanCristobal" data-slide-to="1"><img class=" " src="images_tours/galapagos/tours-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>San Cristobal</h2><header>
											<p>Hora de salida del muelle 07 a.m.
									Navegaci&oacute;n aproximada de 2 horas hacia Puerto Baquerizo Moreno nos dirigimos en embarcaci&oacute;n hacia Rocas
									Kicker ( Le&oacute;n Dormido) para realizar Snorkell, en este sitio hay mucha actividad se puede observar , tiburones,
									mantas rayas lobos marinos , peces, tortugas marinas, luego se visita Isla Lobos para observar fragatas , piqueros,
									lobos marinos, y descansamos en la playa Manglecito para tomar el box lunch y nadar. Retorno a Puerto Ayora
									6H00 p.m. aproximadamente
									</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="IsabelaModal" tabindex="-1" role="dialog" aria-labelledby="IsabelaModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="IsabelaModalLabel">Isabela</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselIsabela" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="images_tours/galapagos/bird-tours-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="images_tours/galapagos/cactus-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselIsabela" data-slide-to="0" class="active"><img class=" " src="images_tours/galapagos/bird-tours-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
							<li data-target="#carouselIsabela" data-slide-to="1"><img class=" " src="images_tours/galapagos/cactus-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Isabela</h2><header>
											<p>
									Recojida en Hotel: 06h50am. 
									Navegaci&oacute;n (2 horas de viaje) Arribo a la isla Isabela, donde el gu&iacute;a dar&aacute; indicaciones de las actividades a realizar
									tomamos un bus o taxi que los lleva hacia los Humedales donde se encontrar&aacute;n con: laguna de los Flamingos, centro
									de crianza de tortugas gigantes un peque&ntilde;o centro de interpretaci&oacute;n del Parque Nacional Gal&aacute;pagos y la playa,
									tambi&eacute;n es posible encontrarnos con iguanas marinas, lagartijas de lava, plantas volc&aacute;nicas como el cactus de lava, la
									escalesia, entre otras. Nos dirigimos al muelle para tomar una embarcaci&oacute;n para visitar el islote tintoreras, donde se realiza un desembarque
									seco caminata de 20 minutos para apreciar los tiburones de aleta blanca, iguanas, piqueros patas azules, pinguinos,
									realizamos snorkell fuera del islote y podemos observar, manta rayas, lobos marinos, tortugas marinas.
									</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="BahiaModal" tabindex="-1" role="dialog" aria-labelledby="BahiaModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BahiaModalLabel">Tour de Bahia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
									
								
									<div class="row">
										<div class="col-12 col-sm-6 col-md-6">
											
											
						<div id="carouselBahia" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="images_tours/galapagos/tortoise-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
							<div class="carousel-item">
							  <img class="" src="images_tours/galapagos/pelikan-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador">
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselBahia" data-slide-to="0" class="active"><img class=" " src="images_tours/galapagos/tortoise-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
							<li data-target="#carouselBahia" data-slide-to="1"><img class=" " src="images_tours/galapagos/pelikan-galapagos-ecuador.jpg" alt="Tours Galapagos Ecuador"></li>
						  </ol>
						
						</div>
										</div><div class="col-12 col-sm-6 col-md-6">
										<header><h2>Tour de Bahia</h2><header>
											<p>
									Se visita la  Isla “la  Lobería” llamada as&iacute; por la gran colonia de lobos marinos  donde se  realiza snorkel con los lobos  marinos, es un sitio  muy especial para  ver gran variedad de peces de colores, diversidad  de especies  del fondo marino.
									Luego nos dirigimos a Punta Estrada  se puede observar  animales tales como  gaviotines, piqueros, fragatas, lobos marinos, manta raya,  ETC.
									Visita canal de los tiburones se observa los tiburones y  desde el Mirador se puede apreciar el puerto y las monta&ntilde;as  de la  parte alta de la Isla
									islote tintoreras, donde se realiza un desembarque seco caminata de 20 minutos para apreciar los tiburones de aleta blanca, iguanas, piqueros patas azules, pingüinos, realizamos snorkell  fuera del islote  y podemos observar, manta  rayas, lobos marinos, tortugas marinas. 
									Visita Playa de los Perros para observar colonia de iguanas marinas, lobos marinos  y vista paisaj&iacute;stica.
									Visita al canal del  amor  hermoso canal de agua dormidas.
									Visita las Grietas, piscina  natural de aguas filtradas, excelente para  hacer snorkell.
									</p>
										</div>
									</div>
									
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

