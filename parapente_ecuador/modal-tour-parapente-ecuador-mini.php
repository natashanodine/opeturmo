<div class="modal fade" id="MiniParapenteModal" tabindex="-1" role="dialog" aria-labelledby="MiniParapenteModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal_tours" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="MiniParapenteModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

	  
	   <div class="row">
											<div class="col-md-6 col-sm-12 tour_col_left">
												<div class="tours_pax">
													<p>DIA 1</p>
													<ul>
													<li>Vuelo en parapente en Guayaquil</li>
													<li>Traslado a La Troncal</li> 
													</ul>
												</div>
												<div class="tours_pax">
													<p>DIA 2</p>
													<ul>
													<li>Vuelo en parapente en La Troncal</li>
													<li>Traslado a Alausi</li>
													</ul>
												</div>
												
												<div class="tours_pax">
													<p>DIA 3</p>
													<ul>
													<li>Vuelo en parapente en Cochabamba Gonzol </li>
													<li>Recorrido en tren Alausi - Sibambe - Alausi,</li>
													<li>Traslado Riobamba</li>
													</ul>
												</div>

											</div>

											<div class="col-md-6 col-sm-12 tour_col_right">
													
												<div class="tours_pax">
													<p>DIA 4</p>
													<ul>
													<li>Vuelo en parapente en Tunshi</li> 
													<li>City tour de Riobamba</li>
													<li>Salida a Baños de Agua Santa</li>
													</ul>
												</div>
													
												<div class="tours_pax">
													<p>DIA 5</p>
													<ul>
													<li>Vuelo en parapente Niton</li>
													<li>Hospedaje Baños</li>
													</ul>
												</div>
													
												<div class="tours_pax">
													<p>DIA 6</p>
													<ul>
													<li>Vuelo en parapente Niton</li>
													<li>Salida Guayaquil</li>
													</ul>
												</div>
											</div>
										</div>
	  
	  
									
 </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>