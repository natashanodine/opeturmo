<div class="modal fade" id="reserveTandemModal" tabindex="-1" role="dialog" aria-labelledby="reserveTandemModal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="reserveTandemModal">Contact Us </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
				<form role='form' class='form' action='https://opeturmo.com/parapente_ecuador/tandem_form.php' method='post'>
					
					<div class="form-group row">
					<label for="name" class="col-sm-2 col-form-label">Name:</label>
					<div class="col-sm-10">
					  <input type="text" class="form-control" name="name" id="name" placeholder="Name" required >
					</div>
				  </div>
					<div class="form-group row">
					<label for="tel" class="col-sm-2 col-form-label">Telephone:</label>
					<div class="col-sm-10">
					  <input type="number" class="form-control" name="tel"  id="tel" placeholder="Telephone" >
					</div>
				  </div>
					<div class="form-group row">
					<label for="email" class="col-sm-2 col-form-label">Email</label>
					<div class="col-sm-10">
					  <input type="email" class="form-control" name="email" id="email" placeholder="Email" required >
					</div>
				  </div>
				  <div class="form-group row">
						<label for="date" class="col-sm-2 col-form-label">Date:</label>
						<div class="col-sm-10">
						<input type="date" id="date" name="date">
						</div>										
					</div>	
					
					<div class="form-group row">
					<label for="comment" class="col-sm-2 col-form-label">Menssage</label>
					<div class="col-sm-10">
						<textarea class='form-control' rows='2' name='comment' placeholder='Menssage' required ></textarea>
					</div>
				  </div>
														
					
					<p>Where do you want to fly?</p>
					<div class="row">
					<div class="col-12 col-sm-6 col-md-6">
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Monta&ntilde;ita">Paragliding Monta&ntilde;ita</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Guayaquil">Paragliding Guayaquil</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Puerto Lopez">Paragliding Puerto L&oacute;pez</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Quito">Paragliding Quito</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Ibarra">Paragliding Ibarra</label>
					</div>
					</div>
					<div class="col-12 col-sm-6 col-md-6">

					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Crucita">Paragliding Crucita</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Canoa">Paragliding Canoa</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Ba&ntilde;os">Parapente Ba&ntilde;os</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="radio" value="Parapente Cuenca">Paragliding Cuenca</label>
					</div>
					</div>
					</div>
					<div class="form-group row ">
					<div class="col-12"> 
					<div class="captcha_wrapper">
						<div class="g-recaptcha" data-sitekey="6LcPkZEUAAAAAPbw2G7UDZ6RSXQBegbV6Fd79PCK"></div>
					</div>
					</div>
				  </div>
				  
					<div class="form-group row contact-button">
					<div class="col-sm-12">
					  <button type="submit" class="btn button-contact">SEND</button>
					</div>
				  </div>
				</form>
			 </div>
      <div class="modal-footer">
				
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
 
 
 
 
 
 
 
 
 
 
 
 
<div class="content-desktop">
 
 <div class="row header_row">
		<div class="col-12">
		<header><h1>Paragliding Flights Ecuador</h1></header>
		</div>
	</div>
<div class="row">
	<div class="accordion" id="accordion_vuelos">
		 <div class="row  vuelos">
			<div class="col-2 col-sm-2 col-md-2 vuelos_box">
			<div class="card card-vuelos" data-toggle="collapse" data-target="#collapseMontanita" aria-expanded="true" aria-controls="collapseMontanita">
			    <header><h1>Montanita</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/vuelos-parapente-playa-montanita-ecuador.png" alt="Paragliding Montanita Ecuador">
				<button>More...</button>
			</div>
			</div>
			<div class="col-2  col-sm-2 col-md-2 vuelos_box">
			<div class="card  card-vuelos collapsed" data-toggle="collapse" data-target="#collapsePLopez" aria-expanded="false" aria-controls="collapsePLopez">
				 <header><h1>Puerto Lopez</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/vuelos-parapente-puerto-lopez-ecuador.png" alt="Paragliding Puerto Lopez Ecuador">
				<button>More...</button>
			</div>
			</div>
			<div class="col-2  col-sm-2 col-md-2 vuelos_box">
			<div class="card card-vuelos collapsed" data-toggle="collapse" data-target="#collapseGuayaquil" aria-expanded="false" aria-controls="collapseGuayaquil">
				 <header><h1>Guayaquil</h1></header>	  
				<img class=" " src="https://opeturmo.com/images_parapente/parapente-guayaquil/vuelos-parapente-guayaquil-ecuador.png" alt="Paragliding Guayaquil Ecuador">
				<button>More...</button>
			</div>
			</div>
			<div class="col-2  col-sm-2 col-md-2 vuelos_box">
			<div class="card card-vuelos collapsed" data-toggle="collapse" data-target="#collapseQuito" aria-expanded="false" aria-controls="collapseQuito">
				 <header><h1>Quito</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-quito-ecuador.png" alt="Paragliding Quito Ecuador">
				<button>More...</button>
			</div>
			</div>
			<div class="col-2  col-sm-2 col-md-2 vuelos_box">
			<div class="card  card-vuelos collapsed" data-toggle="collapse" data-target="#collapseIbarra" aria-expanded="false" aria-controls="collapseIbarra">
				  <header><h1>Ibarra</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-ibarra-ecuador.png" alt="Paragliding Ibarra Ecuador">
				<button>More...</button>
			</div>
			</div>
			<div class="col-2 col-sm-2  col-md-2 vuelos_box">
			<div class="card  card-vuelos collapsed" data-toggle="collapse" data-target="#collapseOtros" aria-expanded="false" aria-controls="collapseOtros">
				 <header><h1>Other places</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-crucita/vuelos-parapente-crucita-ecuador.png" alt="Paragliding Canoa Ecuador">
				<button>More...</button>
			</div>
		</div>
		</div>
		
		 <div class="row vuelos_details">
			<div class="card col-md-12">
				<div id="collapseMontanita" class="collapse show" aria-labelledby="headingMontanita" data-parent="#accordion_vuelos">
				  <div class="card-body row">
					<div class="col-md-4">
						
						
						<div id="carouselMontanita" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-playa-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador">
								<div class="carousel-caption">
								  <h2>Parapegliding in Monta&ntilde;ita</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/playa-parapente-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapegliding in Monta&ntilde;ita</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-montanita.jpg" alt="Parapgliding Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapegliding in Monta&ntilde;ita</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-con-motor-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapegliding in Monta&ntilde;ita with a motor</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-a-motor-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapegliding in Monta&ntilde;ita with a motor</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselMontanita" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-playa-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador"></li>
							<li data-target="#carouselMontanita" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/playa-parapente-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador"></li>
							<li data-target="#carouselMontanita" data-slide-to="2"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-montanita.jpg" alt="Parapgliding Montanita Ecuador"></li>
							<li data-target="#carouselMontanita" data-slide-to="3"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-con-motor-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador"></li>
							<li data-target="#carouselMontanita" data-slide-to="4"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-a-motor-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador"></li>
						  </ol>
						
						</div>
						  
						  
											
						
					</div>
					<div class="col-md-8">
					<header><h1>Paragliding Monta&ntilde;ita</h1></header>
					<p>We fly in a paraglider with the pilot guide, who takes you to the adventure activity, in front of the sea on the Ruta del Spondylus (Ruta del Sol). The guide takes care of the whole operation, you just have to enjoy and fulfill your dream of flying. The flight experience lasts between 12-15 minutes.</p>
					<li><b>Paragliding</b> Paragliding flights in front of the sea is a dynamic flight that takes place moving through the skies taking advantage of the mountainside. The sea wind allows us to be able to sustain ourselves in the air and fly like birds.</li>
					<p><b>Promotions:</b></p>
					<ul>
					<li>They fly in Parpente 6 people pay 5</li>
					<li>Free birthday photos</li>
					</ul>
					<li><b>Paragliding with a Motor</b> A flight in motorized Paragliding can cover more distance in the same time, we ascend higher to appreciate the incredible Pacific ocean and the continent of the Ecuadorian profile</li>
					<p>We attend from 11:00 am to 6:00 pm, but this schedule may vary depending on the time of year and day. </p>
										
				 
					<div class="row ">
						
						<div class="col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">    
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a class="content-tablet" href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/lB4k" class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Book Now">Parapente Montanita</a>
							
							</p>
						<p id="social_icons_detail">	
							<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Ecuador,+Ruta+del+Spondylus+E15" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>
														
							<a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Montanita_Santa_Elena_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapentemontanita/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteMontanitaEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						<div class="col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluye" role="button" aria-expanded="false" aria-controls="collapseIncluye">
								INCLUDES: <img class="down-icon" src="../svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluye">
							    <div class="card card-body">
								<ul>			
									
									<li>Local transport if more than two people</li>
									
									<li>Insurance</li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				  </div>
				</div>
			</div>
		  <div class="card col-md-12">
		  <div id="collapsePLopez" class="collapse" aria-labelledby="headingPLopez" data-parent="#accordion_vuelos">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselPLopez" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez.jpg" alt="Parapente Puerto Lopez Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Puerto Lopez</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/vuelos-parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
							<div class="carousel-caption">
								  <h2>Paragliding in Puerto Lopez</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
							<div class="carousel-caption">
								  <h2>Paragliding in Puerto Lopez</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
							<div class="carousel-caption">
								  <h2>Paragliding in Puerto Lopez</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-flights-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
							<div class="carousel-caption">
								  <h2>Paragliding in Puerto Lopez</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselPLopez" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselPLopez" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselPLopez" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
							<li data-target="#carouselPLopez" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/vuelos-parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
							<li data-target="#carouselPLopez" data-slide-to="2"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li><br>
							<li data-target="#carouselPLopez" data-slide-to="3"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
							<li data-target="#carouselPLopez" data-slide-to="4"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-flights-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
						  </ol>
						
						
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Paragliding Puerto L&oacute;pez</h1></header>
						<p>We fly in paragliding with the pilot guide, which takes you to perform the adventure activity of paragliding. The guide is in charge of the whole operation, you just have to enjoy and fulfill your dream of flying.</p>

						<p>We leave the meeting point to the paragliding takeoff in Puerto L&oacute;pez with the 4x4 vehicle, which is located inside the Machalilla National Park and it takes about 20 minutes. From the top of the takeoff you can see the bay of Puerto L&oacute;pez and a large part of the park, such as the island of Salango, the agua blanca (white water) river, etc. and when the sky is clear the isla de la plata (island of silver) is observed.</p>
						<p>We received a technical explain from the guide before the flight to know a bit about how the activity is carried out.</p>
						<p>We fly from the Machalilla National Park over spondylus route and landed on the beach in the harbor of Puerto L&oacute;pez the capital of the sky.</p>
						<p>In case of weather conditions are not suitable for flying in Puerto Lopez we will have the option of moving to another place to take the paragliding flight 10 minutes from Puerto Lopez.</p>
						<p>We attend from 10:30 am to 5:00 p.m.</p>
						
					<div class="row ">
						<div class="col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a class="content-tablet" href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/aL34" class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Book Now" data-button-text="Book Now">Paragliding Puerto Lopez</a>
							</p>
							<p id="social_icons_detail">
							<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Paragliding+Puerto+Lopez" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>	</a>			
															
							<a href="https://www.tripadvisor.com/Attraction_Review-g635730-d11872462-Reviews-Opeturmo-Puerto_Lopez_Manabi_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapentepuertolopez/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapentePuertoLopezEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						
						<div class="col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluye2" role="button" aria-expanded="false" aria-controls="collapseIncluye2">
								INCLUDES: <img class="down-icon" src="../svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluye2">
							    <div class="card card-body">
								<ul>			
									
									<li>Local transport if more than two people</li>
									
									<li>Insurance</li>
								</ul>
								</div>
							</div>
						</div>
					</div>
						
						</div>
				  </div>
			</div>
		  
			
			
			
		  </div>
		  <div class="card col-md-12">
		  <div id="collapseGuayaquil" class="collapse" aria-labelledby="headingGuayaquil" data-parent="#accordion_vuelos">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselGuayaquil" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-guayaquil/vuelos-parapente-guayaquil.jpg" alt="Paragliding Guayaquil Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Guayaquil</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-guayaquil/parapente-guayaquil-ecuador.jpg" alt="Paragliding Guayaquil Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Guayaquil</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselGuayaquil" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselGuayaquil" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselGuayaquil" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-guayaquil/vuelos-parapente-guayaquil.jpg" alt="Paragliding Guayaquil Ecuador"></li>
							<li data-target="#carouselGuayaquil" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-guayaquil/parapente-guayaquil-ecuador.jpg" alt="Paragliding Guayaquil Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Paragliding Guayaquil</h1></header>
						<p>We are on Vía a la Costa 13km and a half. The adventure that starts from the 4x4 truck ride up the Chongon - Colonche mountain "Cerro Blanco" to the "Bototillo" paragliding track that has a height of 300 meters above sea level, flying here we can observe the city of Guayaquil, its mangroves, gulf and the Guayas River.</p>
						<p>We received a technical talk before the flight.</p>
						<p>We attend from 10:30 am to 5:00 p.m.</p>
						<div class="row ">
						
						<div class="col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a class="content-tablet" href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/bD6Z" class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Book Now">Parapente Guayaquil</a>
							</p>
							<p id="social_icons_detail">
							<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Guayaquil" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>	</a>														
							<a href="https://www.tripadvisor.com/Attraction_Review-g303845-d11851293-Reviews-Opeturmo-Guayaquil_Guayas_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapenteguayaquil/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteGuayaquilEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						
						<div class="col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluye1" role="button" aria-expanded="false" aria-controls="collapseIncluye1">
								INCLUDES: <img class="down-icon" src="../svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluye1">
							    <div class="card card-body">
								<ul>			
									
									<li>Local transport if more than two people</li>
									
									<li>Insurance</li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				  </div>
			</div>
		  
			
			
			
		  </div>
		  <div class="card col-md-12">
			<div id="collapseQuito" class="collapse" aria-labelledby="headingQuito" data-parent="#accordion_vuelos">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselQuito" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-quito-ecuador.jpg" alt="Paragliding Quito Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Quito</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-paragliding-quito-ecuador.jpg" alt="Paragliding Quito Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Quito</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselQuito" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselQuito" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselQuito" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-quito-ecuador.jpg" alt="Paragliding Quito Ecuador"></li>
							<li data-target="#carouselQuito" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-paragliding-quito-ecuador.jpg" alt="Paragliding Quito Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Paragliding Quito</h1></header>
						<p>We take off from the hill of Lumbisi towards the City, we observe the landscape of the city of Quito you can observe the mountains. We landed at the foot of the hill.</p>
						<p>The meeting point is on the way to Lumbisí km 1 at the entrance to La Primavera at 8:30 am. From there we got on a vehicle to the mountain.</p>
						
					
					<div class="row ">
						<div class="col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a class="content-tablet" href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/yNBk" class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Book Now">Parapente Quito</a>
							</p>
							<p id="social_icons_detail">
							<!-- <a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Ecuador,+Ruta+del+Spondylus+E15" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>						
							-->
							<a href="https://www.tripadvisor.com/Attraction_Review-g294308-d10113679-Reviews-Opeturmo-Quito_Pichincha_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapentequitoecuador/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteQuitoEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						
						<div class="col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluye3" role="button" aria-expanded="false" aria-controls="collapseIncluye3">
								INCLUDES: <img class="down-icon" src="../svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluye3">
							    <div class="card card-body">
								<ul>			
									
									<li>Local transport if more than two people</li>
									
									<li>Insurance</li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				  </div>
			</div>
		  </div>
		  
		  <div class="card col-md-12">
			<div id="collapseIbarra" class="collapse" aria-labelledby="headingIbarra" data-parent="#accordion_vuelos">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselIbarra" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-ibarra.jpg" alt="Paragliding Ibarra Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Ibarra</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-paragliding-ibarra-ecuador.jpg" alt="Paragliding Ibarra Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Ibarra</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselIbarra" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselIbarra" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselIbarra" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-ibarra.jpg" alt="Paragliding Ibarra Ecuador"></li>
							<li data-target="#carouselIbarra" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-paragliding-ibarra-ecuador.jpg" alt="Paragliding Ibarra Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Paragliding Ibarra</h1></header>
						<p>Flying in Ibarra you can see from the air the lagoon of Yahuarcocha, the beautiful mountains of the Ecuadorian highlands, the Imbabura and Cotacachi Volcanoes.</p>

						<p>Flights in Ibarra are early in the morning.</p>
						
					
					<div class="row ">
						<div class="col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a class="content-tablet" href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/7rMB" class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Book Now">Parapente Ibarra</a>
							</p>
							<p id="social_icons_detail">
							<!--<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Ecuador,+Ruta+del+Spondylus+E15" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>
								-->							
							<a href="https://www.tripadvisor.com/Attraction_Review-g312858-d11855169-Reviews-Opeturmo-Ibarra_Imbabura_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapenteibarra/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteIbarraEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						
						<div class="col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluye4" role="button" aria-expanded="false" aria-controls="collapseIncluye4">
								INCLUDES: <img class="down-icon" src="../svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluye4">
							    <div class="card card-body">
								<ul>			
									
									<li>Local transport if more than two people</li>
									
									<li>Insurance</li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		  </div>
		  <div class="card col-md-12">
			<div id="collapseOtros" class="collapse" aria-labelledby="headingOtros" data-parent="#accordion_vuelos">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselOtros" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-canoa/vuelos-parapente-canoa-ecuador.jpg" alt="Paragliding Canoa Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Canoa</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-cuenca/vuelos-parapente-cuenca.jpg" alt="Paragliding Cuenca Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Ba&ntilde;os</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselOtros" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselOtros" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselOtros" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-canoa/vuelos-parapente-canoa-ecuador.jpg" alt="Paragliding Canoa Ecuador"></li>
							<li data-target="#carouselOtros" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-cuenca/vuelos-parapente-cuenca.jpg" alt="Paragliding Cuenca Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
						
						<header><h1>Paragliding Ba&ntilde;os</h1></header>
						<p>We fly in Niton observing the landscape of Baños where you can see the Tungurahua volcano</p>
						<header><h1>Paragliding  Cuenca </h1></header>
						<p>In Cuenca you fly in Paute over the mountains of the Ecuadorian highlands.</p>
						<header><h1>Paragliding Ancon - Salinas</h1></header>
						<p>We fly on the cliffs of Ancón near Salinas</p>
						<header><h1>Paragliding San Pedro</h1></header>
						<p> We fly in Playa Bruja near San Pedro in Santa Elena.</p>
						<header><h1>Paragliding Canoa</h1></header>
						<p>In Canoa you can see the river of Bahia de Caraquez from the air and the beautiful beach and the mountains with its jungle of Canoa from the air.  </p>
						<header><h1>Paragliding Crucita</h1></header>
						<p>We fly on the beache of Crucita in Manab&iacute; with the view of the beach and ocean.</p>
						
						
					<div class="row ">
						<div class="col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a class="content-tablet" href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/NBy9" class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Book Now">Parapente Montanita</a>
							</p>
							
							<div class="row">
															
								<div class="accordion" id="accordionSocial">

								<div class="row">
								<div class="col-3">
								  <div id="headingDir">
									  <h5 class="mb-0">
										<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseDir" aria-expanded="true" aria-controls="collapseDir">
										<img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>				
										</button>
									  </h5>
									</div>
								  </div>
								  <div class="col-3">
								  <div id="headingTripadvisor">
									  <h5 class="mb-0">
										<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTripadvisor" aria-expanded="true" aria-controls="collapseTripadvisor">
										 <img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/>					
										</button>
									  </h5>
									</div>
								  </div>
								  
								<div class="col-3">
									<div  id="headingInstagram">
									  <h5 class="mb-0">
										<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseInstagram" aria-expanded="false" aria-controls="collapseInstagram">
										<img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>
															
										</button>
									  </h5>
									</div>
								  
								</div>  
								<div class="col-3">
								  
									<div  id="headingFace">
									  <h5 class="mb-0">
										<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFace" aria-expanded="false" aria-controls="collapseFace">
										<img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>
															
										</button>
									  </h5>
									</div>
								</div>
								</div>
								  
								  <div class="row">
								  <div class="col-12">
								  <div class="card">
									

									<div id="collapseDir" class="collapse " aria-labelledby="headingDir" data-parent="#accordionSocial">
									  <div class="card-body">
											<div class="row">
											  <div class="col-6">
												<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Canoa" target="blank"><p><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>Canoa</p></a>
											  </div>
										</div>				
										
									  </div>
									</div>
								  </div>
								  <div class="card">
									

									<div id="collapseTripadvisor" class="collapse " aria-labelledby="headingTripadvisor" data-parent="#accordionSocial">
									  <div class="card-body">
										  <div class="row">
											  <div class="col-6">
											  <a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Banos_Tungurahua_Province.html" target="blank"><p><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/>Ba&ntilde;os</p></a>
											  <a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Cuenca_Azuay_Province.html" target="blank"><p><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/>Cuenca</p></a>
											  </div>
											  <div class="col-6">
											 <!-- <p><a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Montanita_Santa_Elena_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>Canoa</p>
											  <p><a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Montanita_Santa_Elena_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>Crucita</p>
											 --> </div>
										</div>
									</div>
								  </div>
								  </div>
								  <div class="card">
									<div id="collapseInstagram" class="collapse" aria-labelledby="headingInstagram" data-parent="#accordionSocial">
									  <div class="card-body">
									   <div class="row">
											  <div class="col-6">
											  <a href="https://www.instagram.com/parapentebanos/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Ba&ntilde;os</p></a>
											  <a href="https://www.instagram.com/cuencaparapente/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Cuenca</p></a>
											  <a href="https://www.instagram.com/parapentesalinas/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Ancon</p></a>
											  </div>
											  <div class="col-6">
											  <a href="https://www.instagram.com/parapentemontanita/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>San Pedro</p></a>
											  <a href="https://www.instagram.com/parapentecanoa/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Canoa</p></a>
											  <a href="https://www.instagram.com/parapentecrucita/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Crucita</p></a>
											  </div>
										</div>
									</div>
								  </div>
								  </div>
								  <div class="card">
									<div id="collapseFace" class="collapse" aria-labelledby="headingFace" data-parent="#accordionSocial">
									  <div class="card-body">
									  <div class="row">
									  <div class="col-6">
									 <a href="https://www.facebook.com/ParapenteBanosEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Ba&ntilde;os</p></a>
									 <a href="https://www.facebook.com/ParapenteCuencaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Cuanca</p></a>
									 <a href="https://www.facebook.com/ParapenteAnconSalinasEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Ancon</p></a>
									  </div>
									  <div class="col-6">
									 <a href="https://www.facebook.com/ParapenteMontanitaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>San Pedro</p></a>
									 <a href="https://www.facebook.com/ParapenteCanoaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Canoa</p></a>
									 <a href="https://www.facebook.com/ParapenteCrucitaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Crucita</p></a>
									  </div>
									  </div>
									  </div>
									</div>
								  </div>
								</div>
								</div>
								</div>							
							
							</div>							
							
							
							
							
						</div>
							
							
							<div class="col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluye5" role="button" aria-expanded="false" aria-controls="collapseIncluye5">
								INCLUDES: <img class="down-icon" src="../svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluye5">
							    <div class="card card-body">
								<ul>			
									
									<li>Local transport if more than two people</li>
									
									<li>Insurance</li>
								</ul>
								</div>
							</div>
						</div>
							
							
							
						</div>
						
						</div>
						
				</div>
			</div>
		  </div>
		  
		  </div>
		  
		</div>
    </div>
	
	
</div> 
<!-- end desktop -->









<div class="content-mobile">




 
 <div class="row header_row">
		<div class="col-12">
		<header><h1>Vuelos Paragliding Ecuador</h1></header>
		</div>
	</div>
<div class="row">
	<div class="accordion" id="accordion_vuelos_mobile">
		 <div class="row vuelos">
			<div class="col-6 col-sm-2 col-md-2 vuelos_box">
			<div class="card card-vuelos collapsed" data-toggle="collapse" data-target="#collapseMontanitaMobile" aria-expanded="true" aria-controls="collapseMontanitaMobile">
			    <header><h1>Montanita</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/vuelos-parapente-playa-montanita-ecuador.png" alt="Paragliding Montanita Ecuador">
				<button>More...</button>
			</div>
			</div>
			<div class="col-6 col-sm-2 col-md-2 vuelos_box">
			
			<div class="card  card-vuelos collapsed" data-toggle="collapse" data-target="#collapsePLopezMobile" aria-expanded="false" aria-controls="collapsePLopezMobile">
				 <header><h1>Puerto Lopez</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/vuelos-parapente-puerto-lopez-ecuador.png" alt="Paragliding Puerto Lopez Ecuador">
				<button>More...</button>
			</div>
			
			
			</div>
			</div>
			
			
		 <div class="row vuelos_details">
			<div class="card col-md-12">
				<div id="collapseMontanitaMobile" class="collapse" aria-labelledby="headingMontanita" data-parent="#accordion_vuelos_mobile">
				  <div class="card-body row">
					<div class="col-md-4">
						
						
						<div id="carouselMontanitaMobile" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						   <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-playa-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapegliding in Monta&ntilde;ita</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/playa-parapente-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapegliding in Monta&ntilde;ita</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-montanita.jpg" alt="Parapgliding Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapegliding in Monta&ntilde;ita</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-con-motor-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapegliding in Monta&ntilde;ita with a motor</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-a-motor-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador">
							<div class="carousel-caption">
								  <h2>Parapegliding in Monta&ntilde;ita with a motor</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselMontanita" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselMontanita" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselMontanitaMobile" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-playa-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador"></li>
							<li data-target="#carouselMontanitaMobile" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/playa-parapente-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador"></li>
							<li data-target="#carouselMontanitaMobile" data-slide-to="2"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-montanita.jpg" alt="Parapgliding Montanita Ecuador"></li>
							<li data-target="#carouselMontanitaMobile" data-slide-to="3"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-con-motor-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador"></li>
							<li data-target="#carouselMontanitaMobile" data-slide-to="4"><img class=" " src="https://opeturmo.com/images_parapente/parapente-montanita/parapente-a-motor-montanita-ecuador.jpg" alt="Parapgliding Montanita Ecuador"></li>
						  </ol>
						
						</div>
											
						
					</div>
					<div class="col-md-8">
					<header><h1>Paragliding Monta&ntilde;ita</h1></header>
					<p>We fly in a paraglider with the pilot guide, who takes you to the adventure activity, in front of the sea on the Ruta del Spondylus (Ruta del Sol). The guide takes care of the whole operation, you just have to enjoy and fulfill your dream of flying. The flight experience lasts between 12-15 minutes.</p>
					<li><b>Paragliding</b> Paragliding flights in front of the sea is a dynamic flight that takes place moving through the skies taking advantage of the mountainside. The sea wind allows us to be able to sustain ourselves in the air and fly like birds.</li>
					<p><b>Promociones:</b></p>
					<ul>
					<li>Vuelan en Parpente 6 personas pagan 5</li>
					<li>Cumplea&ntilde;eros gratis las fotos</li>
					</ul>
					<li><b>Paragliding a Motor</b> Un vuelo en Paragliding motorizado se puede recorrer mayor distancia en el mismo tiempo, ascendemos a mayor altura para apreciar el increible oceano pacifico y el continente del perfil ecuatoriano </li>
					<p>Atendemos desde las 11:00am hasta las 6:00pm, pero este horario puede variar dependiendo la epoca del a&ntilde;o y del dia. </p>
										
								
					<div class="row ">
						<div class="col-12 col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/lB4k" class="peek-book-btn-blue peek-book-button-flat peek-book-button-sm" data-button-text="Book Now">Parapente Montanita</a>
							</p>
						
							<p id="social_icons_detail">	
							<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Ecuador,+Ruta+del+Spondylus+E15" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>
														
							<a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Montanita_Santa_Elena_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapentemontanita/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteMontanitaEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						<div class="col-12 col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluyeMobile" role="button" aria-expanded="false" aria-controls="collapseIncluyeMobile">
								INCLUDES: <img class="down-icon" src="../svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluyeMobile">
							    <div class="card card-body">
								<ul>			
									
									<li>Local transport if more than two people</li>
									<li>Insurance</li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				  </div>
				</div>
			</div>
		  <div class="card col-md-12">
		  <div id="collapsePLopezMobile" class="collapse" aria-labelledby="headingPLopez" data-parent="#accordion_vuelos_mobile">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselPLopezMobile" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
							<div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez.jpg" alt="Parapente Puerto Lopez Ecuador">
							<div class="carousel-caption">
								  <h2>Paragliding in Puerto Lopez</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/vuelos-parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
							<div class="carousel-caption">
								  <h2>Paragliding in Puerto Lopez</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
							<div class="carousel-caption">
								  <h2>Paragliding in Puerto Lopez</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
							<div class="carousel-caption">
								  <h2>Paragliding in Puerto Lopez</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-flights-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador">
							<div class="carousel-caption">
								  <h2>Paragliding in Puerto Lopez</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselPLopezMobile" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselPLopezMobile" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselPLopezMobile" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
							<li data-target="#carouselPLopezMobile" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/vuelos-parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
							<li data-target="#carouselPLopezMobile" data-slide-to="2"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/parapente-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
							<li data-target="#carouselPLopezMobile" data-slide-to="3"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
							<li data-target="#carouselPLopezMobile" data-slide-to="4"><img class=" " src="https://opeturmo.com/images_parapente/parapente-puerto-lopez/paragliding-flights-puerto-lopez-ecuador.jpg" alt="Parapente Puerto Lopez Ecuador"></li>
						  </ol>
					
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Paragliding Puerto L&oacute;pez</h1></header>
						<p>We fly in paragliding with the pilot guide, which takes you to perform the adventure activity of paragliding. The guide is in charge of the whole operation, you just have to enjoy and fulfill your dream of flying.</p>

						<p>We leave the meeting point to the paragliding takeoff in Puerto L&oacute;pez with the 4x4 vehicle, which is located inside the Machalilla National Park and it takes about 20 minutes. From the top of the takeoff you can see the bay of Puerto L&oacute;pez and a large part of the park, such as the island of Salango, the agua blanca (white water) river, etc. and when the sky is clear the isla de la plata (island of silver) is observed.</p>
						<p>We received a technical explain from the guide before the flight to know a bit about how the activity is carried out.</p>
						<p>We fly from the Machalilla National Park over spondylus route and landed on the beach in the harbor of Puerto L&oacute;pez the capital of the sky.</p>
						<p>In case of weather conditions are not suitable for flying in Puerto Lopez we will have the option of moving to another place to take the paragliding flight 10 minutes from Puerto Lopez.</p>
						<p>We attend from 10:30 am to 5:00 p.m.</p>
						
						
					<div class="row ">
						<div class="col-12 col-md-6 col-sm-6 col-xs-6 buttons_row">
						<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/aL34" class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-sm" data-button-text="Book Now" data-button-text="Book Now">Paragliding Puerto Lopez</a>
							</p>
						
							<p id="social_icons_detail">
							<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Puerto+Lopez" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>	</a>			
															
							<a href="https://www.tripadvisor.com/Attraction_Review-g635730-d11872462-Reviews-Opeturmo-Puerto_Lopez_Manabi_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapentepuertolopez/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapentePuertoLopezEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						
						<div class="col-12 col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluyeMobile2" role="button" aria-expanded="false" aria-controls="collapseIncluyeMobile2">
								INCLUDES: <img class="down-icon" src="../svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluyeMobile2">
							    <div class="card card-body">
								<ul>			
									
									<li>Local transport if more than two people</li>
									<li>Insurance</li>
								</ul>
								</div>
							</div>
						</div>
					</div>
						
						</div>
				  </div>
			</div>
		  
			
			
			
		  </div>
		  </div>
		  
			
			
			<div class="row vuelos">
			<div class="col-6 col-sm-2 col-md-2 vuelos_box">
			<div class="card card-vuelos collapsed" data-toggle="collapse" data-target="#collapseGuayaquilMobile" aria-expanded="false" aria-controls="collapseGuayaquilMobile">
				 <header><h1>Guayaquil</h1></header>	  
				<img class=" " src="https://opeturmo.com/images_parapente/parapente-guayaquil/vuelos-parapente-guayaquil-ecuador.png" alt="Paragliding Guayaquil Ecuador">
				<button>More...</button>
			</div>
			
			</div>
			<div class="col-6 col-sm-2 col-md-2 vuelos_box">
			<div class="card card-vuelos collapsed" data-toggle="collapse" data-target="#collapseQuitoMobile" aria-expanded="false" aria-controls="collapseQuitoMobile">
				 <header><h1>Quito</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-quito-ecuador.png" alt="Paragliding Quito Ecuador">
				<button>More...</button>
			</div>
			</div>
			</div>
			
			
			

		   <div class="row vuelos_details">
		  <div class="card col-md-12">
		  <div id="collapseGuayaquilMobile" class="collapse" aria-labelledby="headingGuayaquil" data-parent="#accordion_vuelos_mobile">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselGuayaquilMobile" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							   <img class="" src="https://opeturmo.com/images_parapente/parapente-guayaquil/vuelos-parapente-guayaquil.jpg" alt="Paragliding Guayaquil Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Guayaquil</h2>
								</div>							
							</div>
							<div class="carousel-item">
							   <img class="" src="https://opeturmo.com/images_parapente/parapente-guayaquil/parapente-guayaquil-ecuador.jpg" alt="Paragliding Guayaquil Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Guayaquil</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselGuayaquil" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselGuayaquil" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselGuayaquilMobile" data-slide-to="0" class="active"> <img class="" src="https://opeturmo.com/images_parapente/parapente-guayaquil/vuelos-parapente-guayaquil.jpg" alt="Paragliding Guayaquil Ecuador"></li>
							<li data-target="#carouselGuayaquilMobile" data-slide-to="1"> <img class="" src="https://opeturmo.com/images_parapente/parapente-guayaquil/parapente-guayaquil-ecuador.jpg" alt="Paragliding Guayaquil Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Paragliding Guayaquil</h1></header>
						<p>We are on Vía a la Costa 13km and a half. The adventure that starts from the 4x4 truck ride up the Chongon - Colonche mountain "Cerro Blanco" to the "Bototillo" paragliding track that has a height of 300 meters above sea level, flying here we can observe the city of Guayaquil, its mangroves, gulf and the Guayas River.</p>
						<p>We received a technical talk before the flight.</p>
						<p>We attend from 10:30 am to 5:00 p.m.</p>
						
					<div class="row ">
						<div class="col-12 col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/bD6Z" class="peek-book-btn-blue peek-book-button-flat peek-book-button-sm" data-button-text="Book Now">Parapente Guayaquil</a>
						</p>
						
							<p id="social_icons_detail">
							<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Guayaquil" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/></a>
							
															
							<a href="https://www.tripadvisor.com/Attraction_Review-g303845-d11851293-Reviews-Opeturmo-Guayaquil_Guayas_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapenteguayaquil/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteGuayaquilEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						
						<div class="col-12 col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluyeMobile1" role="button" aria-expanded="false" aria-controls="collapseIncluyeMobile1">
								INCLUDES: <img class="down-icon" src="../svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluyeMobile1">
							    <div class="card card-body">
								<ul>			
									
									<li>Local transport if more than two people</li>
									<li>Insurance</li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				  </div>
			</div>
		  
			
			
			
			
		  </div>
		  <div class="card col-md-12">
			<div id="collapseQuitoMobile" class="collapse" aria-labelledby="headingQuito" data-parent="#accordion_vuelos_mobile">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselQuitoMobile" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-quito-ecuador.jpg" alt="Paragliding Quito Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Quito</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-paragliding-quito-ecuador.jpg" alt="Paragliding Quito Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Quito</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselQuito" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselQuito" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselQuitoMobile" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-quito-ecuador.jpg" alt="Paragliding Quito Ecuador"></li>
							<li data-target="#carouselQuitoMobile" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-quito/vuelos-parapente-paragliding-quito-ecuador.jpg" alt="Paragliding Quito Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Paragliding Quito</h1></header>
						<p>We take off from the hill of Lumbisi towards the City, we observe the landscape of the city of Quito you can observe the mountains. We landed at the foot of the hill.</p>
						<p>The meeting point is on the way to Lumbisí km 1 at the entrance to La Primavera at 8:30 am. From there we got on a vehicle to the mountain.</p>
						
					
					<div class="row ">
						<div class="col-12 col-12 col-md-6 col-sm-6 col-xs-6 buttons_row">
						<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/yNBk" class="peek-book-btn-blue peek-book-button-flat peek-book-button-sm" data-button-text="Book Now">Parapente Quito</a>
							</p>
						
								<p id="social_icons_detail">
								<!--
								<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Ecuador,+Ruta+del+Spondylus+E15" target="blank"><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>	
							-->
														
							<a href="https://www.tripadvisor.com/Attraction_Review-g294308-d10113679-Reviews-Opeturmo-Quito_Pichincha_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapentequitoecuador/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteQuitoEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						<div class="col-12 col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluyeMobile3" role="button" aria-expanded="false" aria-controls="collapseIncluyeMobile3">
								INCLUDES: <img class="down-icon" src="../svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluyeMobile3">
							    <div class="card card-body">
								<ul>			
									
									<li>Local transport if more than two people</li>
									<li>Insurance</li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				  </div>
			</div>
		  </div>
		  
		  </div>
		  
		  
		  
			<div class="row vuelos">
			<div class="col-6 col-sm-2 col-md-2 vuelos_box">
			<div class="card  card-vuelos collapsed" data-toggle="collapse" data-target="#collapseIbarraMobile" aria-expanded="false" aria-controls="collapseIbarraMobile">
				  <header><h1>Ibarra</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-ibarra-ecuador.png" alt="Paragliding Ibarra Ecuador">
				<button>More...</button>
			</div>
			</div>
			<div class="col-6 col-sm-2  col-md-2 vuelos_box">
			<div class="card  card-vuelos collapsed" data-toggle="collapse" data-target="#collapseOtrosMobile" aria-expanded="false" aria-controls="collapseOtrosMobile">
				 <header><h1>Other places</h1></header>	  
				<img class="" src="https://opeturmo.com/images_parapente/parapente-crucita/vuelos-parapente-crucita-ecuador.png" alt="Paragliding Canoa Ecuador">
				<button>More...</button>
			</div>
		</div>
		</div>
		

		  
		   <div class="row vuelos_details">
		  <div class="card col-md-12">
			<div id="collapseIbarraMobile" class="collapse" aria-labelledby="headingIbarra" data-parent="#accordion_vuelos_mobile">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselIbarraMobile" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-ibarra.jpg" alt="Paragliding Ibarra Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Ibarra</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-paragliding-ibarra-ecuador.jpg" alt="Paragliding Ibarra Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Ibarra</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselIbarra" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselIbarra" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselIbarraMobile" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-ibarra.jpg" alt="Paragliding Ibarra Ecuador"></li>
							<li data-target="#carouselIbarraMobile" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-ibarra/vuelos-parapente-paragliding-ibarra-ecuador.jpg" alt="Paragliding Ibarra Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
					<header><h1>Paragliding Ibarra</h1></header>
						<p>Flying in Ibarra you can see from the air the lagoon of Yahuarcocha, the beautiful mountains of the Ecuadorian highlands, the Imbabura and Cotacachi Volcanoes.</p>

						<p>Flights in Ibarra are early in the morning.</p>
						
					
					<div class="row ">
						<div class="col-12 col-md-6 col-sm-6 col-xs-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/7rMB" class="peek-book-btn-blue peek-book-button-flat peek-book-button-sm" data-button-text="Book Now">Parapente Ibarra</a>
							</p>
						
						<p id="social_icons_detail">
															
							<a href="https://www.tripadvisor.com/Attraction_Review-g312858-d11855169-Reviews-Opeturmo-Ibarra_Imbabura_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>
							
							<a href="https://www.instagram.com/parapenteibarra/" target="blank"><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/></a>
							<a href="https://www.facebook.com/ParapenteIbarraEcuador/" target="blank"><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/></a>
							</p>
						</div>
						
						<div class="col-12 col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluyeMobile4" role="button" aria-expanded="false" aria-controls="collapseIncluyeMobile4">
								INCLUDES: <img class="down-icon" src="../svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluyeMobile4">
							    <div class="card card-body">
								<ul>			
									
									<li>Local transport if more than two people</li>
									<li>Insurance</li>
								</ul>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		  </div>
		  <div class="card col-md-12">
			<div id="collapseOtrosMobile" class="collapse" aria-labelledby="headingOtros" data-parent="#accordion_vuelos_mobile">
			  <div class="card-body row">
					<div class="col-md-4">
						<div id="carouselOtrosMobile" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-canoa/vuelos-parapente-canoa-ecuador.jpg" alt="Paragliding Canoa Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Canoa</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/parapente-cuenca/vuelos-parapente-cuenca.jpg" alt="Paragliding Cuenca Ecuador">
								<div class="carousel-caption">
								  <h2>Paragliding in Ba&ntilde;os</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselOtros" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselOtros" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselOtrosMobile" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/parapente-canoa/vuelos-parapente-canoa-ecuador.jpg" alt="Paragliding Canoa Ecuador"></li>
							<li data-target="#carouselOtrosMobile" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/parapente-cuenca/vuelos-parapente-cuenca.jpg" alt="Paragliding Cuenca Ecuador"></li>
						  </ol>
						
						</div>
					</div>
					<div class="col-md-8">
						
						<header><h1>Paragliding Ba&ntilde;os</h1></header>
						<p>We fly in Niton observing the landscape of Baños where you can see the Tungurahua volcano</p>
						<header><h1>Paragliding  Cuenca </h1></header>
						<p>In Cuenca you fly in Paute over the mountains of the Ecuadorian highlands.</p>
						<header><h1>Paragliding Ancon - Salinas</h1></header>
						<p>We fly on the cliffs of Ancón near Salinas</p>
						<header><h1>Paragliding San Pedro</h1></header>
						<p> We fly in Playa Bruja near San Pedro in Santa Elena.</p>
						<header><h1>Paragliding Canoa</h1></header>
						<p>In Canoa you can see the river of Bahia de Caraquez from the air and the beautiful beach and the mountains with its jungle of Canoa from the air.  </p>
						<header><h1>Paragliding Crucita</h1></header>
						<p>We fly on the beache of Crucita in Manab&iacute; with the view of the beach and ocean.</p>
						
					
					<div class="row ">
						<div class="col-12 col-sm-6 col-md-6 buttons_row">
							<p id="social_icons_detail">
							<a class="tel_box" href="tel:+593987237776"><button type="button" class="btn button-contact"><i class="fa fa-phone fas-tel"></i></button></a>
							<a><button type="button" class="btn button-contact"  data-toggle='modal' data-target='#reserveTandemModal' ><i class="fa fa-envelope fas-mail"></i></button></a>
							<a href="intent://send/+593987237776#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><button type="button" class="btn button-contact"><img alt="whatsapp" src="https://opeturmo.com/images_parapente/social-icons/whatsapp.png"/></button></a>
							<a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/NBy9" class="peek-book-btn-blue peek-book-button-flat peek-book-button-sm" data-button-text="Book Now">Parapente Montanita</a>
							</p>
						
							<div class="row">
							<div class="col-12">							
								
							<div class="accordion" id="accordionSocialMobile">

								<div class="row">
								<div class="col-3">
								  <div id="headingDirMobile">
									  <h5 class="mb-0">
										<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseDirMobile" aria-expanded="true" aria-controls="collapseDirMobile">
										<img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>				
										</button>
									  </h5>
									</div>
								  </div>
								  <div class="col-3">
								  <div id="headingTripadvisorMobile">
									  <h5 class="mb-0">
										<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTripadvisorMobile" aria-expanded="true" aria-controls="collapseTripadvisorMobile">
										 <img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/>					
										</button>
									  </h5>
									</div>
								  </div>
								  
								  
								<div class="col-3">
									<div  id="headingInstagramMobile">
									  <h5 class="mb-0">
										<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseInstagramMobile" aria-expanded="false" aria-controls="collapseInstagramMobile">
										<img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>
															
										</button>
									  </h5>
									</div>
								  
								</div>  
								<div class="col-3">
								  
									<div  id="headingFaceMobile">
									  <h5 class="mb-0">
										<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFaceMobile" aria-expanded="false" aria-controls="collapseFaceMobile">
										<img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>
															
										</button>
									  </h5>
									</div>
								</div>
								</div>
								  
								  <div class="row">
								  <div class="col-12">
								  <div class="card">
									

									<div id="collapseDirMobile" class="collapse " aria-labelledby="headingDirMobile" data-parent="#accordionSocialMobile">
									  <div class="card-body">
											<div class="row">
											 <div class="col-6">
												<a href="https://www.google.com/maps/search/?api=1&query=Opeturmo+-+Parapente+Canoa" target="blank"><p><img alt="google_map" src="https://opeturmo.com/images_parapente/social-icons/map_xxsmall.png"/>Canoa</p></a>
											  </div>
										</div>				
										
									  </div>
									</div>
								  </div>
								  <div class="card">
									

									<div id="collapseTripadvisorMobile" class="collapse " aria-labelledby="headingTripadvisorMobile" data-parent="#accordionSocialMobile">
									  <div class="card-body">
										  <div class="row">
											   <div class="col-6">
											  <a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Banos_Tungurahua_Province.html" target="blank"><p><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/>Ba&ntilde;os</p></a>
											  <a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Cuenca_Azuay_Province.html" target="blank"><p><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/>Cuenca</p></a>
											  </div>
											  <div class="col-6">
											 <!-- <p><a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Montanita_Santa_Elena_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>Canoa</p>
											  <p><a href="https://www.tripadvisor.com/Attraction_Review-g609140-d7834907-Reviews-Opeturmo-Montanita_Santa_Elena_Province.html" target="blank"><img alt="tripadvisor" src="https://opeturmo.com/images_parapente/social-icons/tripadvisor-xxsmall.png"/></a>Crucita</p>
											 --> </div>
										</div>
									</div>
								  </div>
								  </div>
								  <div class="card">
									<div id="collapseInstagramMobile" class="collapse" aria-labelledby="headingInstagramMobile" data-parent="#accordionSocialMobile">
									  <div class="card-body">
									   <div class="row">
											    <div class="col-6">
											  <a href="https://www.instagram.com/parapentebanos/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Ba&ntilde;os</p></a>
											  <a href="https://www.instagram.com/cuencaparapente/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Cuenca</p></a>
											  <a href="https://www.instagram.com/parapentesalinas/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Ancon</p></a>
											  </div>
											  <div class="col-6">
											  <a href="https://www.instagram.com/parapentemontanita/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>San Pedro</p></a>
											  <a href="https://www.instagram.com/parapentecanoa/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Canoa</p></a>
											  <a href="https://www.instagram.com/parapentecrucita/" target="blank"><p><img alt="instagram" src="https://opeturmo.com/images_parapente/social-icons/instagram_xxsmall.png"/>Crucita</p></a>
											  </div>
										</div>
									</div>
								  </div>
								  </div>
								  <div class="card">
									<div id="collapseFaceMobile" class="collapse" aria-labelledby="headingFaceMobile" data-parent="#accordionSocialMobile">
									  <div class="card-body">
									  <div class="row">
									  <div class="col-6">
									 <a href="https://www.facebook.com/ParapenteBanosEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Ba&ntilde;os</p></a>
									 <a href="https://www.facebook.com/ParapenteCuencaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Cuanca</p></a>
									 <a href="https://www.facebook.com/ParapenteAnconSalinasEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Ancon</p></a>
									  </div>
									  <div class="col-6">
									 <a href="https://www.facebook.com/ParapenteMontanitaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>San Pedro</p></a>
									 <a href="https://www.facebook.com/ParapenteCanoaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Canoa</p></a>
									 <a href="https://www.facebook.com/ParapenteCrucitaEcuador/" target="blank"><p><img alt="facebook" src="https://opeturmo.com/images_parapente/social-icons/facebook_xxsmall.png"/>Crucita</p></a>
									  </div>
									  </div>
									  </div>
									</div>
								  </div>
								</div>
								</div>
								</div>
								
							
							
							
							</div>							
							</div>							
							
							<div class="col-12 col-md-4 col-sm-6 bring">
							<p data-toggle="collapse" href="#collapseIncluyeMobile5" role="button" aria-expanded="false" aria-controls="collapseIncluyeMobile5">
								INCLUDES: <img class="down-icon" src="../svg/si-glyph-triangle-down.svg" /> </p>
							<div class="collapse" id="collapseIncluyeMobile5">
							    <div class="card card-body">
								<ul>			
									
									<li>Local transport if more than two people</li>
									<li>Insurance</li>
								</ul>
								</div>
							</div>
						</div>
							
							
						</div>
						
							
						</div>
							
							
							
							
							
							
						</div>
						
						
					</div>
					</div>
				</div>
			</div>
		  </div>
		  
		  </div>
		  
		</div>
    </div>
	



