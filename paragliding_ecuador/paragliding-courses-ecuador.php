
	<div class="row header_row">
		<div class="col-12">
			<header><h1>Paragliding School Ecuador</h1></header>
		</div>
	</div>
	

	
	<div class="row cursos">
		<div class="col-12 col-sm-12 col-md-8">
			<header><h2>Paraglising Courses</h2></header>
			
			<p>In our paragliding school you will learn this adventure sport with an APPI certified instructor (Associacion of Paraglider Pilots and Instructors). We have different paragliding courses depending on your level.</p>
			<h4>Iniciation</h4>
			<p>This is the first paragliding course degree in the APPI Education System. The student learns how to install and control the wing on the ground (’Ground Skimming’) and have his/her first solo flight experience. At this level, the student is allowed to fly ONLY with radio communication and under an Instructor’s supervision.</p>
			<p>We use Playa Bruja take-off for this level.</p>
			<p><a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/zgp9v"  class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Book Now" >Book Now</a></p>
			<h4>Progretion 1</h4>
			<p>In this paragliding course the student is capable of flying from the take-off to the landing zone in stable weather conditions, in altitude gliding. At this level, the student is still allowed to fly ONLY with radio communication and under an Instructor’s supervision.</p>
			<p>We use Playa Bruja take-off for this level.</p>
			<p><a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/kxk9q"  class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Book Now" >Book Now</a></p>
			<h4>Progretion 2</h4>
			<p>In this paragliding course the student has achieved the level to become an Independent Pilot.</p>

				<p>They are able to :</p>
				<ol>
				<li>Fly without supervision</li>
				<li>Analyze different weather conditions</li>
				<li>Have a specialty in alpine, soaring and/ or flat land.</li></ol>
				<p>Before flying at different sites, APPI Solo Pilots are recommended to choose places where they can receive the assistance of an APPI School. Here, they receive important information, support and more security</p>
			<p>We use Playa Bruja or Puerto Lopez take-off for this level.</p>
			<p><a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/kxk9q"  class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Book Now" >Book Now</a></p>
			<h4>Cross Country</h4>
			<p>At this level of the paragliding courses you learn how to find and turn in thermals. We use Guayaquil take-off for this level.</p>
			<p><a href="https://www.peek.com/s/4aaee0fc-19f9-43da-8667-9972fe009432/kxk9q"  class="peek-book-btn-blue peek-book-button-flat peek-book-button-size-md" data-button-text="Book Now" >Book Now</a></p>
			<p>When you finish the course you ger a APPI certification. </p>
			
		</div>
		<div class="col-12 col-sm-4 col-md-4 ">
		  	 
			<div id="carouselCursosParapente" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/cursos-parapente/cursos_de_parapente_playa_ecuador.jpg" alt="Cursos Parapente Ecuador">
								<div class="carousel-caption">
								  <h2>Cursos de Parapente Ecuador</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/cursos-parapente/escuela_parapente_ecuador.jpg" alt="Cursos Parapente Ecuador">
							<div class="carousel-caption">
								  <h2>Cursos de Parapente Ecuador</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/cursos-parapente/cursos_parapente_ecuador_playa.jpg" alt="Cursos Parapente Ecuador">
							<div class="carousel-caption">
								  <h2>Cursos de Parapente Ecuador</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/cursos-parapente/escuela_parapente_playa.jpg" alt="Cursos Parapente Ecuador">
							<div class="carousel-caption">
								  <h2>Cursos de Parapente Ecuador</h2>
								</div>							
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/cursos-parapente/curso_parapente_ecuador.jpg" alt="Cursos Parapente Ecuador">
							<div class="carousel-caption">
								  <h2>Cursos de Parapente Ecuador</h2>
								</div>							
							</div>
						  </div>
						  <!-- <a class="carousel-control-prev" href="#carouselCursosParapente" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselCursosParapente" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> -->
						  <ol class="carousel-indicators">
							<li data-target="#carouselCursosParapente" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/cursos-parapente/cursos_de_parapente_playa_ecuador.jpg" alt="Cursos Parapente Ecuador"></li>
							<li data-target="#carouselCursosParapente" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/cursos-parapente/escuela_parapente_ecuador.jpg" alt="Cursos Parapente Ecuador"></li>
							<li data-target="#carouselCursosParapente" data-slide-to="2"><img class=" " src="https://opeturmo.com/images_parapente/cursos-parapente/cursos_parapente_ecuador_playa.jpg" alt="Cursos Parapente Ecuador"></li>
							<li data-target="#carouselCursosParapente" data-slide-to="3"><img class=" " src="https://opeturmo.com/images_parapente/cursos-parapente/escuela_parapente_playa.jpg" alt="Cursos Parapente Ecuador"></li>
							<li data-target="#carouselCursosParapente" data-slide-to="4"><img class=" " src="https://opeturmo.com/images_parapente/cursos-parapente/curso_parapente_ecuador.jpg" alt="Cursos Parapente Ecuador"></li>
						  </ol>
						
						</div>
		</div>
	</div>
	
	
	