<?php get_header(); ?>
<div class="row blog_row">
<section class=" col-12 col-md-9">
		<?php if (have_posts()) : ?>

 			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

			<?php /* If this is a category archive */ if (is_category()) { ?>
				<header><h2>Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h2></header>

			<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
				<header><h2>Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h2></header>

			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
				<header><h2>Archive for <?php the_time('F jS, Y'); ?></h2></header>

			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
				<header><h2>Archive for <?php the_time('F, Y'); ?></h2></header>

			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
				<header><h2>Archive for <?php the_time('Y'); ?></h2></header>

			<?php /* If this is an author archive */ } elseif (is_author()) { ?>
				<header><h2>Author Archive</h2></header>

			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
				<header><h2>Blog Archives</h2></header>
			
			<?php } ?>

			
			
			
			
			
			<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>
			
			<?php while (have_posts()) : the_post(); ?>
			<div class="blog_post">
				<div <?php post_class() ?>>
				<header class="header">
				<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
			</header>
					
					<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>

					<article class="entry row">
				<div class="row">
				<div class="col-md-12 images">
										 <?php
										preg_match_all('/(<img [^>]*>)/', get_the_content(), $images);
					for($i=0; isset($images[1]) && $i < count($images[1]); $i++) {
						echo $images[1][$i];
					}
										?>
										<p><?php $content = preg_replace('/(<img [^>]*>)/', '', get_the_content());
					$content = apply_filters('the_content', $content);
					echo $content;
				?></p>
				
				</div>				
				</div>	
				<div class="row">
				<div class="col-md-12 comments">
				<?php comments_template(); ?>
				</div>
				</div>
			</article>

				</div>
			</div>
			<?php endwhile; ?>
			
			
			
			

			<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>
			
	<?php else : ?>

		<header><h2>Nothing found</h2></header>

	<?php endif; ?>
</section>
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>