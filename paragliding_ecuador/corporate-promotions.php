 <div class="row">
			<div class="col-12 header-prom-corp">
				<header><h1>Corporate Promotions</h1></header>
			</div>
		</div>
   
   <div class="row prom_corp">
			<div class="col-12 col-sm-6 col-md-6">
				<p>
We offer you corporate promotions of packages of flights in paragliding, for educational institutions and corporate institutions. This will allow you to recharge your batteries, reduce stress, encourage integration with the aim of improving teamwork and increase productivity.</p>
				</div>
			<div class="col-12 col-sm-6 col-md-6">
				<div class="prom-corp-col">
					<div id="carouselPromCorp" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/promociones-corporativas/promociones_corpovativas_parapente.jpg" alt="Promociones Corporativas Parapente">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/promociones-corporativas/promocion_corpovativa_parapente.jpg" alt="Promociones Corporativas Parapente">
							</div>
						  </div>
						   <a class="carousel-control-prev" href="#carouselPromCorp" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselPromCorp" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> 
						  <ol class="carousel-indicators">
							<li data-target="#carouselPromCorp" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/promociones-corporativas/promociones_corpovativas_parapente.jpg" alt="Promociones Corporativas Parapente"></li>
							<li data-target="#carouselPromCorp" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/promociones-corporativas/promocion_corpovativa_parapente.jpg" alt="Promociones Corporativas Parapente"></li>
						  </ol>
						
						</div>
				</div>
							  
			</div>
	</div>