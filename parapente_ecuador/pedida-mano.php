 <div class="row">
			<div class="col-12 header-pedida-mano">
				<header><h1>Pedida de Mano</h1></header>
			</div>
		</div>
   
   <div class="row pedida_mano">
			<div class="col-12 col-sm-6 col-md-6">
				<p>Hacemos de tu pedida de mano de un momento inolvidable para que tu pareja y tú lo recuerden toda la vida. Con el pasar del tiempo, será contada la forma como propusiste matrimonio a tus amigos y extraños, por ello ten en cuenta que solo ocurrirá una vez en tu vida, debe ser memorable y por supuesto, con su gran toque romántico, que es fundamental en la proposición.<p>

				<p>Vuelo Libre en parapente para 1 persona</b><p>
				<p>Un vuelo aproximado de 10 a 15 min por persona. Aterrizaje en la playa, en donde el chico le estará esperando con un ramo de flores y un corazón conformado de piedras blancas sobre la arena. Es opcional si desea traer su propia leyenda escrita.<p>

				<p><b>Vuelo Libre en parapente para 2 personas </b><p>
				<p>Salen los dos al mismo tiempo, con diferentes pilotos guías. En un vuelo aproximado de 10 a 15 min por persona. Aterrizaje en la playa, primero aterriza quien realizara la proposición para esperar a la chica, con un ramo de flores y un corazón conformado de piedras blancas sobre la arena. La leyenda escrita es visible desde una altura mientras la chica realiza la actividad.<p> 

				<p>Se lo puede realizar en Guayaquil (temporada de junio a diciembre) y la playa en Libertador Bolívar / Playa Bruja a 10 minutos antes de Montañita. <p>

				<p><b>Incluye:</b><p>
				<ul>
				<li>Fotos y video 3 cámaras distintas de alta resolución con edición.</li>
				<li>Transporte local de los equipos a utilizar, pero no de los clientes.</li>
				<li>El diseño del arte viene por parte del cliente.</li>
				</ul>
				
				</div>
			<div class="col-12 col-sm-6 col-md-6">
				<div class="pedida-mano-col">
					<div id="carouselPadidaMano" class="carousel slide carousel-vuelos" data-interval="false" data-ride="carousel">
						
						  <div class="carousel-inner">
							<div class="carousel-item active">
							  <img class="" src="https://opeturmo.com/images_parapente/pedida-mano/parapente-a-motor-pedida-de-mano.jpg" alt="Corporate Promotions Paragliding">
							</div>
							<div class="carousel-item">
							  <img class="" src="https://opeturmo.com/images_parapente/pedida-mano/parapente-a-motor-pedida-de-matrimonio.jpg" alt="Corporate Promotions Paragliding">
							</div>
						  </div>
						   <a class="carousel-control-prev" href="#carouselPadidaMano" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselPadidaMano" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						  </a> 
						  <ol class="carousel-indicators">
							<li data-target="#carouselPadidaMano" data-slide-to="0" class="active"><img class=" " src="https://opeturmo.com/images_parapente/pedida-mano/parapente-a-motor-pedida-de-mano.jpg" alt="Corporate Promotions Paragliding"></li>
							<li data-target="#carouselPadidaMano" data-slide-to="1"><img class=" " src="https://opeturmo.com/images_parapente/pedida-mano/parapente-a-motor-pedida-de-matrimonio.jpg" alt="Corporate Promotions Paragliding"></li>
						  </ol>
						
						</div>
				</div>
							  
			</div>
	</div>